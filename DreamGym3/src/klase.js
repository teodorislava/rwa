import {DatabaseService} from './database';
export class Trening
{
    constructor(id, tip, trajanje, trener_ime,
        klijent_ime, klijent_prezime,
         dan, mesec, godina, vreme, cena)
    {
        this.id=id;
        this.tip=tip;
        this.trajanje=trajanje;
        this.klijent_prezime=klijent_prezime;
        this.trener_ime=trener_ime;
        this.klijent_ime=klijent_ime;
        this.dan=dan;
        this.mesec=mesec;
        this.godina=godina;
        this.vreme=vreme;
        this.cena=cena;
    }
    prikaz()
    {
        const main=document.getElementById("dodatak");
        main.innerHTML="";
        let lab=document.createElement("label");
        lab.innerHTML="Pronadjene su ove rezervacije: ";
        main.appendChild(lab);
        let br=document.createElement("br");
        main.appendChild(br);
        let trening=document.createElement("div");
        trening.className="trening";
        let tekst=document.createElement("div");
        tekst.className="text";
        tekst.innerHTML="Trening: "+this.tip+", Trajanje: "+
        this.trajanje+"min, Trener: "+this.trener_ime+" "+ ", Klijent: "+this.klijent_ime+
        " "+this.klijent_prezime+", Cena: "+this.cena+
        ", Vreme: "+this.vreme+" ,Datum: "+
        this.dan+"."+this.mesec+"."+this.godina+".";
        trening.appendChild(tekst);
        main.appendChild(trening);
    }
}
export class Masaza
{
    constructor(id, tip, trajanje, maser_ime,
        klijent_ime, klijent_prezime,
         dan, mesec, godina, vreme, cena)
    {
        this.id=id;
        this.tip=tip;
        this.trajanje=trajanje;
        this.klijent_prezime=klijent_prezime;
        this.maser_ime=maser_ime;
        this.klijent_ime=klijent_ime;
        this.dan=dan;
        this.mesec=mesec;
        this.godina=godina;
        this.cena=cena;
        this.vreme=vreme;
    }
    prikaz()
    {
        const main=document.getElementById("dodatak");
        main.innerHTML="";
        let lab=document.createElement("label");
        lab.innerHTML="Pronadjene su ove rezervacije: ";
        main.appendChild(lab);
        let br=document.createElement("br");
        main.appendChild(br);
        let masaza=document.createElement("div");
        masaza.className="masaza";
        let tekst=document.createElement("div");
        tekst.className="text";
        tekst.innerHTML="Masaza: "+this.tip+", Trajanje: "+
        this.trajanje+"min, Maser: "+this.maser_ime+" "+ ", Klijent: "+this.klijent_ime+
        " "+this.klijent_prezime+", Cena: "+this.cena+
        ", Vreme: "+this.vreme+" ,Datum: "+
        this.dan+"."+this.mesec+"."+this.godina+".";
        masaza.appendChild(tekst);
        main.appendChild(masaza);
    }
}
