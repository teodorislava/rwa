import * as Rxjs from 'rxjs';
import { IfObservable } from 'rxjs/observable/IfObservable';
import {Trening} from './klase.js';
import {Masaza} from './klase.js';

export class DatabaseService {
  constructor() 
  {
    
  }
  static prikazMasaza()
  {
    const url="http://localhost:3000/masaze";
    const obs=Rxjs.Observable.fromPromise(fetch(url).then(response=>
    {
        response.json().then(el=> 
        {
          el.forEach(el=>{
          let m=new Masaza(el.id, el.tip, el.trajanje, el.maser_ime,
          el.maser_prezime, el.klijent_ime, el.klijent_prezime,
          el.dan, el.mesec, el.godina, el.cena);
          m.prikaz();
          });
        });
    }));
  }
  static vratiMasaze()
  {
    const url="http://localhost:3000/masaze";
    return Rxjs.Observable.fromPromise(fetch(url).then(response=> response.json()));
  }
}
