import {Rezervacije} from './rezervacije';
import {Masaza} from './klase';
import {DatabaseService} from './database';
import * as Rxjs from 'rxjs';
import { IfObservable } from 'rxjs/observable/IfObservable';

export function  rezervacijaForma()
{  
    let br=document.createElement("br");
    let masaze=["sijacu", "regularna", "parcijalna", "stopala", "toplim kamenjem"];
    let trajanje=["30","45","60","75","90"];
    let maseri=["Mihajlo Djordjevic","Marija Stefanovic", "Petra Stankovic", "Stefan Ljubic", "Kostadin Srbinovic"];

    let div=document.getElementById("dodatak");
    div.innerHTML="";
    div.appendChild(br);
    let forma=document.createElement("div");
    div.appendChild(forma);

    let p=document.createElement("p");
    forma.appendChild(p);
    let label=document.createElement("label");
    label.innerHTML="Unesite vase ime: ";
    p.appendChild(label);
    let input=document.createElement("input");
    input.type="text";
    input.className="ime";
    p.appendChild(input);
    let p2=document.createElement("p");
    forma.appendChild(p2);
    let label2=document.createElement("label");
    label2.innerHTML="Unesite vase prezime: ";
    p2.appendChild(label2);
    let input2=document.createElement("input");
    input2.type="text";
    input2.className="prezime";
    p2.appendChild(input2);
    let p3=document.createElement("p");
    forma.appendChild(p3); 
    let label3=document.createElement("label");
    label3.innerHTML="Odaberite masera: ";
    p3.appendChild(label3);
    let select=document.createElement("select");
    select.className="maser";
    maseri.forEach(el=>{
        let option=document.createElement("option");
        option.name="maser_ime";
        option.value=el;
        option.innerHTML=el;
        select.appendChild(option);
    });
    p3.appendChild(select);

    let p4=document.createElement("p");
    forma.appendChild(p4); 
    let label4=document.createElement("label");
    label4.innerHTML="Odaberite tip masaze: ";
    p4.appendChild(label4);
    let select2=document.createElement("select");
    select2.className="tip";
    masaze.forEach(el=>{
        let option=document.createElement("option");
        option.name="tip";
        option.value=el;
        option.innerHTML=el;
        select2.appendChild(option);
    });
    p4.appendChild(select2);

    let p5=document.createElement("p");
    forma.appendChild(p5); 
    let label5=document.createElement("label");
    label5.innerHTML="Odaberite trajanje masaze: ";
    p5.appendChild(label5);
    let select3=document.createElement("select");
    select3.className="trajanje";
    trajanje.forEach(el=>{
        let option=document.createElement("option");
        option.name="trajanje";
        option.value=el;
        option.innerHTML=el+"min";
        select3.appendChild(option);
    });
    p5.appendChild(select3);
    
    let p6=document.createElement("p");
    forma.appendChild(p6); 
    let label6=document.createElement("label");
    label6.innerHTML="Odaberite datum: ";
    p6.appendChild(label6);
    let date=document.createElement("input");
    date.type="date";
    date.className="date";
    p6.appendChild(date);

    let p7=document.createElement("p");
    forma.appendChild(p7); 
    let label7=document.createElement("label");
    label7.innerHTML="Odaberite vreme: ";
    p7.appendChild(label7);
    let time=document.createElement("input");
    time.type="time";
    time.min="10:00:00";
    time.max="22:00:00";
    time.className="time";
    p7.appendChild(time);

    let submit=document.createElement("button");
    submit.innerHTML="Rezervisi";
    submit.onclick=function()
    {
        let ime=document.getElementsByClassName("ime");
        let prezime=document.getElementsByClassName("prezime");
        let maser=document.getElementsByClassName("maser");
        let tip=document.getElementsByClassName("tip");
        let trajanje=document.getElementsByClassName("trajanje");
        let time=document.getElementsByClassName("time");
        let date=document.getElementsByClassName("date");
        let cena=trajanje[0].value*20;
        let spl= date[0].value.split('-');
        let masaza=new Masaza(0, tip[0].value, trajanje[0].value,
        maser[0].value, ime[0].value, prezime[0].value, spl[2], spl[1],
        spl[0], time[0].value, cena);
        Rezervacije.RezervisiNovuMasazu(masaza);
    }
    forma.appendChild(submit);
}
export function funkcijaOdabir()
{
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let br=document.createElement("br");
    div.innerHTML="Odaberite operaciju:";
    div.appendChild(br);

    let p=document.createElement("p");
    div.appendChild(p);
    let radio=document.createElement("input");
    radio.type="radio";
    radio.name="operacija";
    radio.value="dodavanje";
    p.appendChild(radio);
    let labela=document.createElement("label");
    labela.innerHTML="Rezervacija masaze      ";
    p.appendChild(labela);

    
    let radio2=document.createElement("input");
    radio2.type="radio";
    radio2.name="operacija";
    radio2.value="azuriranje";
    p.appendChild(radio2);
    let labela2=document.createElement("label");
    labela2.innerHTML="Azuriranje masaze      ";
    p.appendChild(labela2);

    let radio3=document.createElement("input");
    radio3.type="radio";
    radio3.name="operacija";
    radio3.value="brisanje";
    p.appendChild(radio3);
    let labela3=document.createElement("label");
    labela3.innerHTML="Brisanje rezervacije";
    p.appendChild(labela3);

    let p4=document.createElement("p");
    div.appendChild(p4);
    let button=document.createElement("button");
    button.innerHTML="Odaberi";
    button.onclick=function()
    {
        let op=document.querySelectorAll("input:checked");
        console.log(op);
        if(op[0].value==="dodavanje")
            rezervacijaForma();
        else if(op[0].value==="brisanje")
            brisanje();
        else if(op[0].value==="azuriranje")
            azuriranje();
    }
    div.appendChild(button);
    div.appendChild(br);
}
export function brisanje()
{
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let br=document.createElement("br");
    div.appendChild(br);
    let input=document.createElement("input");
    input.type="text";
    input.className="ime";
    let labela=document.createElement("label");
    labela.innerHTML="Unesite ime i prezime: ";
    div.appendChild(labela);
    div.appendChild(input);
    let nadDiv=document.createElement("div");
    nadDiv.innerHTML="";
    div.appendChild(nadDiv);

    const url="http://localhost:3000/masaze";
    const obs = Rxjs.Observable.fromPromise(fetch(url).then(response => response.json()));

    Rxjs.Observable.fromEvent(input, "input")
    .debounceTime(500)
    .map(ev => ({tekst: ev.target.value }))
    .switchMap(text => obs)  
    .subscribe(masaze => {
        nadDiv.innerHTML="";
        masaze.forEach(el=>{
            let i=document.getElementsByClassName("ime");
            let pomString=el.klijent_ime+" "+el.klijent_prezime;
            if(i[0].value!=="" && pomString.indexOf(i[0].value)!==-1)
            {
                let d=document.createElement("div");
                nadDiv.appendChild(d);
                d.innerHTML="Masaza: "+el.tip+", Trajanje: "+
                el.trajanje+", Maser: "+el.maser_ime+" " + "Klijent: "+el.klijent_ime+
                " "+el.klijent_prezime+", Cena: "+el.cena+
                ", Vreme: "+el.vreme+" , Datum: "+el.dan+"."+el.mesec+"."+el.godina+".";
                d.className="brisanje";
                d.onclick=function()
                {
                    Rezervacije.obrisiMasazu(el);
                }
            
            }
        });  
    });
}
export function azuriranje()
{
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let br=document.createElement("br");
    div.appendChild(br);
    let input=document.createElement("input");
    input.type="text";
    input.className="ime";
    let labela=document.createElement("label");
    labela.innerHTML="Unesite ime i prezime: ";
    div.appendChild(labela);
    div.appendChild(input);
    let nadDiv=document.createElement("div");
    nadDiv.innerHTML="";
    div.appendChild(nadDiv);

    const url="http://localhost:3000/masaze";
    const obs = Rxjs.Observable.fromPromise(fetch(url).then(response => response.json()));

    Rxjs.Observable.fromEvent(input, "input")
    .debounceTime(500)
    .map(ev => ({tekst: ev.target.value }))
    .switchMap(text => obs)  
    .subscribe(masaze => {
        nadDiv.innerHTML="";
        masaze.forEach(el=>{
            let i=document.getElementsByClassName("ime");
            let pomString=el.klijent_ime+" "+el.klijent_prezime;
            if(i[0].value!=="" && pomString.indexOf(i[0].value)!==-1)
                {
                    let d=document.createElement("div");
                    nadDiv.appendChild(d);
                    d.innerHTML="Masaza: "+el.tip+", Trajanje: "+
                    el.trajanje+", Maser: "+el.maser_ime+" " + "Klijent: "+el.klijent_ime+
                    " "+el.klijent_prezime+", Cena: "+el.cena+
                    ", Vreme: "+el.vreme+" , Datum: "+el.dan+"."+el.mesec+"."+el.godina+".";
                    d.className="brisanje";
                    d.onclick=function()
                    {
                        izmenaForma(el);
                    }
                
                }
        });  
    });
}
export function  izmenaForma(m)
{  
    let br=document.createElement("br");
    let masaze=["sijacu", "regularna", "parcijalna", "stopala", "toplim kamenjem"];
    let trajanje=["30","45","60","75","90"];
    let maseri=["Mihajlo Djordjevic","Marija Stefanovic", "Petra Stankovic", "Stefan Ljubic", "Kostadin Srbinovic"];

    let div=document.getElementById("dodatak");
    div.innerHTML="";
    div.appendChild(br);
    let forma=document.createElement("div");
    div.appendChild(forma);

    let p=document.createElement("p");
    forma.appendChild(p);
    let label=document.createElement("label");
    label.innerHTML="Unesite vase ime: ";
    p.appendChild(label);
    let input=document.createElement("input");
    input.type="text";
    input.value=m.klijent_ime;
    input.className="ime";
    p.appendChild(input);
    let p2=document.createElement("p");
    forma.appendChild(p2);
    let label2=document.createElement("label");
    label2.innerHTML="Unesite vase prezime: ";
    p2.appendChild(label2);
    let input2=document.createElement("input");
    input2.type="text";
    input2.value=m.klijent_prezime;
    input2.className="prezime";
    p2.appendChild(input2);
    let p3=document.createElement("p");
    forma.appendChild(p3); 
    let label3=document.createElement("label");
    label3.innerHTML="Odaberite masera: ";
    p3.appendChild(label3);
    let select=document.createElement("select");
    select.className="maser";
    let i=0;
    maseri.forEach((el, ind) =>{
        let option=document.createElement("option");
        option.name="maser_ime";
        option.value=el;
        option.innerHTML=el;
        select.appendChild(option);
        if(el===m.maser_ime)
            i=ind;
    });
    select.selectedIndex=i;
    p3.appendChild(select);

    let p4=document.createElement("p");
    forma.appendChild(p4); 
    let label4=document.createElement("label");
    label4.innerHTML="Odaberite tip masaze: ";
    p4.appendChild(label4);
    let select2=document.createElement("select");
    select2.className="tip";
    let i2=0;
    masaze.forEach((el,ind) =>{
        let option=document.createElement("option");
        option.name="tip";
        option.value=el;
        option.innerHTML=el;
        select2.appendChild(option);
        if(el===m.tip)
            i2=ind;
    });
    select2.selectedIndex=i2;
    p4.appendChild(select2);

    let p5=document.createElement("p");
    forma.appendChild(p5); 
    let label5=document.createElement("label");
    label5.innerHTML="Odaberite trajanje masaze: ";
    p5.appendChild(label5);
    let select3=document.createElement("select");
    select3.className="trajanje";
    let i3=0;
    trajanje.forEach((el,ind) => {
        let option=document.createElement("option");
        option.name="trajanje";
        option.value=el;
        option.innerHTML=el+"min";
        select3.appendChild(option);
        if(el===m.trajanje)
            i3=ind;
    });
    select3.selectedIndex=i3;
    p5.appendChild(select3);
    
    let p6=document.createElement("p");
    forma.appendChild(p6); 
    let label6=document.createElement("label");
    label6.innerHTML="Odaberite datum: ";
    p6.appendChild(label6);
    let date=document.createElement("input");
    date.type="date";
    date.value=m.godina+"-"+m.mesec+"-"+m.dan;
    console.log(date);
    console.log(m);
    date.className="date";
    p6.appendChild(date);

    let p7=document.createElement("p");
    forma.appendChild(p7); 
    let label7=document.createElement("label");
    label7.innerHTML="Odaberite vreme: ";
    p7.appendChild(label7);
    let time=document.createElement("input");
    time.type="time";
    time.value=m.vreme;
    time.min="10:00:00";
    time.max="22:00:00";
    time.className="time";
    p7.appendChild(time);

    let submit=document.createElement("button");
    submit.innerHTML="Rezervisi";
    submit.onclick=function()
    {
        let ime=document.getElementsByClassName("ime");
        let prezime=document.getElementsByClassName("prezime");
        let maser=document.getElementsByClassName("maser");
        let tip=document.getElementsByClassName("tip");
        let trajanje=document.getElementsByClassName("trajanje");
        let time=document.getElementsByClassName("time");
        let date=document.getElementsByClassName("date");
        let cena=trajanje[0].value*20;
        let spl= date[0].value.split('-');
        console.log(spl);
        let masaza=new Masaza(m.id, tip[0].value, trajanje[0].value,
        maser[0].value, ime[0].value, prezime[0].value, spl[2], spl[1],
        spl[0], time[0].value, cena);
        Rezervacije.izmeniMasazu(masaza);
    }
    forma.appendChild(submit);
}