import {Trener} from './klase.js';
import {Trening} from './klase.js';
import { DatabaseService } from './database.js';
import {Rezervacije} from './rezervacije';
import {rezervacijaForma} from './prikaz';
import {funkcijaOdabir} from './prikaz';

let rez=new Rezervacije();

let funkcijaFitnes=function()
{
    let d=document.getElementById("dodatak");
    d.innerHTML="";
    let button=document.createElement("button");
    button.innerHTML="UspehUspehUspeliSmoJej!";
    d.appendChild(button);
}

let linkF=document.getElementById("fitnes");
linkF.onclick=function()
{
    funkcijaFitnes();
}

let linkO=document.getElementById("oNama");
linkO.onclick=function()
{
    funkcijaOnama();
}
let funkcijaOnama=function()
{
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let tekst=document.createElement("div");
    tekst.innerHTML="Dobrodošli! "+
    "Dobrodošli u DreamGym, prvi niški fitness-wellness centar namenjen prvenstveno damama."+
    "DreamGym je osnovan 2010. godine. Pored fitness programa, prvi uvodi wellness sadržaje u Nišu. Nudimo sve što je potrebno savremenoj ženi: najveći izbor sportskih programa, tretmana za negu tela, kao i opuštanje u našoj RELAX zoni."+
    "Naš cilj je da uz pomoć stručnih trenera, vrhunskih uslova za trening i efektivnog rada na sebi postignete maksimalan učinak u najkraćem mogućem roku."+
    "U našoj teretani radi čak 10 trenera i 5 masera, čije su usluge konstantno na raspolaganju.";
    tekst.className="welcome";
    div.appendChild(tekst);
}

let funkcijaRezervacija=function()
{
    let d=document.getElementById("dodatak");
    d.innerHTML="";
    rezervacijaForma();
}

let linkR=document.getElementById("rezervacijeM");
linkR.onclick=function()
{
    funkcijaOdabir();
}
