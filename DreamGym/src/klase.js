import {DatabaseService} from './database';
export class Trening
{
    constructor(id, tip, trajanje, trener_ime, trener_prezime,
        klijent_ime, klijent_prezime,
         dan, mesec, godina, cena)
    {
        this.id=id;
        this.tip=tip;
        this.trajanje=trajanje;
        this.klijent_prezime=klijent_prezime;
        this.trener_prezime=trener_prezime;
        this.trener_ime=trener_ime;
        this.klijent_ime=klijent_ime;
        this.dan=dan;
        this.mesec=mesec;
        this.godina=godina;
        this.cena=cena;
    }
}
export class Masaza
{
    constructor(id, tip, trajanje, maser_ime,
        klijent_ime, klijent_prezime,
         dan, mesec, godina, vreme, cena)
    {
        this.id=id;
        this.tip=tip;
        this.trajanje=trajanje;
        this.klijent_prezime=klijent_prezime;
        this.maser_ime=maser_ime;
        this.klijent_ime=klijent_ime;
        this.dan=dan;
        this.mesec=mesec;
        this.godina=godina;
        this.cena=cena;
        this.vreme=vreme;
    }
    prikaz()
    {
        const main=document.getElementById("zakazani");
        let masaza=document.createElement("div");
        masaza.className="masaza";
        let vreme=document.createElement("time");
        vreme.dateTime=this.dan+"."+this.mesec+"."+this.godina+".";
        let tekst=document.createElement("div");
        tekst.className="text";
        tekst.innerHTML="Masaza: "+this.tip+", Trajanje: "+
        this.trajanje+", Trener: "+this.maser_ime+" "+
        this.maser_prezime+", Klijent: "+this.klijent_ime+
        " "+this.klijent_prezime+", Cena: "+this.cena+
        ", Vreme: ";
        masaza.appendChild(tekst);
        masaza.appendChild(vreme);
        main.appendChild(masaza);
    }
    
}
