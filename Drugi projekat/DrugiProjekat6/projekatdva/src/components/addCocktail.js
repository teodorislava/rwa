import React, {Component} from 'react';

class AddCocktail extends Component
{
    constructor(props)
    {
        super(props);
        this.state={komentar:"", korisnik:"", vreme:""}
        this.handleUsername = this.handleUsername.bind(this);
        this.handleKomentar = this.handleKomentar.bind(this);
        this.handleVreme = this.handleVreme.bind(this);
    }
    render()
    {
        return(
            <div>
                <h3>U zavisnosti od tezine koktela koji ste napravili i vremena koje vam je trebalo za to bice vam dodeljen odgovarajuci broj poena, proverite svoj profi, mozda ste dobili novu titulu ;) </h3>
                <p><input value={this.state.korisnik} type="text" placeholder="Username" className="naslovClanka" onChange={this.handleUsername} /></p>
                <textarea value={this.state.komentar} placeholder="Vas komentar" className="sadrzajClanka" onChange={this.handleKomentar}></textarea>
                <p><input value={this.state.vreme} type="text" placeholder="Vreme pripreme" className="naslovClanka" onChange={this.handleVreme} /></p>
                <button onClick={() => this.props.clicked(this.state)}> Dodaj utisak </button>
            </div>
        )
    }
    handleUsername(event)
    {
        this.setState({korisnik:event.target.value});
    }
    handleKomentar(event)
    {
        this.setState({komentar:event.target.value});
    }
    handleVreme(event)
    {
        this.setState({vreme:event.target.value});
    }
}
export default AddCocktail;