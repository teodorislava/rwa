import React, {Component} from 'react';

class StartPageAdmin extends Component 
{
    render()
    {
        return(
            <div id="pocetna">
                <h1>Mixology</h1>
                <nav>
                    <li>
                        <a className="navigacija" id="start" href="/pocetnaAdmin"> Pocetna </a>
                        <a className="navigacija" id="kokteli" href="/article"> Dodaj clanak</a>
                        <a className="navigacija" id="odjava" href="/odjava"> Odjavite se</a>
                    </li>
                </nav>
                <br />
                <div id="sadrzaj">
                    <img className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2018/05/03111734/9-Great-Cheap-Bottles-That-Bartenders-Swear-By-720x720-article.jpg" /> 
                    <img className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2018/05/10121148/banana-leaf-coasters_article_720x720.jpg" />
                    <img className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2017/07/13090635/teal-quila-sunrise-720x720-recipe.jpg" />
                </div>
            </div>
        )
    }
} 
export default StartPageAdmin; 