import React, {Component} from 'react';
import { readArticles } from '../store/actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import StartPage from './startpage';
import Article from './article';

class ReadArticles extends Component
{
    render()
    {
        console.log(this.props.articles.data);
        if(this.props.articles.viewArticle)
            return(<div>
                    <StartPage></StartPage> 
                    {this.props.articles.data.data.map((a, index) => {
                        return (
                            <Article article={a} key={index}>
                            </Article>
                        )
                    })}
                </div>);

        return (<div><StartPage></StartPage><button onClick={()=>this.props.returnArticles()}> Izlistaj clanke </button></div>)
    }
}
function mapStateToProps(state)
{
    return { 
        articles:state.articles
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({returnArticles:readArticles}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ReadArticles);