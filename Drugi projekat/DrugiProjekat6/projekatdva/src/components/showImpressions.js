import React, {Component} from 'react';

class Impression extends Component{
    constructor(props)
    {
        super(props);
        this.state = {utisak:this.props.utisak.utisak, vreme:this.props.utisak.vreme, id:this.props.utisak.id, username:this.props.utisak.username};
        this.handleUtisak = this.handleUtisak.bind(this);
        this.handleVreme = this.handleVreme.bind(this);
    }
    render()
    {
        if(this.props.utisak.username === this.props.user.user)
            return (<div>
                        <article>
                            <h2>Korisnik: {this.props.utisak.username}</h2>
                            <textarea type="text" value={this.state.utisak} className="sadrzajClanka"
                                onChange={this.handleUtisak}> </textarea>
                            <p>Vreme utroseno za pripremu:</p> 
                            <p><input className="naslovClanka" value={this.state.vreme} onChange={this.handleVreme} type="text" /> </p> 
                            <p><button onClick={() => this.props.clickedIzmena(this.state)}> Izmeni </button> 
                               <button onClick={() => this.props.clickedBrisanje(this.props.utisak.id)}>Obrisi</button></p>
                        </article>
                    </div>)
        else
        {
            if(this.props.profil==="ne")
                return (
                    <div>
                        <article>
                                <h2>Korisnik: {this.props.utisak.username}</h2>
                                {this.props.utisak.utisak}
                                <p>Vreme utroseno za pripremu: {this.props.utisak.vreme}</p>
                        </article>
                    </div>);
            if(this.props.profil==="da")
            {
                return( <div>
                    <article>
                            <h2>Koktel: {this.props.utisak.koktel}</h2>
                            {this.props.utisak.utisak}
                            <p>Vreme utroseno za pripremu: {this.props.utisak.vreme}</p>
                    </article>
                    </div>);
            }
        }
    }
    handleVreme(event)
    {
        this.setState({vreme:event.target.value});
    }
    handleUtisak(event)
    {
        this.setState({utisak:event.target.value}); 
    }
}
export default Impression;