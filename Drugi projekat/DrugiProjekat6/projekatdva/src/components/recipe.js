import React, {Component} from 'react';
import { of } from 'rxjs/observable/of';
import { mapTo, delay } from 'rxjs/operators';
import {connect} from 'react-redux';
import { concat} from 'rxjs';
import {step, stepCount} from '../store/actions';
import {bindActionCreators} from 'redux';
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/interval";
import 'rxjs/add/observable/range';
import { timer } from 'rxjs/observable/timer';


class Recipe extends Component
{
    render()
    {
        console.log(this.props.step);

        if(this.props.step.viewStepCount)
        {
            return(<div>
                <h2 className="korak"> {this.props.step.data.step.opis}</h2>
                <h3>Korak traje: {this.props.step.data.step.vreme} sekundi. Trenutno: {this.props.step.data.count} sekundi </h3>
               </div>) 
        }

        if(this.props.step.viewStep)
        {
            this.countdown(this.props.step.data);
            return(<div>
                    <h2 className="korak"> {this.props.step.data.step.opis}</h2>
                    <h3>Korak traje: {this.props.step.data.step.vreme} sekundi. Trenutno: {this.props.step.data.count} sekundi </h3>
                </div>) 
        }
        return(
                <div>
                {this.renderSteps(this.props.recept.data.steps)}
                </div>
        );
        
    }
    countdown(step)
    {
        Observable.interval(1000)
        .take(step.step.vreme)
        .subscribe(val => this.props.showStepSpecial(val, step.step));
    }
    renderSteps(steps)
    {
        const prikaz = of(null);
        switch(steps.length)
        {
            case 1:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!',vreme:steps[0].vreme*1000}), delay(steps[0].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            case 2:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!',vreme:steps[1].vreme*1000}), delay(steps[1].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            case 3:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!', vreme:steps[2].vreme*1000}), delay(steps[2].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            case 4:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!',vreme:steps[3].vreme*1000}), delay(steps[3].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            case 5:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4]), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!',vreme:steps[4].vreme*1000}), delay(steps[4].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            case 6:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4]), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo(steps[5]), delay(steps[4].vreme*1000)),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!',vreme:steps[5].vreme*1000}), delay(steps[5].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            case 7:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4]), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo(steps[5]), delay(steps[4].vreme*1000)),
                    prikaz.pipe(mapTo(steps[6]), delay(steps[5].vreme*1000)),
                    prikaz.pipe(mapTo({opis:'Uzivajte u vasem koktelu!!',vreme:steps[6].vreme*1000}), delay(steps[6].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(0, val));
                break;
            }
            default:
                break;
        }
    }
}
function mapStateToProps(state)
{
    return { recept:state.recipe, step:state.step }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({showStep:step, showStepSpecial:stepCount}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Recipe);