import React, {Component} from 'react';
import { putUser } from '../cocktail.service';

class Logout extends Component
{
    render()
    {
        putUser("", "false");
        return(<Redirect to="/"></Redirect>);
    }
}
export default Logout;