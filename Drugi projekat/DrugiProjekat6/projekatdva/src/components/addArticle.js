import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { addArticle } from '../store/actions';
import StartPageAdmin from './startPageAdmin';
class AddArticle extends Component
{
    constructor(props)
    {
        super(props);
        this.state={naslov:"", sadrzaj:""}
        this.handleNaslov = this.handleNaslov.bind(this);
        this.handleSadrzaj = this.handleSadrzaj.bind(this);
    }
    render()
    {
        console.log("seljacki dibager"); //proveri status i prikazi poy
        console.log(this.props.response);
        if(this.props.response.viewResponse)
        {
            if(this.props.response.data.status===200 || this.props.response.data.status===201)
            {
                return(<div><StartPageAdmin></StartPageAdmin><div><h2> Dodavanje clanka uspesno! </h2></div></div>);
            }
            else
            {
                return(
                <div>
                    <StartPageAdmin></StartPageAdmin>
                    <p> <input value={this.state.naslov} type="text" className="naslovClanka" name="naslov" placeholder="Naslov" /> </p>
                    <textarea value={this.state.sadrzaj} className="sadrzajClanka" name="sadrzaj" placeholder="Sadrzaj"></textarea>
                    <p> <button name="submit" onClick={()=>this.submit()}> Dodaj clanak </button> </p>
                    <h2> Dodavanje clanka neuspesno! </h2>
                </div>);
            }
        }
        return(
            <div>
                <StartPageAdmin></StartPageAdmin>
                <p> <input type="text" className="naslovClanka" onChange={this.handleNaslov} placeholder="Naslov" /> </p>
                <textarea className="sadrzajClanka" placeholder="Sadrzaj" onChange={this.handleSadrzaj}></textarea>
                <p> <button name="submit" onClick={()=>this.submit()}> Dodaj clanak </button> </p>
            </div>
        )
    }
    handleNaslov(event)
    {
        this.setState({naslov:event.target.value});
    }
    handleSadrzaj(event)
    {
        this.setState({sadrzaj:event.target.value});
    }
    submit()
    {
        this.props.submit(this.state.naslov, this.state.sadrzaj);
    }
}
function mapStateToProps(state)
{
    return { 
        response:state.responseArticle
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({submit:addArticle}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(AddArticle);