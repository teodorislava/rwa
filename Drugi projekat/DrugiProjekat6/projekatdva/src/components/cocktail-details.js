import React, {Component} from 'react';
import {recipe, readImpression, addImpression, removeImpression} from '../store/actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Recipe from './recipe';
import AddCocktail from './addCocktail';
import Impression from './showImpressions';
import {updateImpression} from '../cocktail.service';
import {Redirect} from 'react-router-dom';

class CocktailDetails extends Component
{
    render()
    {
        console.log(this.props.recept);
        return(
            <div>
                {this.renderDetails()}
            </div>
        )
    }
    renderDetails()
    {
        if(this.props.recept.viewRecipe)
            return (<Recipe handle={()=>this.end()}></Recipe>)
        return (
            <div>
                <div className="cocktailRecipe">
                    <div className="CocktailInfo">
                    <h1>{this.props.koktel.naziv}</h1>
                    <h3>Tip case u kojoj je preporuceno sluziti koktel: {this.props.koktel.tip_case} </h3>
                    <h3>Sastojci: </h3>
                    <ul>
                        {this.props.koktel.sastojci.map((el, index) => {
                            return (
                                <li key={index} className="sastojak">Ime sastojka:   {el.ime}    Kolicina:   {el.kolicina}</li>
                            )
                        })}
                    </ul>
                    <h3>Vreme potrebno za pripremu koktela: {this.props.koktel.vreme_pripreme} min.</h3>
                    <h3>Broj koraka za pripremu koktela: {this.props.koktel.broj_koraka}</h3>
                    <h3>Popularnost koktela: {this.props.koktel.rejting} </h3>
                    <h3> Pripremite se, tutorijal pocinje na klik sledeceg dugmeta!</h3>
                    <button onClick={() => this.props.recipe(this.props.koktel.koraci)}> Zapocni pripremu! </button>
                    </div>
                    <img className="cocktailImg" src={this.props.koktel.img} /> 
                </div>
                <div className="utisciDiv">
                {this.props.utisci.filter(el => (el.koktel===this.props.koktel.naziv)).map((element, index) =>
                    {
                        return (
                            <div key={index}>
                            <Impression profil="ne" utisak={element} user={this.props.user}
                            clickedBrisanje={()=> this.props.removeImpression(this.props.koktel, this.props.user, element.id)}
                            clickedIzmena={(utisak) => this.UpdateImpression(utisak, this.props.koktel)}></Impression>
                            </div>
                        )
                    })
                }
                </div>
                <div className="add-cocktail-container">
                    <h2>Ukoliko ste i vi napravili ovaj koktel dodajte vase utiske! </h2>
                    <AddCocktail 
                    koktel={this.props.koktel} 
                    user={this.props.user}
                    clicked={(utisak)=> this.props.addImpression(utisak, this.props.koktel, this.props.user)}></AddCocktail>
                </div>
            </div>
        )
    }
    UpdateImpression(impr, koktel)
    {
        updateImpression(impr, koktel);
        alert("Uspesna izmena utiska!")
    }
    end()
    {
        return(<Redirect from="/kokteli" to="/pocetna"></Redirect>)
    }
}
function mapStateToProps(state)
{
    return { 
        recept:state.recipe
     }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({recipe:recipe, addImpression: addImpression, removeImpression:removeImpression}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(CocktailDetails);