import React, {Component} from 'react';

class Register extends Component
{
    render()
    {
        return(
            <div id="registracija">
                <h2>Unesite vase informacije: </h2>
                <p><input type="text" name="ime" placeholder="Ime"/></p>
                <p><input type="text" name="prezime" placeholder="Prezime"/></p>
                <p><input type="number" name="godine" placeholder="Godine" /></p>
                <p><input type="text" name="username" placeholder="Korisnicko ime"/></p>
                <p><input type="email" name="mail" placeholder="E-mail"/></p>
                <p><input type="password" name="pass" placeholder="Sifra"/></p>
                <p><input type="password" name="passdva" placeholder="Ponovite sifru"/></p>
                <p><button name="submit" onClick={() => this.submit()}> Registruj se</button></p>
            </div>
        )
    }
    submit()
    {
        let ime=document.getElementsByName("ime")[0].value;
            let prezime=document.getElementsByName("prezime")[0].value;
            let username=document.getElementsByName("username")[0].value;
            let mail=document.getElementsByName("mail")[0].value;
            let pass=document.getElementsByName("pass")[0].value;
            let passdva=document.getElementsByName("passdva")[0].value;
            let godine=document.getElementsByName("godine")[0].value;
            if(pass!==passdva)
            {
                alert("Sifre se ne podudaraju!");
                return;
            }
            if(godine<18)
            {
                alert("Registracija na ovom sajtu je moguca samo za punoletna lica!");
                return;
            }
            fetch('http://localhost:4000/korisnici', {
            method: 'post',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({ime:ime,prezime:prezime,godine:godine,username:username,mail:mail,password:pass})
            }).then(res=> 
            {
                    res.json();
                    if(res.status===201 || res.status===200)
                    {
                        alert("Rezervacija uspesna!");
                    }
                    else
                    {
                        alert("Rezervacija neuspesna!");
                    }
            });
    }
}
export default Register;