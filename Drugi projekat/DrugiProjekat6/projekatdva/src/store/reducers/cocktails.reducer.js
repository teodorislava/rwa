import {COCKTAILS_RECEIVE} from '../actions';

export default function(state={view:false}, action)
{
    switch(action.type)
    {
        case COCKTAILS_RECEIVE:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {view:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};