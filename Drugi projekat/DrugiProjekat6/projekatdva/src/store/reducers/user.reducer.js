import {RETURN_USER} from '../actions';

export default function(state={viewUser:false}, action)
{
    switch(action.type)
    {
        case RETURN_USER:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewUser:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};