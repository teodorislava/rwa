import {RETURN_ARTICLES} from '../actions';

export default function(state={viewArticle:false}, action)
{
    switch(action.type)
    {
        case RETURN_ARTICLES:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewArticle:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};