import {ADD_ARTICLE_RESPONSE} from '../actions';

export default function(state={viewResponse:false}, action)
{
    switch(action.type)
    {
        case ADD_ARTICLE_RESPONSE:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewResponse:true, data:action.payload.response});
        }
        default:
            return state;
    }
    return state;
};