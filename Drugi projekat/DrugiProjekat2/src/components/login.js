import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login, loginInit, notifyLog} from '../store/actions';
import StartPage from './startpage';

class Login extends Component
{
    constructor(props)
    {
        super(props);
        this.state={log:false, user:""};
    }
    render()
    {
        let loggedIn=this.log();
        if(loggedIn)
            return (<StartPage user={this.state.username}></StartPage>);
        return(
            <div>
                <p><input type="text" name="username" placeholder="Korisnicko ime" /></p>
                <p><input type="password" name="pass" placeholder="Sifra" /></p>
                <p><button onClick={() => this.props.submit()}> Prijavi se </button></p>
            </div>
        )
    }
    log()
    {
        if(this.state.log)
            return true;
        let inu = document.getElementsByName("username")[0];
        let inp = document.getElementsByName("pass")[0];
        if(this.props.logIn.log===false)
            return false;
        let f = this.props.logIn.data.filter(el => (el.username===inu.value && el.password === inp.value));
        if(f.length===1)
            this.setState({log:true, user:f[0].username});
        this.props.notify();
    }
}

function mapStateToProps(state)
{
    return { 
        logIn: state.login
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({submit:loginInit, notify:notifyLog}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);