import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login, loginInit} from '../store/actions';
import Cocktail from './cocktail';
import {getCocktails, getCocktail} from '../store/actions';
import CocktailDetails from './cocktail-details';

class LoginRegister extends Component
{
    constructor(props)
    {
        super(props);
    }
    render()
    {
        return(
            <div className="start">
                <a href="/login">Prijavite se</a>
                <a href="/register">Registrujte se </a>
            </div>
        )
    }
}
export default LoginRegister;