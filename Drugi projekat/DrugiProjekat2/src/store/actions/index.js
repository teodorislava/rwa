export const LOGIN_REQUEST ="LOGIN_REQUEST";
export const LOGIN ="LOGIN";
export const NOTIFY="NOTIFY";
export const COCKTAILS_REQUEST="COCKTAILS_REQUEST";
export const COCKTAILS_RECEIVE="COCKTAILS_RECEIVE";
export const COCKTAIL_REQUEST="COCKTAIL_REQUEST";
export const COCKTAIL_RECEIVE="COCKTAIL_RECEIVE";
export const RECIPE="RECIPE";
export const STEP="STEP";
export const REGISTER="REGISTER";

export function login(data)
{
    return {
        type:LOGIN, 
        payload:data
    }
}
export function loginInit()
{
    return{
        type:LOGIN_REQUEST
    }
}

export function notifyLog()
{
    return {
        type:NOTIFY
    }
}
export function getCocktails(data)
{
    return{
        type:COCKTAILS_REQUEST,
        payload:data
    }
}
export function receiveCocktails(data)
{
    return {
        type:COCKTAILS_RECEIVE,
        payload: data
    }
}
export function getCocktail(group, id)
{
    console.log(group, id);
    return {
        type:COCKTAIL_REQUEST,
        payload: {group:group, id:id}
    }
}
export function receiveCocktail(data)
{
    console.log(data);
    return {
        type:COCKTAIL_RECEIVE,
        payload: data
    }
}
export function recipe(steps)
{
    return {
        type:RECIPE,
        payload: {steps:steps}
    }
}
export function step(s)
{
    return {
        type:STEP,
        payload:{step:s}
    }
}
export function register(obj)
{
    return {
        type:REGISTER,
        payload:obj
    }
}