import {LOGIN} from '../actions';

export default function(state={log:false}, action)
{
    switch(action.type)
    {
        case LOGIN:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {log:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};