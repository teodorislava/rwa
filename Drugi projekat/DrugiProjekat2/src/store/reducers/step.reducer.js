import {STEP} from '../actions';

export default function(state={viewStep:false}, action)
{
    switch(action.type)
    {
        case STEP:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewStep:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};