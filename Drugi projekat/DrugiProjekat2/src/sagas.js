import {call, put, takeEvery, takeLatest} from 'redux-saga/effects';
import {LOGIN, login, LOGIN_REQUEST, getCocktails, receiveCocktails, COCKTAILS_REQUEST, receiveCocktail, COCKTAIL_REQUEST} from './store/actions';
import {all, fork} from 'redux-saga/effects';
import {fetchData, fetchCocktails, fetchCocktail} from './cocktail.service.js';

function* getApiData(action)
{
    try{
        const data = yield call(fetchData);
        yield put(login(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaLogin()
{
    yield takeLatest(LOGIN_REQUEST, getApiData);
}

function* getCocktailsData(action)
{
    try{
        const data = yield call(fetchCocktails, action.payload);
        yield put(receiveCocktails(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaCocktails()
{
    yield takeLatest(COCKTAILS_REQUEST, getCocktailsData);
}

function* getCocktailData(action)
{
    try{
        console.log(action);
        const data = yield call(fetchCocktail, action.payload.group, action.payload.id);
        console.log(data);
        yield put(receiveCocktail(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaCocktail()
{
    yield takeLatest(COCKTAIL_REQUEST, getCocktailData);
}

export default function* root() {
    yield all([
      fork(sagaLogin), 
      fork(sagaCocktails), 
      fork(sagaCocktail)
    ])
}