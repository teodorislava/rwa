import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {Router, Route, Switch} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import reducer from './store/reducers';
import Login from './components/login';
import Cocktails from './components/cocktails';
import StartPage from './components/login-register';
import Recipe from './components/recipe';
import Register from './components/register';
import Pocetna from './components/startpage';
import AboutUs from './components/aboutus';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
  );
sagaMiddleware.run(rootSaga);
store.runSaga = sagaMiddleware.run;
export let history = new  createBrowserHistory();
ReactDOM.render((
    <Provider store={store}> 
        <Router history={history}>
            <Switch>
                <Route path='/login' component={Login} />
                <Route path='/kokteli' component={Cocktails} />
                <Route path='/register' component={Register} />
                <Route path='/pocetna' component={Pocetna} />
                <Route path='/aboutus' component={AboutUs} />
                <Route path='/' component={StartPage} />
            </Switch>
        </Router>
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
