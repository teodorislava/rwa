export const fetchData =  async () => {
    try{
        const response = await fetch('http://localhost:4000/korisnici');
        const data = await response.json();
        const responseA= await fetch('http://localhost:4000/admini');
        const dataA=await responseA.json();
        return Object.assign({}, {korisnici:data, admini:dataA});
    }
    catch(e)
    {
        console.log(e);
    }
}
export const fetchCocktails = async (tip) => {
    try{
        const response = await fetch('http://localhost:4000/'+tip);
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const fetchCocktail = async (tip, id) => {
    try{
        const response = await fetch('http://localhost:4000/'+tip+'/'+id);
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const putArticle = async (naslov, sadrzaj) => {
    try {
        const response = await fetch('http://localhost:4000/clanci',  {method: 'post',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({sadrzaj:sadrzaj,naslov:naslov})});
        console.log(response);
        return response;
    }
    catch(e)
    {
        console.log(e);
    }
}

export const readArticles = async () => {
    try{
        const response = await fetch('http://localhost:4000/clanci');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}  

export const fetchImpressions =  async () => {
    try
    {
        const response = await fetch('http://localhost:4000/utisci');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const putImpression = async (impr, koktel) => {
    try
    {
        const response = await fetch('http://localhost:4000/utisci', {
            method: 'post',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({tezina:koktel.tezina, username:impr.korisnik,utisak:impr.komentar, vreme:impr.vreme, koktel:koktel.naziv, tip:koktel.tip})
        });
    }
    catch(e)
    {

    }
}

export const updateImpression = async (impr, koktel) => {
    try
    {
        const response = await fetch('http://localhost:4000/utisci/'+impr.id, {
            method: 'put',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({username:impr.username,utisak:impr.utisak, vreme:impr.vreme, koktel:koktel.naziv, tip:koktel.tip})
        });
        const data = await response.json();
        console.log(data);
        return data;
    }
    catch(e)
    {

    }
}

export const removeImpression = async (id) => {
    try
    {
        const response = await fetch('http://localhost:4000/utisci/'+id, {
            method: 'delete',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            }
        });
        const data = await response.json();
        console.log(data);
        return data;
    }
    catch(e)
    {

    }
}

export const readImpressions = async () => {
    try {
        const response = await fetch('http://localhost:4000/utisci');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const putUser = async (user, admin, logged) => {
    try {
        const response = await fetch('http://localhost:4000/log/1',  {method: 'put',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({user:user, admin:admin, logged:logged})});
        return response;
    }
    catch(e)
    {
        console.log(e);
    }
}

export const getUser = async () => {
    try {
        const response = await fetch('http://localhost:4000/log/1');
        const data = await response.json();
        console.log(data);
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
