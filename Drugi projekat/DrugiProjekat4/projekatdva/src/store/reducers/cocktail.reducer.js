import {COCKTAIL_RECEIVE, RETURN_IMPRESSIONS, REMOVE_IMPRESSION_RESPONSE} from '../actions';

export default function(state={viewCocktail:false, viewResponse:false}, action)
{
    switch(action.type)
    {
        case COCKTAIL_RECEIVE:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewCocktail:true, viewResponse:false, data:action.payload});
        }
        case RETURN_IMPRESSIONS:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewCocktail:true, viewResponse:false, data:action.payload});
        }
        case REMOVE_IMPRESSION_RESPONSE:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewCocktail:true, viewResponse:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};