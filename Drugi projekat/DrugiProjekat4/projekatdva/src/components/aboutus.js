import React, {Component} from 'react';
import StartPage from './startpage';

class AboutUs extends Component
{
    render() 
    {
        return(
            <div>
                <StartPage></StartPage>
                Dobrodosli! Ova stranica je namenjena svima koji zele da nauce kako da naprave fenomenalne koktele! 
                Ukoliko imate nalog ulogujte se, u suprotnom kreirajte nalog i pokrenite zabavu! Podsecamo vas da se na sajt mogu prijaviti samo osobe starije od 18 godina!
            </div>
        )
    }
}
export default AboutUs;