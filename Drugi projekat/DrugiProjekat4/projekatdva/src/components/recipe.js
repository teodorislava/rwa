import React, {Component} from 'react';
import { of } from 'rxjs/observable/of';
import { mapTo, delay } from 'rxjs/operators';
import {connect} from 'react-redux';
import { concat} from 'rxjs';
import {step} from '../store/actions';
import {bindActionCreators} from 'redux';
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/interval";
import 'rxjs/add/observable/range';
import { timer } from 'rxjs/observable/timer';


class Recipe extends Component
{
    render()
    {
        if(this.props.step.viewStep)
        {
            const val = this.countdown(this.props.step.data.step);
            console.log(val);
            return(<div>
                    <h2 className="korak">{this.props.step.data.step.opis}</h2>
                    <h3>Korak traje: {this.props.step.data.step.vreme} sekundi. Preostalo vreme: {val}</h3>
                   </div>) 
        }
        return(
                <div>
                {this.renderSteps(this.props.recept.data.steps)}
                </div>
        );
        
    }
    countdown(step)
    {
        Observable.interval(1000)
        .map(val => step.vreme-val)
        .subscribe(val => this.props.showStep(step));
    }
    renderSteps(steps)
    {
        const prikaz = of(null);
        console.log(steps);
        switch(steps.length)
        {
            case 1:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[0].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 2:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[1].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 3:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[2].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 4:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[3].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 5:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4]), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[4].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 6:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4]), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo(steps[5]), delay(steps[4].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[5].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 7:{
                console.log("shvatio sam da ima sedam koraka");
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo(steps[1]), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2]), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3]), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4]), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo(steps[5]), delay(steps[4].vreme*1000)),
                    prikaz.pipe(mapTo(steps[6]), delay(steps[5].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[6].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            default:
                break;
        }
    }
}
function mapStateToProps(state)
{
    return { recept:state.recipe, step:state.step }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({showStep:step}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Recipe);