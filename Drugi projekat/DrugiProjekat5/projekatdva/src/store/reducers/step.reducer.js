import {STEP, STEP_COUNT} from '../actions';

export default function(state={viewStep:false, viewStepCount:false}, action)
{
    switch(action.type)
    {
        case STEP:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewStep:true, viewStepCount:false, data:action.payload});
        }
        case STEP_COUNT:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewStepCount:true, viewStep:false, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};