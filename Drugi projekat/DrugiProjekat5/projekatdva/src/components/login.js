import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login, loginInit, notifyLog} from '../store/actions';
import StartPage from './startpage';
import {Redirect} from 'react-router-dom';
import { putUser } from '../cocktail.service';
import { pipe } from 'rxjs';

class Login extends Component
{
    constructor(props)
    {
        super(props);
        this.state={ log:false, user:"", privilegovani:false};
    }
    render()
    {
        this.log();
        if(this.state.privilegovani)
        {
            return (<Redirect from="/login" to="/pocetnaAdmin" />);
        }
        if(this.state.log)
        {
            return (<Redirect from="/login" to="/pocetna" />);
        }
        return(
            <div id="login">
                <p><input type="text" name="username" placeholder="Korisnicko ime" /></p>
                <p><input type="password" name="pass" placeholder="Sifra" /></p>
                <p><button onClick={() => this.props.submit()}> Prijavi se </button></p>
            </div>
        )
    }
    log()
    {
        //upisi u bazu ko se ulogovao

        let inu = document.getElementsByName("username")[0];
        let inp = document.getElementsByName("pass")[0];

        if(this.props.logIn.log===false)
            return false;
        
        let a= this.props.logIn.data.admini.filter(el => (el.username===inu.value && el.password === inp.value));
        if(a.length===1)
        {
            this.setState({privilegovani:true, user:a[0].username});
        }

        let f = this.props.logIn.data.korisnici.filter(el => (el.username===inu.value && el.password === inp.value));
        if(f.length===1)
            this.setState({log:true, user:f[0].username});

        if(this.state.log===true)
            putUser(this.state.user, "false");
        else if(this.state.privilegovani===true)
            putUser(this.state.user, "true");
    }
}

function mapStateToProps(state)
{
    return { 
        logIn: state.login
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({submit:loginInit}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);