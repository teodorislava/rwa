import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login, loginInit, readImpression} from '../store/actions';
import Cocktail from './cocktail';
import {getCocktails, getCocktail, readUser} from '../store/actions';
import CocktailDetails from './cocktail-details';
import StartPage from './startpage';


class Cocktails extends Component
{
    constructor(props)
    {
        super(props);
        this.state = 
        { 
            kokteli:["Bloody Mary", "Martini", "Manhattan", "Daiquiri", "Vodka", "Bourbon", "Gin", "Shots", "Margarita", "Punch", "Champagne", "Tequila"],
            baza:["bloodymary", "martini", "manhattan", "daiquiri", "vodka", "bourbon", "gin", "shots", "margarita", "punch", "champagne", "tequila"],
            user:"", admin:"false", logged:"false"
        };
    }
    render()
    {   //treba da se izmeni da se zna kad je start page kad admin start page!!!
        if(this.props.cocktail.viewCocktail===true)
        {
            return(
                <div>
                    <StartPage></StartPage>
                    <CocktailDetails
                    utisci={this.props.cocktail.data.impressions}
                    koktel={this.props.cocktail.data.cocktail}
                    user={this.props.cocktail.data.user}>
                    </CocktailDetails>
                </div>
            );
        }
        
        if(this.props.cocktails.view===true)
            return(
                <div>
                    <StartPage></StartPage>
                    {this.props.cocktails.data.map((el, index) => {
                        return (
                            <Cocktail
                                key = {index}
                                naslov = {el.naziv}
                                clicked = {() => this.props.getCocktail(el.tip, el.id)}>
                            </Cocktail>
                        )
                    })}
                </div>
            );
        
        return(
            <div>
                <StartPage></StartPage>
                <h3>Unesite grupu koktela o kojoj zelite da saznate vise: </h3>
                <div>
                    {this.state.kokteli.map((title, index) => {
                        return (
                            <Cocktail
                                key={index}
                                naslov={title}
                                clicked= {() => this.props.getCocktails(this.state.baza[index])}>
                            </Cocktail>
                        )
                    })}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state)
{
    console.log(state);
    return {  
        cocktails: state.cocktails,
        cocktail: state.cocktail
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getCocktails:getCocktails, getCocktail:getCocktail}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);