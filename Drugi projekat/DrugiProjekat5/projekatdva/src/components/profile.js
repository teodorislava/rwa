import React, {Component} from 'react';
import { readUser, returnImpressions, loginInit } from '../store/actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import StartPage from './startpage';
import "rxjs/add/operator/take";

class Profile extends Component
{   
    constructor(props)
    {
        super(props);
        this.state={log:true, user:"", poeni:-1, titula:"", utisci:""};
    }
    componentWillMount()
    {
        this.getLoggedUser(this.props.user.viewUser);
    }
    render()
    { 
        this.points(this.props.user, this.props.user.viewUser);
        if(this.props.user.viewUser)
        {
            return (
                <div>
                    <StartPage></StartPage>
                    <div>
                        <h2>Vas profil</h2>
                        <p>Ime: {this.state.user.ime}</p>
                        <p>Prezime: {this.state.user.prezime}</p>
                        <p>Godine: {this.state.user.godine}</p>
                        <p>Mail: {this.state.user.mail}</p>
                        <p>Poeni: {this.state.poeni}</p>
                        <p>Titula: {this.state.titula.naziv}</p>
                    </div>
                </div>);
        }
        else
            return <div>Loading...</div>
    }
    getLoggedUser(log)
    {
        this.props.getUser();
    }
    points(data, log)
    {
        if(log)
        {
            console.log(data);
            const us=data.data.user.user;
            let f = data.data.users.korisnici.filter(el => (el.username===us));

            let user=null;
            if(f.length===1)
                user=f[0];
            let utisci = data.data.impressions.filter(el => (el.username===us));
            let poeni=0;
            utisci.forEach(el => {
                if(el.tezina==="tesko")
                    poeni=poeni+8;
                else if(el.tezina==="srednje")
                    poeni= poeni+4;
                else
                    poeni=poeni+2;
            });
            let index = Math.trunc(poeni/10);
            this.setState({user:user, poeni:poeni, titula:data.data.titles[index], utisci:utisci});
        }
    }
    shouldComponentUpdate(props, state)
    {
        return this.state.poeni===-1; 
    }
}
function mapStateToProps(state)
{
    return {  
        user:state.user
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getUser:readUser}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);