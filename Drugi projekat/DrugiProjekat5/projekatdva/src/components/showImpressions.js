import React, {Component} from 'react';

class Impression extends Component{
    constructor(props)
    {
        super(props);
        this.state = {utisak:this.props.utisak.utisak, vreme:this.props.utisak.vreme, id:this.props.utisak.id, username:this.props.utisak.username};
        this.handleUtisak = this.handleUtisak.bind(this);
        this.handleVreme = this.handleVreme.bind(this);
    }
    render()
    {
        console.log(this.props);
        if(this.props.utisak.username === this.props.user.user)
            return (<div className="sadrzajClanka">
                        <article>
                            <h2>Korisnik: {this.props.utisak.username}</h2>
                            <textarea type="text" value={this.state.utisak}
                                onChange={this.handleUtisak}> </textarea>
                            <p>Vreme utroseno za pripremu:</p> 
                            <p><input value={this.state.vreme} onChange={this.handleVreme} type="text" /> </p> 
                            <p><button onClick={() => this.props.clickedIzmena(this.state)}> Izmeni </button> 
                               <button onClick={() => this.props.clickedBrisanje(this.props.utisak.id)}>Obrisi</button></p>
                        </article>
                    </div>)
        else
            return (
                <div>
                    <article>
                            <h2>Korisnik: {this.props.utisak.username}</h2>
                            {this.props.utisak.utisak}
                            <p>Vreme utroseno za pripremu: {this.props.utisak.vreme}</p>
                    </article>
                </div>
            )
    }
    handleVreme(event)
    {
        this.setState({vreme:event.target.value});
    }
    handleUtisak(event)
    {
        this.setState({utisak:event.target.value}); 
    }
}
export default Impression;