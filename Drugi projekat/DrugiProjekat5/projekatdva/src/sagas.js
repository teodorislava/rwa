import {call, put, takeEvery, takeLatest} from 'redux-saga/effects';
import {LOGIN, login, LOGIN_REQUEST, returnUser, getCocktails, receiveCocktails, COCKTAILS_REQUEST, receiveCocktail, COCKTAIL_REQUEST, ADD_ARTICLE, addArticleResponse, READ_ARTICLES, returnArticles, READ_IMPRESSIONS, READ_USER, ADD_IMPRESSION, removeImpressionResponse, REMOVE_IMPRESSION, GET_IMPRESSIONS} from './store/actions';
import {all, fork} from 'redux-saga/effects';
import { readArticles, putArticle, fetchData, fetchCocktails, fetchCocktail, fetchImpressions, getUser, putImpression, removeImpression, fetchTitles} from './cocktail.service.js';
import {returnImpressions} from './store/actions';

function* getApiData(action)
{
    try{
        const data = yield call(fetchData);
        yield put(login(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaLogin()
{
    yield takeLatest(LOGIN_REQUEST, getApiData);
}

function* getCocktailsData(action)
{
    try{
        const data = yield call(fetchCocktails, action.payload);
        yield put(receiveCocktails(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaCocktails()
{
    yield takeLatest(COCKTAILS_REQUEST, getCocktailsData);
}

function* getCocktailData(action)
{
    try{
        const cocktail = yield call(fetchCocktail, action.payload.group, action.payload.id);
        const impr = yield call(fetchImpressions);
        const user = yield call(getUser);
        const data = Object.assign({cocktail:cocktail, impressions:impr, user:user});
        console.log(data);
        yield put(receiveCocktail(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaCocktail()
{
    yield takeLatest(COCKTAIL_REQUEST, getCocktailData);
}


function* sagaAddArticle()
{
    yield takeLatest(ADD_ARTICLE, putNewArticle);
}
function* putNewArticle(action)
{
    try{
        const data = yield call(putArticle, action.payload.naslov, action.payload.sadrzaj);
        console.log(data);
        yield put(addArticleResponse(data));
    }
    catch(e)
    {
        console.log(e);
    }
}


function* sagaReadArticles()
{
    yield takeLatest(READ_ARTICLES, readAllArticles);
}
function* readAllArticles(action)
{
    try{
        const data = yield call(readArticles);
        yield put(returnArticles(data));
    }
    catch(e)
    {
        console.log(e);
    }
}

function* sagaGetUser()
{
    yield takeLatest(READ_USER, getLogUser);
}

function* getLogUser(action)
{
    try{
        const user = yield call(getUser);
        const users = yield call(fetchData);
        const impr = yield call(fetchImpressions);
        const t = yield call(fetchTitles);
        const data = Object.assign({user:user, users:users, impressions:impr, titles:t});
        yield put(returnUser(data)); 
    }
    catch(e)
    {
        console.log(e);
    }
}

function* sagaGetImpression()
{
    yield takeLatest(GET_IMPRESSIONS, getAllImpressions);
}

function* getAllImpressions(action)
{
    try{
        const impr = yield call(fetchImpressions);
        yield put(returnImpressions(impr)); 
    }
    catch(e)
    {
        console.log(e);
    }
}

function* sagaPutImpression()
{
    yield takeLatest(ADD_IMPRESSION, getImpressions); 
}

function* getImpressions(action)
{
    try{
        const rsp = yield call(putImpression, action.payload.utisak, action.payload.koktel);
        const impr = yield call(fetchImpressions);
        const data = Object.assign({cocktail:action.payload.koktel, user:action.payload.user, impressions:impr});
        yield put(returnImpressions(data)); 
    }
    catch(e)
    {
        console.log(e);
    }
}
function* sagaRemoveImpression()
{
    yield takeLatest(REMOVE_IMPRESSION, getImpressionsAfterDelete);
}

function* getImpressionsAfterDelete(action)
{
    try
    {
        const rsp = yield call(removeImpression, action.payload.id);
        const impr = yield call(fetchImpressions);
        const data = Object.assign({response:rsp, cocktail:action.payload.koktel, user:action.payload.user, impressions:impr});
        yield put(removeImpressionResponse(data)); 
    }
    catch(e)
    {
        console.log(e);
    }
}

export default function* root() {
    yield all([
      fork(sagaLogin), 
      fork(sagaCocktails), 
      fork(sagaCocktail), 
      fork(sagaAddArticle),
      fork(sagaReadArticles),
      fork(sagaGetUser),
      fork(sagaPutImpression), 
      fork(sagaRemoveImpression),
      fork(sagaGetImpression)
    ])
}