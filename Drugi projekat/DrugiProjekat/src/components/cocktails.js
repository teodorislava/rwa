import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {login, loginInit} from '../store/actions';
import Cocktail from './cocktail';
import {getCocktails, getCocktail} from '../store/actions';
import CocktailDetails from './cocktail-details';

class Cocktails extends Component
{
    constructor(props)
    {
        super(props);
        this.state={
            log:false, selected:0, 
            kokteli:["Bloody Mary", "Martini", "Manhattan", "Daiquiri", "Vodka", "Bourbon", "Gin", "Shots", "Margarita", "Punch", "Champagne", "Tequila"],
            baza:["bloodymary", "martini", "manhattan", "daiquiri", "vodka", "bourbon", "gin", "shots", "margarita", "punch", "champagne", "tequila"]};
    }
    render()
    {
        if(this.state.log) //ovaj deo treba da se doradi
            return <h2> Molimo vas prijavite se da biste pristupili ovom delu sajta! </h2>;

        console.log(this.props.cocktails);
        if(this.props.cocktail.viewCocktail===true)
        {
            return(
                <CocktailDetails
                koktel={this.props.cocktail.data}>
                </CocktailDetails>
            );
        }
        
        if(this.props.cocktails.view===true)
            return(
                <div>
                    {this.props.cocktails.data.map((el, index) => {
                        return (
                            <Cocktail
                                key = {index}
                                naslov = {el.naziv}
                                clicked = {() => this.props.getCocktail(el.tip, el.id)}>
                            </Cocktail>
                        )
                    })}
                </div>
            );
        
        return(
            <div>
                <h3>Unesite grupu koktela o kojoj zelite da saznate vise: </h3>
                <div>
                    {this.state.kokteli.map((title, index) => {
                        return (
                            <Cocktail
                                key={index}
                                naslov={title}
                                clicked= {() => this.props.getCocktails(this.state.baza[index])}>
                            </Cocktail>
                        )
                    })}
                </div>
            </div>
        );
    }
    log()
    {
        if(this.props.notify.log)
            this.setState({log:true});
    }
}

function mapStateToProps(state)
{
    console.log(state);
    return { 
        notify: state.notify, 
        cocktails: state.cocktails,
        cocktail: state.cocktail
    }    
}

function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getCocktails:getCocktails, getCocktail:getCocktail}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);