import React, {Component} from 'react';
import {recipe} from '../store/actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Recipe from './recipe';

class CocktailDetails extends Component
{
    render()
    {
        console.log(this.props.recept);
        return(
            <div>
                {this.renderDetails()}
            </div>
        )
    }
    renderDetails()
    {
        if(this.props.recept.viewRecipe)
            return (<Recipe></Recipe>)
        return (
            <div>
                <h2>{this.props.koktel.naziv}</h2>
                <h3>Tip case u kojoj je preporuceno sluziti koktel: {this.props.koktel.tip_case} </h3>
                <h3>Sastojci: </h3>
                <ul>
                    {this.props.koktel.sastojci.map((el, index) => {
                        return (
                            <li key={index} className="sastojak">Ime sastojka:   {el.ime}    Kolicina:   {el.kolicina}</li>
                        )
                    })}
                </ul>
                <h4>Vreme potrebno za pripremu koktela: {this.props.koktel.vreme_pripreme} min.</h4>
                <h4>Broj koraka za pripremu koktela: {this.props.koktel.broj_koraka}</h4>
                <h3> Pripremite se, tutorijal pocinje na klik sledeceg dugmeta!</h3>
                <button onClick={() => this.props.recipe(this.props.koktel.koraci)}> Zapocni pripremu! </button> 
            </div>
        )
    }
}
function mapStateToProps(state)
{
    return { recept:state.recipe }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({recipe:recipe}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(CocktailDetails);