import React, {Component} from 'react';
class StartPage extends Component 
{
    render()
    {
        return(
            <div id="pocetna">
                <h1>Mixology</h1>
                <nav>
                    <li><a className="navigacija" id="start" href="/pocetna"> Pocetna </a>
                        <a className="navigacija" id="onama" href="/onama"> O nama </a>
                        <a className="navigacija" id="login" href="/login"> Prijavi se </a>
                        <a className="navigacija" id="registracija" href="/registracija"> Registruj se </a>
                        <a className="navigacija" id="kokteli" href="/kokteli">Kokteli</a>
                    </li>
                </nav>
                <br />
                <div id="sadrzaj">
                    <img className="mainImg" src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/stocksy-txp8493e505x2j100-medium-19372mod-1510078005.jpg" />
                </div>
            </div>
        )
    }
} 
export default StartPage; 