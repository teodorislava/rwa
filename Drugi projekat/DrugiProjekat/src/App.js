import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import StartPage from './components/startpage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <StartPage></StartPage>
      </div>
    );
  }
}

export default App;
