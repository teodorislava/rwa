import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {Router, Route} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import {Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';
import reducer from './store/reducers';
import Login from './components/login';
import Cocktails from './components/cocktails';
import StartPage from './components/startpage';
import Recipe from './components/recipe';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
  );
sagaMiddleware.run(rootSaga);
store.runSaga = sagaMiddleware.run;
export let history = new  createBrowserHistory();
ReactDOM.render((
    <Provider store={store}> 
        <Router history={history}>
            <div>
                <Route path='/' component={StartPage} />
                <Route path='/login' component={Login} />
                <Route path='/kokteli' component={Cocktails} />
                <Route path='/kokteli/recept' component={Recipe} />
            </div>
    </Router>
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
