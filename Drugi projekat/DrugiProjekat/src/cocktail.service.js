export const fetchData =  async () => {
    try{
        const response = await fetch('http://localhost:4000/korisnici');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const fetchCocktails = async (tip) => {
    try{
        const response = await fetch('http://localhost:4000/'+tip);
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const fetchCocktail = async (tip, id) => {
    try{
        const response = await fetch('http://localhost:4000/'+tip+'/'+id);
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}