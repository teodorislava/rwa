import {COCKTAIL_RECEIVE} from '../actions';

export default function(state={viewCocktail:false}, action)
{
    switch(action.type)
    {
        case COCKTAIL_RECEIVE:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewCocktail:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};