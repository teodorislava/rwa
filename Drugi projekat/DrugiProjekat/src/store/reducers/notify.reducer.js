import {NOTIFY} from '../actions';

export default function(state={log:false}, action)
{
    switch(action.type)
    {
        case NOTIFY:
        {
            return Object.assign({}, state, {log:true});
        }
        default:
            return state;
    }
    return state;
};