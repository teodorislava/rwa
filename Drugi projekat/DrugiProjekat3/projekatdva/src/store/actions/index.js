export const LOGIN_REQUEST ="LOGIN_REQUEST";
export const LOGIN ="LOGIN";
export const NOTIFY="NOTIFY";
export const COCKTAILS_REQUEST="COCKTAILS_REQUEST";
export const COCKTAILS_RECEIVE="COCKTAILS_RECEIVE";
export const COCKTAIL_REQUEST="COCKTAIL_REQUEST";
export const COCKTAIL_RECEIVE="COCKTAIL_RECEIVE";
export const RECIPE="RECIPE";
export const STEP="STEP";
export const REGISTER="REGISTER";
export const ADD_ARTICLE="ADD_ARTICLE";
export const ADD_ARTICLE_RESPONSE="ADD_ARTICLE_RESPONSE";
export const READ_ARTICLES="READ_ARTICLES";
export const RETURN_ARTICLES="RETURN_ARTICLES";
export const READ_USER="READ_USER";
export const RETURN_USER="RETURN_USER";
export const ADD_IMPRESSION="ADD_IMPRESSION";
export const RETURN_IMPRESSIONS="RETURN_IMPRESSIONS";
export const REMOVE_IMPRESSION="REMOVE_IMPRESSION";
export const REMOVE_IMPRESSION_RESPONSE="REMOVE_IMPRESSION_RESPONSE";

export function login(data)
{
    return {
        type:LOGIN, 
        payload:data
    }
}
export function loginInit()
{
    return{
        type:LOGIN_REQUEST
    }
}

export function notifyLog()
{
    return {
        type:NOTIFY
    }
}
export function getCocktails(data)
{
    return{
        type:COCKTAILS_REQUEST,
        payload:data
    }
}
export function receiveCocktails(data)
{
    return {
        type:COCKTAILS_RECEIVE,
        payload: data
    }
}
export function getCocktail(group, id)
{
    return {
        type:COCKTAIL_REQUEST,
        payload: {group:group, id:id}
    }
}
export function receiveCocktail(data)
{
    console.log(data);
    return {
        type:COCKTAIL_RECEIVE,
        payload: data
    }
}
export function recipe(steps)
{
    return {
        type:RECIPE,
        payload: {steps:steps}
    }
}
export function step(s)
{
    return {
        type:STEP,
        payload:{step:s}
    }
}
export function register(obj)
{
    return {
        type:REGISTER,
        payload:obj
    }
}
export function addArticle(naslov, sadrzaj)
{
    return {
        type:ADD_ARTICLE,
        payload: {naslov:naslov, sadrzaj:sadrzaj}
    }
}
export function addArticleResponse(response)
{
    return {
        type:ADD_ARTICLE_RESPONSE,
        payload: {response:response}
    }
}
export function readArticles()
{
    console.log("shvatio sam koja je akcija");
    return {
        type:READ_ARTICLES
    }
}
export function returnArticles(data)
{
    return {
        type:RETURN_ARTICLES,
        payload: {data:data}
    }
}
export function readUser()
{
    return {
        type:READ_USER
    }
}
export function returnUser(data)
{
    return {
        type:RETURN_USER,
        payload:data
    }
}

export function addImpression(utisak, koktel, user)
{
    return {
        type:ADD_IMPRESSION,
        payload: {utisak:utisak, koktel:koktel, user:user}
    }
}

export function returnImpressions(data)
{
    return {
        type:RETURN_IMPRESSIONS,
        payload:data
    }
}

export function removeImpression(koktel, user, id)
{
    return {
        type:REMOVE_IMPRESSION,
        payload:{koktel:koktel, user:user, id:id}
    }
}
export function removeImpressionResponse(odg)
{
    return {
        type: REMOVE_IMPRESSION_RESPONSE,
        payload:odg
    }
}