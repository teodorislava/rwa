import {combineReducers} from 'redux';
import loginReducer from './login.reducer';
import notifyReducer from './notify.reducer';
import cocktailsReducer from './cocktails.reducer';
import cocktailReducer from './cocktail.reducer';
import recipeReducer from './recipe.reducer';
import stepReducer from './step.reducer';
import addArticleReducer from './addArticle.reducer';
import readArticleReducer from './readArticle.reducer';
import userReducer from './user.reducer';

export default combineReducers({
    login:loginReducer,
    notify:notifyReducer,
    cocktails:cocktailsReducer, 
    cocktail:cocktailReducer,
    recipe:recipeReducer,
    step:stepReducer,
    responseArticle:addArticleReducer,
    articles:readArticleReducer,
    user:userReducer
});