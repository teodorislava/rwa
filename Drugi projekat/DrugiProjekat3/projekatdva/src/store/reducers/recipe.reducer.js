import {RECIPE} from '../actions';

export default function(state={viewRecipe:false}, action)
{
    switch(action.type)
    {
        case RECIPE:
        {
            if(action.payload!==undefined)
                return Object.assign({}, state, {viewRecipe:true, data:action.payload});
        }
        default:
            return state;
    }
    return state;
};