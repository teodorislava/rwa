import React, {Component} from 'react';
import { of } from 'rxjs/observable/of';
import { mapTo, delay } from 'rxjs/operators';
import {connect} from 'react-redux';
import { concat} from 'rxjs';
import {step} from '../store/actions';
import {bindActionCreators} from 'redux';
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/interval";

class Recipe extends Component
{
    render()
    {
        if(this.props.step.viewStep)
        {
            this.countdown(this.props.step.data.step);
            return(<div>
                    <h2 className="korak">{this.props.step.data.step.opis}</h2>
                    <h3>{this.props.step.data.step.vreme}</h3>
                   </div>) 
        }
        return(
                <div>
                {this.renderSteps(this.props.recept.data.steps)}
                </div>
        );
        
    }
    countdown(step)
    {
        Observable.interval(2000)
        .subscribe(el => {step.vreme--; this.props.showStep(step);});   
    }
    renderSteps(steps)
    {
        const prikaz = of(null);
        console.log(steps);
        switch(steps.length)
        {
            case 1:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0])),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[0].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 2:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0].opis)),
                    prikaz.pipe(mapTo(steps[1].opis), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[1].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 3:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0].opis)),
                    prikaz.pipe(mapTo(steps[1].opis), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2].opis), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[2].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 4:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0].opis)),
                    prikaz.pipe(mapTo(steps[1].opis), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2].opis), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3].opis), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[3].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 5:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0].opis)),
                    prikaz.pipe(mapTo(steps[1].opis), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2].opis), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3].opis), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4].opis), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[4].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 6:{
                const message = concat(
                    prikaz.pipe(mapTo(steps[0].opis)),
                    prikaz.pipe(mapTo(steps[1].opis), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2].opis), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3].opis), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4].opis), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo(steps[5].opis), delay(steps[4].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[5].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            case 7:{
                console.log("shvatio sam da ima sedam koraka");
                const message = concat(
                    prikaz.pipe(mapTo(steps[0].opis)),
                    prikaz.pipe(mapTo(steps[1].opis), delay(steps[0].vreme*1000)),
                    prikaz.pipe(mapTo(steps[2].opis), delay(steps[1].vreme*1000)),
                    prikaz.pipe(mapTo(steps[3].opis), delay(steps[2].vreme*1000)),
                    prikaz.pipe(mapTo(steps[4].opis), delay(steps[3].vreme*1000)),
                    prikaz.pipe(mapTo(steps[5].opis), delay(steps[4].vreme*1000)),
                    prikaz.pipe(mapTo(steps[6].opis), delay(steps[5].vreme*1000)),
                    prikaz.pipe(mapTo('Uzivajte u vasem koktelu!!'), delay(steps[6].vreme*1000))
                );
                const subscribe = message.subscribe(val => this.props.showStep(val));
                break;
            }
            default:
                break;
        }
    }
}
function mapStateToProps(state)
{
    return { recept:state.recipe, step:state.step }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({showStep:step}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Recipe);