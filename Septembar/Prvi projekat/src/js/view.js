import { Masaza } from '../models/massage';
import { Trening } from '../models/training';
import { ReservationsService } from '../services/reservations-service';
import { Elements } from './elements';
import * as Rxjs from 'rxjs';
import { interval } from 'rxjs/observable/interval';


export class View {

    constructor() {
        this.main = document.getElementById('root');
        this.trajanje = ["30","45","60","75","90"];
        this.grupe = ["ledja","grudi","ramena","stomak","gluteus","ruke","usloza","pzloza"];
        this.prikaz = ["Leđa", "Grudi", "Ramena", "Stomak", "Gluteus", "Ruke", "Unutrašnja i spoljašnja loža", "Prednja i zadnja loža"];
        this.quote();
    }

    quote() {
        interval(5000)
        .map(num => parseInt(Math.random()*10))
        .take(100)
        .subscribe(num => {
            ReservationsService.getQuote(num);
        });
    }
    reservationForm (type) {
        this.main.innerHTML = '';
        const form = Elements.createDiv('form-container', this.main, '');
        const p = Elements.createParagraph(form, '');
        Elements.createInput('ime', 'text', '', p, 'Unesite vaše ime');
        const p2 = Elements.createParagraph(form, '');
        Elements.createInput('prezime', 'text', '', p2, 'Unesite vaše prezime');
        const p3 = Elements.createParagraph(form, '');
        const p4 = Elements.createParagraph(form, '');

        if(type === 'masaza') {
            ReservationsService.getActivityTypes().then(activity => {
                const a = activity.filter(a => a.tip === 'masaza').map(a => a.naziv);
                Elements.createLabel('Odaberite tip masaže: ', p3);
                Elements.createSelect('tip', p3, a);
            });
            ReservationsService.getEmployees().then(employees => {
                    const m = employees.filter(e => e.type === 'maser').map(e => e.ime);
                    Elements.createLabel('Odaberite masera: ', p4);
                    Elements.createSelect('maser_ime', p4, m);
            });
        } 
        else if(type === 'fitnes'){
            ReservationsService.getActivityTypes().then(activity => {
                const a = activity.filter(a => a.tip === 'trening').map(a => a.naziv);
                Elements.createLabel('Odaberite tip treninga: ', p3);
                Elements.createSelect('tip', p3, a);
            });
            ReservationsService.getEmployees().then(employees => {
                    const m = employees.filter(e => e.type === 'trener').map(e => e.ime);
                    Elements.createLabel('Odaberite trenera: ', p4);
                    Elements.createSelect('trener_ime', p4, m);
            });
        }
        
        const p5 = Elements.createParagraph(form, '');
        Elements.createLabel('Odaberite trajanje: ', p5);
        Elements.createSelect('trajanje', p5, this.trajanje);
        
        const p6 = Elements.createParagraph(form, '');
        Elements.createLabel('Odaberite datum: ', p6);
        Elements.createInput('date', 'date', '', p6, '');
        
        const p7 = Elements.createParagraph(form, '');
        Elements.createLabel('Odaberite vreme: ', p7);
        Elements.createInput('time', 'time', '', p7, '');

        const submitButton = Elements.createButton('submit', 'Rezerviši', form);
        this.buttonBack(type);
        submitButton.onclick = () => {

        let ime = document.getElementsByName("ime");
        let prezime = document.getElementsByName("prezime");
        let maser = document.getElementsByName("maser_ime");
        let trener = document.getElementsByName("trener_ime");
        let tipp = document.getElementsByName("tip");
        let trajanje = document.getElementsByName("trajanje");
        let time = document.getElementsByName("time");
        let date = document.getElementsByName("date");
        let cena = trajanje[0].value*20;
        let spl = date[0].value.split('-');
        
        let masaza = {};
        let trening = {};
        if(type === "masaza")
            masaza = new Masaza(0, tipp[0].value, trajanje[0].value,
            maser[0].value, ime[0].value, prezime[0].value, spl[2], spl[1],
            spl[0], time[0].value, cena);
        else if(type === 'fitnes')
            trening = new Trening(0, tipp[0].value, trajanje[0].value,
                trener[0].value, ime[0].value, prezime[0].value, spl[2], spl[1],
                spl[0], time[0].value, cena);
        
        if(tipp[0].value==="" || trajanje[0].value==="" ||
            ime[0].value==="" || prezime[0].value==="" || spl[1]==="" || spl[0]==="" || time[0].value==="")
        {                 
            this.main.innerHTML="Nevalidni podaci!"; 
            this.buttonAdd(type); 
        }
        else
        {
            let obs;
            if(type === "masaza")
                obs = ReservationsService.getMassages();
            else if(type === "fitnes")
                obs = ReservationsService.getTrainings();
            obs.then(el => 
            {
                if(type === "masaza")
                {
                    let niz = el.filter(el => 
                        (el.maser_ime === masaza.maser_ime &&
                        el.dan === masaza.dan &&
                        el.mesec === masaza.mesec &&
                        el.godina === masaza.godina &&
                        el.vreme === masaza.vreme));

                    if(niz.length !== 0)
                        this.alreadyExists(type);
                    else {
                        Elements.createNotification('back');
                        ReservationsService.postMassage(masaza);
                        const button = document.getElementsByClassName('back')[0];
                        button.onclick = () => {
                            this.choseAction(type);
                        }       
                    }
                }
                else if(type === "fitnes")
                {
                    let niz = el.filter(el => 
                        (el.trener_ime === trening.trener_ime &&
                        el.dan === trening.dan &&
                        el.mesec === trening.mesec &&
                        el.godina === trening.godina &&
                        el.vreme === trening.vreme));

                    if(niz.length !== 0)
                        this.alreadyExists(type);
                    else {
                        Elements.createNotification('back');
                        ReservationsService.postTraining(trening);
                        const button = document.getElementsByClassName('back')[0];
                        button.onclick = () => {
                            this.choseAction(type);
                        }
                    }
                }
            })
        }
        }
    }

    alreadyExists(type) {
        alert("Termin je zauzet!");
        this.reservationForm(type);
    }
    choseAction(type) {

        this.main.innerHTML = "";
        Elements.createBr(this.main);
        Elements.createLabel('Odaberite aktivnost:', this.main);
        const p = Elements.createParagraph(this.main, '');
        Elements.createInput('operacija', 'radio', 'dodavanje', p, '', 'action-input');
        if(type === 'masaza') {
            Elements.createLabel('Rezervacija masaže', p);
        }
        else {
            Elements.createLabel('Rezervacija treninga', p);
        }

        const p2 = Elements.createParagraph(this.main, '');
        Elements.createInput('operacija', 'radio', 'azuriranje', p2, '', 'action-input');
        if(type === 'masaza') {
            Elements.createLabel('Ažuriranje masaže', p2);
        }
        else {
            Elements.createLabel('Ažuriranje treninga', p2);
        }

        const p3 = Elements.createParagraph(this.main, '');
        Elements.createInput('operacija', 'radio', 'brisanje', p3, '', 'action-input');
        if(type === 'masaza') {
            Elements.createLabel('Brisanje masaže', p3);
        }
        else {
            Elements.createLabel('Brisanje treninga', p3);
        }

        const p4 = Elements.createParagraph(this.main, '');
        Elements.createInput('operacija', 'radio', 'nagrada', p4, '', 'action-input');
        if(type === 'masaza') {
            Elements.createLabel('Nagradna masaža', p4);
        }
        else {
            Elements.createLabel('Nagradni trening', p4);
        }

        const p5 = Elements.createParagraph(this.main, '');
        const submitBtn = Elements.createButton('submit', 'Odaberi', p5);
        
        submitBtn.onclick = () => {
            let operation = document.querySelectorAll("input:checked")[0].value;
            if(operation === "dodavanje")
                this.reservationForm(type);
            else if(operation === "brisanje")
                this.deleteData(type);
            else if(operation === "azuriranje")
                this.updateData(type);
            else if(operation === "nagrada")
                this.winner(type);
        }
    }
    watchSearch(search, type, parent, mode) {
        let url;
        if(type === "masaza")
            url = "http://localhost:3000/masaze";
        else if(type === "fitnes")
            url = "http://localhost:3000/treninzi";
        
            Rxjs.Observable.fromEvent(search, "input")
            .debounceTime(500)
            .map(ev => ({tekst: ev.target.value }))
            .switchMap(text => ReservationsService.getData(url))  
            .subscribe(rez => {
                parent.innerHTML = '';
                rez.forEach(el => {
                    let name = document.getElementsByName("ime")[0].value;
                    let pomString = (el.klijent_ime+" "+el.klijent_prezime).toLowerCase();
                    let pomString2 = (el.klijent_prezime+" "+el.klijent_ime).toLowerCase();
                    if(name !== "" && ( pomString.indexOf(name.toLowerCase())!==-1 || pomString2.indexOf(name.toLowerCase())!==-1))
                    {
                        const resultDiv = Elements.createDiv('brisanje', parent, '');
                        resultDiv.onclick = () =>
                        {
                            if(type === "masaza" && mode === 'delete') {
                                Elements.createNotification('back');
                                ReservationsService.deleteMassage(el);
                                const button = document.getElementsByClassName('back')[0];
                                button.onclick = () => {
                                    this.choseAction(type);
                                }
                            }
                            else if(type === "fitnes" && mode === 'delete') {
                                Elements.createNotification('back');
                                ReservationsService.deleteTraining(el);
                                const button = document.getElementsByClassName('back')[0];
                                button.onclick = () => {
                                    this.choseAction(type);
                                }
                            }
                            else if(mode === 'update')
                                this.updateForm(el, type);
                        }
                        if(type === "masaza")
                        {
                            resultDiv.innerHTML = "Masaza: "+el.tip+", Trajanje: "+
                            el.trajanje+", Maser: "+el.maser_ime+" " + "Klijent: "+el.klijent_ime+
                            " "+el.klijent_prezime+", Cena: "+el.cena+
                            ", Vreme: "+el.vreme+" , Datum: "+el.dan+"."+el.mesec+"."+el.godina+".";
                        }
                        else if(type === "fitnes")
                        {
                            resultDiv.innerHTML = "Trening: "+el.tip+", Trajanje: "+
                            el.trajanje+", Trener: "+el.trener_ime+" " + "Klijent: "+el.klijent_ime+
                            " "+el.klijent_prezime+", Cena: "+el.cena+
                            ", Vreme: "+el.vreme+" , Datum: "+el.dan+"."+el.mesec+"."+el.godina+".";
                            
                        }
                    }
            });  
        });
    }
    deleteData(type)
    {
        this.main.innerHTML = "";
        Elements.createBr(this.main);
        Elements.createLabel('Ime i prezime:', this.main);
        const search = Elements.createInput('ime', 'text', '', this.main, 'Unesite ime i prezime');
        const insideDiv = Elements.createDiv('results-container', this.main, '');
        this.watchSearch(search, type, insideDiv, 'delete');
        this.buttonBack(type);
    }
    
    updateData(type) {
        this.main.innerHTML = "";
        Elements.createBr(this.main);
        Elements.createLabel('Ime i prezime:', this.main);
        const search = Elements.createInput('ime', 'text', '', this.main, 'Unesite ime i prezime');
        const insideDiv = Elements.createDiv('results-container', this.main, '');
        this.watchSearch(search, type, insideDiv, 'update');
        this.buttonBack(type);
    }
    updateForm(m, type) {

        this.main.innerHTML = '';
        Elements.createBr(this.main);
        const form = Elements.createDiv('form-container', this.main, '');
        const p = Elements.createParagraph(form, '');
        Elements.createInput('ime', 'text', m.klijent_ime, p, 'Unesite vaše ime');
        const p2 = Elements.createParagraph(form, '');
        Elements.createInput('prezime', 'text', m.klijent_prezime, p2, 'Unesite vaše prezime');
        const p3 = Elements.createParagraph(form, '');
        const p4 = Elements.createParagraph(form, '');

        if(type === 'masaza') {
            ReservationsService.getActivityTypes().then(activity => {
                const a = activity.filter(a => a.tip === 'masaza').map(a => a.naziv);
                Elements.createLabel('Odaberite tip masaže: ', p3);
                Elements.createSelect('tip', p3, a);
            });
            ReservationsService.getEmployees().then(employees => {
                    const m = employees.filter(e => e.type === 'maser').map(e => e.ime);
                    Elements.createLabel('Odaberite masera: ', p4);
                    Elements.createSelect('maser_ime', p4, m);
            });
        } 
        else if(type === 'fitnes') {
            ReservationsService.getActivityTypes().then(activity => {
                const a = activity.filter(a => a.tip === 'trening').map(a => a.naziv);
                Elements.createLabel('Odaberite tip treninga: ', p3);
                Elements.createSelect('tip', p3, a);
            });
            ReservationsService.getEmployees().then(employees => {
                    const m = employees.filter(e => e.type === 'trener').map(e => e.ime);
                    Elements.createLabel('Odaberite trenera: ', p4);
                    Elements.createSelect('trener_ime', p4, m);
            });
        }
        
        const p5 = Elements.createParagraph(form, '');
        Elements.createLabel('Odaberite trajanje: ', p5);
        Elements.createSelect('trajanje', p5, this.trajanje, m.trajanje);
        
        const p6 = Elements.createParagraph(form, '');
        Elements.createLabel('Odaberite datum: ', p6);
        Elements.createInput('date', 'date', m.godina+"-"+m.mesec+"-"+m.dan, p6, '');
        
        const p7 = Elements.createParagraph(form, '');
        Elements.createLabel('Odaberite vreme: ', p7);
        Elements.createInput('time', 'time', m.vreme, p7, '');

        const submitButton = Elements.createButton('submit', 'Izmeni', form);
        submitButton.onclick = () => {

            let ime = document.getElementsByName("ime");
            let prezime = document.getElementsByName("prezime");
            let maser = document.getElementsByName("maser_ime");
            let trener = document.getElementsByName("trener_ime");
            let tipp = document.getElementsByName("tip");
            let trajanje = document.getElementsByName("trajanje");
            let time = document.getElementsByName("time");
            let date = document.getElementsByName("date");
            let cena = trajanje[0].value*20;
            let spl = date[0].value.split('-');
        
            let masaza;
            let trening;
            if(type === "masaza")
                masaza = new Masaza(m.id, tipp[0].value, trajanje[0].value,
                maser[0].value, ime[0].value, prezime[0].value, spl[2], spl[1],
                spl[0], time[0].value, cena);
            else if (type === 'fitnes')
                trening = new Trening(m.id, tipp[0].value, trajanje[0].value,
                    trener[0].value, ime[0].value, prezime[0].value, spl[2], spl[1],
                    spl[0], time[0].value, cena);
        
            if(tipp[0].value==="" || trajanje[0].value==="" ||
                ime[0].value==="" || prezime[0].value==="" || spl[1]==="" || spl[0]==="" || time[0].value==="")
            {                 
                this.main.innerHTML="Nevalidni podaci!"; 
                this.buttonAdd(type); 
            }
            else
            {
                let obs;
                if(type === "masaza")
                    obs = ReservationsService.getTrainings();
                else if(type === "fitnes")
                    obs = ReservationsService.getMassages();

                obs.then(el => 
                {
                    if(type === "masaza")
                    {
                        let niz = el.filter(el => 
                            (el.maser_ime === masaza.maser_ime &&
                            el.dan === masaza.dan &&
                            el.mesec === masaza.mesec &&
                            el.godina === masaza.godina &&
                            el.vreme === masaza.vreme));

                        if(niz.length !== 0)
                            this.alreadyExists(type);
                        else {
                            Elements.createNotification('back');
                            ReservationsService.updateMassage(masaza);
                            const button = document.getElementsByClassName('back')[0];
                            button.onclick = () => {
                                this.choseAction(type);
                            }
                        }
                    }
                    else if(type === "fitnes")
                    {
                        let niz = el.filter(el => 
                            (el.trener_ime === trening.trener_ime &&
                            el.dan === trening.dan &&
                            el.mesec === trening.mesec &&
                            el.godina === trening.godina &&
                            el.vreme === trening.vreme));

                        if(niz.length !== 0)
                            this.alreadyExists(type);
                        else {
                            Elements.createNotification('back');
                            ReservationsService.updateTraining(trening);
                            const button = document.getElementsByClassName('back')[0];
                            button.onclick = () => {
                                this.choseAction(type);
                            }
                        }      
                    }
                })
            }
        }
        this.buttonBack(type);
    }

    aboutUs() {
        this.main.innerHTML = "";
        const text = "Dobrodošli! "+
        "Dobrodošli u DreamGym, prvi niški fitness-wellness centar namenjen prvenstveno damama."+
        "DreamGym je osnovan 2010. godine. Pored fitness programa, prvi uvodi wellness sadržaje u Nišu. Nudimo sve što je potrebno savremenoj ženi: najveći izbor sportskih programa, tretmana za negu tela, kao i opuštanje u našoj RELAX zoni."+
        "Naš cilj je da uz pomoć stručnih trenera, vrhunskih uslova za trening i efektivnog rada na sebi postignete maksimalan učinak u najkraćem mogućem roku."+
        "U našoj teretani radi čak 10 trenera i 5 masera, čije su usluge konstantno na raspolaganju.";
        Elements.createDiv('welcome', this.main, text);   
    }

    searchData(type) {

        this.main.innerHTML = "";
        const p = Elements.createParagraph(this.main, '');
        if(type === "masaza") {
            Elements.createLabel('Odaberite period u kom želite da vidite rezervisane masaže:', p);
        }
        else if(type === "fitnes") {
            Elements.createLabel('Odaberite period u kom želite da vidite rezervisane treninge:', p);
        }

        const p2 = Elements.createParagraph(this.main, '');
        Elements.createLabel('Od: ', p2);
        Elements.createInput('datumi', 'date', '', p2, '');

        const p3 = Elements.createParagraph(this.main, '');
        Elements.createLabel('Do: ', p3);
        Elements.createInput('datumi', 'date', '', p3, '');

        const p4 = Elements.createParagraph(this.main, '');
        const p5 = Elements.createParagraph(this.main, '');

        if(type === "masaza") {
            ReservationsService.getEmployees().then(employees => {
                    const m = employees.filter(e => e.type === 'maser').map(e => e.ime);
                    Elements.createLabel('Odaberite masera: ', p4);
                    Elements.createSelect('maser_ime', p4, m);
            });
        }
        else if(type === "fitnes") {
            ReservationsService.getEmployees().then(employees => {
                const m = employees.filter(e => e.type === 'trener').map(e => e.ime);
                Elements.createLabel('Odaberite trenera: ', p4);
                Elements.createSelect('trener_ime', p4, m);
            });
        }
        Elements.createBr(this.main);

        const submitButton = Elements.createButton('submit', 'Pronađi', p5);
        submitButton.onclick = () => {
            let d = document.getElementsByName("datumi");
            if(type === "masaza") {
                let maser = document.getElementsByName("maser_ime");
                ReservationsService.showMassages(d[0].value, d[1].value, maser[0].value);
            }
            else if(type === "fitnes") {
                let trener = document.getElementsByName("trener_ime");
                ReservationsService.showTrainings(d[0].value, d[1].value, trener[0].value);
            }
        } 
    }

    winner(type)
    {
        Promise.all([
                this.returnRandom(),
                ReservationsService.getReservations(type)
            ]).then(([number, res]) => {
                let w = res.filter(el => el.id === number);
                this.main.innerHTML = "";
                this.main.innerHTML="Dobitnik je: " + w[0].klijent_ime+ " "+ w[0].klijent_prezime+"!";
                this.buttonBack(type);
        });
    
    }
    returnRandom()
    {
        let rand = parseInt(Math.random()*10+1);
        return new Promise((resolve,reject)=>
        {
            setTimeout(()=> resolve(rand), 2000);
        });
    }
    showReview() {
        this.main.innerHTML = "";
        Elements.createParagraph(this.main, 'Unesite ocenu:');

        const p2 = Elements.createParagraph(this.main, '');
        Elements.createInput('ime', 'text', '', p2, 'Ime');

        const p3 = Elements.createParagraph(this.main, '');
        Elements.createInput('prezime', 'text', '', p3, 'Prezime');

        const p4 = Elements.createParagraph(this.main, '');
        Elements.createInput('zaposleni', 'text', '', p4, 'Zaposleni');

        const text = document.createElement("textarea");
        text.placeholder="Opis";
        text.name="opis"; 
        text.className="opis";
        this.main.appendChild(text);

        const p5 = Elements.createParagraph(this.main, '');
        const robotTest = Elements.createInput('robot', 'text', '', p5, '8+2?');
        this.robot(robotTest);

        const p6 = Elements.createParagraph(this.main, '');
        const submitButton = Elements.createButton('submit', 'Prosledi', p6);
        this.submitReview(submitButton);

    }
    robot(input)
    {
        Rxjs.Observable.fromEvent(input, "input")
        .debounceTime(1000)
        .map(ev => ({tekst: ev.target.value }))
        .subscribe(rez => {
            if(rez.tekst !== "10")
            {
                alert("Pali ste robot test! Pokusajte ponovo kasnije.");
                this.main.innerHTML = "";
            }
        });
    }
    submitReview(b)
    {
        Rxjs.Observable.fromEvent(b, "click")
        .subscribe(el => {
            const ime = document.getElementsByName("ime")[0];
            const prezime = document.getElementsByName("prezime")[0];
            const zaposleni = document.getElementsByName("zaposleni")[0];
            const tekst = document.getElementsByName("opis")[0];
            const robot = document.getElementsByName("robot")[0];
            if(Number(robot.value) !==10 && tekst.value !=="" && ime.value !=="" && prezime.value!=="" && zaposleni.value!=="")
                alert("Nevalidni podaci!");
            else {
                Elements.createNotification('back');
                ReservationsService.postReview(ime.value, prezime.value, tekst.value, zaposleni.value);
                const button = document.getElementsByClassName('back')[0];
                button.onclick = () => {
                    this.showReview();
                }
            }
                
        });
    }
    onlineTraining()
    {
        let selektovani=[];
        let selektovaniPrikaz=[];

        this.main.innerHTML = '';
        const h=document.createElement("h3");
        h.innerHTML="Kreirajte sopstveni trening!";
        this.main.appendChild(h);
        Elements.createInput('duzina', 'text', '', this.main, 'Dužina');


        this.grupe.forEach( (el, index) =>
        {
            const p = Elements.createParagraph(this.main, '');
            Elements.createLabel(this.prikaz[index], p);
            Elements.createInput(this.prikaz[index], 'checkbox', el, p, '', 'checkbox');
        });

        const p2 = Elements.createParagraph(this.main, '');
        const button = Elements.createButton('submit', 'Izračunaj', p2);
        
        button.onclick = () =>      {
            const checked = document.querySelectorAll("input:checked");
            checked.forEach(el=>{
                selektovani.push(el.value);
                selektovaniPrikaz.push(el.name);
            });
            const trajanje = document.getElementsByName("duzina");
            ReservationsService.getExcercises(selektovani, selektovaniPrikaz, trajanje[0].value);
        }
    }
    buttonChange(m, type) {
        this.main.innerHTML = "";
        const p = Elements.createParagraph(this.main, '');
        const button = Elements.createButton('submit', 'Nazad', p);
        button.onclick = () =>
        {
            this.updateForm(m,type);
        }
    }
    openReview() {
        this.main.innerHTML = "";
        const p = Elements.createParagraph(this.main, '');
        const button = Elements.createButton('submit', 'Nazad', p);
        button.onclick = () =>
        {
            this.showReview();
        }
    }
    buttonAdd(type) {
        this.main.innerHTML = "";
        const p = Elements.createParagraph(this.main, '');
        const button = Elements.createButton('submit', 'Nazad', p);
        button.onclick = () =>
        {
            this.reservationForm(type);
        }
    }
    buttonSpaFitnes(type) {
        this.main.innerHTML = "";
        const p = Elements.createParagraph(this.main, '');
        const button = Elements.createButton('submit', 'Nazad', p);
        button.onclick = () => 
        {
            this.searchData(type);
        }
    }
    buttonBack(type) {
        const p = Elements.createParagraph(this.main, '');
        const button = Elements.createButton('submit', 'Nazad', p);
        button.onclick = () => 
        {
            this.choseAction(type);
        }
    }
}
