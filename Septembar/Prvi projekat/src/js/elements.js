export class Elements {

    static createButton(name, content, location) {
        const button = document.createElement('button');
        button.innerHTML = content;
        button.name = name;
        button.className = 'main-btn';
        location.appendChild(button);
        return button;
    } 
    static createInput(name, type, value, location, placeholder, className) {
        const input = document.createElement('input');
        input.type = type;
        input.name = name;
        input.placeholder = placeholder;
        input.className = className || "main-input";
        input.value = value;
        location.appendChild(input);
        return input;
    }
    static createDiv(className, location, content) {
        const div = document.createElement('div');
        div.className = className;
        div.innerHTML = content;
        location. appendChild(div);
        return div;
    }
    static createParagraph(location, content) {
        const paragraph = document.createElement('p');
        paragraph.className = 'main-p';
        paragraph.innerHTML = content;
        location.appendChild(paragraph);
        return paragraph;
    }
    static createSelect(name, location, optionList, value) {
        let ind = 0;
        const select = document.createElement('select');
        select.name = name;
        select.value = value;
        select.className = "main-select";
        optionList.forEach((option, index) => {
            const op = document.createElement('option');
            op.innerHTML = option;
            op.name = option;
            op.value = option;
            select.appendChild(op);
            if(option === value)
                ind = index;
        });
        select.selectedIndex = ind;
        location.appendChild(select);
        return select;
    } 
    static createLabel(content, location) {
        const label = document.createElement('label');
        label.className = "main-label";
        label.innerHTML = content;
        location.appendChild(label);
        return label;
    }
    static createNotification(name) {
        const div = document.getElementById('root');
        div.innerHTML = "";
        const paragraph = document.createElement('p');
        paragraph.className = 'message';
        div.appendChild(paragraph);
        const paragraph2 = document.createElement('p');
        paragraph2.className = 'main-p';
        div.appendChild(paragraph2);

        const button = document.createElement('button');
        button.name = name;
        button.innerHTML = "Nazad";
        button.className = 'back';
        paragraph2.appendChild(button);
    }
    static createBr(location) {
        const br = document.createElement('br');
        location.appendChild(br);
    }
    static createForm(location) {
        const form = document.createElement('form');
        location.appendChild(form)
    }
}