import * as Rxjs from 'rxjs';
import { Elements } from '../js/elements';
import { Masaza } from '../models/massage';
import { Trening } from '../models/training';

export class ReservationsService
{
    constructor()
    {}

    static postMassage(el)
    {        
        fetch('http://localhost:3000/masaze', {
            method: 'post',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,maser_ime:el.maser_ime,trajanje:el.trajanje,
                klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,
                mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=> 
                {
                    res.json();
                    if(res.status===201 || res.status===200)
                    {
                        const message = document.getElementsByClassName('message')[0];
                        message.innerHTML = "Rezervacija masaže je uspešna!";
                    }
                    else
                    {
                        const message = document.getElementsByClassName('message')[0];
                        message.innerHTML = "Rezervacija masaže je neuspešna!"
                    }
            });        
    }
    static deleteMassage(el)
    {
        fetch('http://localhost:3000/masaze/'+el.id, {
            method: 'delete'
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Brisanje masaže je uspešno!"
                }
                else
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Brisannje masaže je neuspešno!"
                }
        });
    }
    static updateMassage(el)
    {
        fetch('http://localhost:3000/masaze/'+el.id, {
            method: 'put',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,maser_ime:el.maser_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Izmena masaže je uspešna!"
                }
                else
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Izmena masaže je neuspešna!"
                }
        });
    }
    static postTraining(el)
    {        
        fetch('http://localhost:3000/treninzi', {
            method: 'post',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,trener_ime:el.trener_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Rezervacija treninga je uspešna!";
                }
                else
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Rezervacija treninga je neuspešna!";
                }
        });
    }
    static deleteTraining(el)
    {
        fetch('http://localhost:3000/treninzi/'+el.id, {
            method: 'delete'
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Brisanje treninga je uspešno!";
                }
                else
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Brisanje treninga je neuspešno!";
                }
        });
    }
    static updateTraining(el)
    {
        fetch('http://localhost:3000/treninzi/'+el.id, {
            method: 'put',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,trener_ime:el.trener_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Izmena treninga je uspešna!"
                }
                else
                {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Izmena treninga je neuspešna!"
                }
        });
    }
    static showMassages(od, doo, maser)
    {
      const splOd = od.split("-");
      const splDo = doo.split("-");
      const div = document.getElementById("root");
      div.innerHTML = "";
      Elements.createLabel("Pronadjene su ove rezervacije: ", div);
      
      const url="http://localhost:3000/masaze";
      
      Rxjs.Observable.fromPromise(fetch(url).then(response=>
      {
          response.json().then(el => 
          {
            el.forEach(el => {
              let m = new Masaza(el.id, el.tip, el.trajanje, el.maser_ime, el.klijent_ime,
              el.klijent_prezime, el.dan, el.mesec, el.godina, el.vreme, el.cena);
              if(m.godina >= splOd[0] && m.mesec >= splOd[1] && m.dan >= splOd[2] &&
                 m.godina <= splDo[0] && m.mesec <= splDo[1] && m.dan <= splDo[2] && m.maser_ime === maser)
              {
                m.prikaz();
              }
            });
          });
      }));
      
    }
    static showTrainings(od, doo, trener)
    {
      let splOd = od.split("-");
      let splDo = doo.split("-");
      let div = document.getElementById("root");
      div.innerHTML = "";
      Elements.createLabel("Pronadjene su ove rezervacije: ", div);

      const url="http://localhost:3000/treninzi";
      
      Rxjs.Observable.fromPromise(fetch(url).then(response =>
      {
          response.json().then(el=> 
          {
              el.forEach(el => {
              let m = new Trening(el.id, el.tip, el.trajanje, el.trener_ime,
              el.klijent_ime, el.klijent_prezime,
              el.dan, el.mesec, el.godina, el.vreme, el.cena);
              if(m.godina>=splOd[0] && m.mesec>=splOd[1] && m.dan>=splOd[2] &&
                m.godina<=splDo[0] && m.mesec<=splDo[1] && m.dan<=splDo[2] && m.trener_ime===trener)
              {
                m.prikaz();
              }
            });
          });
      }));
    }
    static getReservations(tip)
    {
      if(tip==="masaza")
      {
        return fetch('http://localhost:3000/masaze').then( response =>
            response.json());
      }
      else 
        return fetch('http://localhost:3000/treninzi').then( response =>
            response.json());
    }
    static getQuote(num)
    {
      fetch('http://localhost:3000/citati').then( response =>
        response.json().then(el=> {
  
          let citat = document.getElementById("citat");
          citat.innerHTML=el[num].tekst;
  
        }));
    }
    static getExcercises(grupe, prikaz, trajanje)
    {
      fetch('http://localhost:3000/vezbe').then( response =>
        response.json().then(el => {
          let duration = 0;
          let target = [];
          let count = 0; 
          let j = 0;
          for(j=0; j < grupe.length; j++) {
              target.push(el[`${grupe[j]}`]);
              count += target[j].length; 
          } 
          count /= grupe.length;
          count = Math.trunc(count);
          
          let serije = new Array(grupe.length * count + 1);
          serije = serije.join('0');
          serije = serije.split('');
          serije = serije.map(parseFloat);
          let i = 0;
          let div = document.getElementById("root");
          div.innerHTML = "";

          while(duration <= trajanje)
          {
            grupe.forEach((gr, index) => {
                let m = el[gr];
                if(i < m.length) {
                    duration = duration + m[i].trajanje;
                    if(duration <= trajanje)
                        serije[index * count +i]++;
                }
            });
            i=(i+1) % count;
          }
          serije.forEach((s, index)=>{
            let gr = Math.trunc(index/count);
            if(s > 0)
            {
                if(index % count === 0)
                {
                  let pp = document.createElement("p");
                  pp.innerHTML = "Grupa mišića: "+prikaz[gr];
                  pp.className = "grupa";
                  div.appendChild(pp);
                }
                let p = document.createElement("p");
                p.className = "vezbe";
                p.innerHTML = "Vežba: "+el[grupe[gr]][index%count].naziv+", Trajanje: "+el[grupe[gr]][index%count].trajanje+"min, Broj ponavljaja: "+el[grupe[gr]][index%count].ponavljaji+" puta, Broj serija: "+s;
                div.appendChild(p);
            }
          });
          let video = document.getElementById("video");
          div.appendChild(video);
          let label = document.createElement("p");
          label.innerHTML = "Preporučeni video snimci za ove grupe vežbi su: ";
          label.className = "grupa";
          video.appendChild(label);
  
          grupe.forEach( gr => {
            let l = el["linkovi"].filter(element => element.tip===gr);
            let v = document.createElement("iframe");
            v.width="400";
            v.height="315";
            v.className="snimci";
            v.src=l[0].url;
            video.appendChild(v);
          });
         
        }));
    }
    static postReview(ime, prezime, tekst, zaposleni)
    {     
      fetch('http://localhost:3000/review', {
              method: 'post',
              headers: {
                'Accept': 'application/json,  text/plain',
                'Content-Type': 'application/json'
              },
              body:JSON.stringify({zaposleni:zaposleni, klijent_ime:ime,klijent_prezime:prezime,tekst:tekst})
            }).then(res=>{
                  res.json();
                  if(res.status === 201 || res.status === 200)
                  {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Vaša ocena je dodata!";
                  }
                  else
                  {
                    const message = document.getElementsByClassName('message')[0];
                    message.innerHTML = "Neuspeh prilikom dodavanja ocene!";
                  }
          });
    }
    static getEmployees()
    {
      return fetch('http://localhost:3000/zaposleni').then(response => response.json()).then(data => {return data});
    }
    static getTrainings() {
        return fetch('http://localhost:3000/treninzi').then(response => response.json()).then(data => {return data});
    }
    static getMassages() {
        return fetch('http://localhost:3000/masaze').then(response => response.json()).then(data => {return data});;
    }
    static getData(url) {
        return Rxjs.Observable.fromPromise(fetch(url).then(response => response.json()));
    }
    static getActivityTypes() {
        return fetch('http://localhost:3000/tipovi').then(response => response.json()).then(data => {return data});
    }
}