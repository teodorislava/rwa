import { Elements } from '../js/elements';

export class Trening
{
    constructor(id, tip, trajanje, trener_ime,
        klijent_ime, klijent_prezime,
         dan, mesec, godina, vreme, cena)
    {
        this.id=id;
        this.tip=tip;
        this.trajanje=trajanje;
        this.klijent_prezime=klijent_prezime;
        this.trener_ime=trener_ime;
        this.klijent_ime=klijent_ime;
        this.dan=dan;
        this.mesec=mesec;
        this.godina=godina;
        this.vreme=vreme;
        this.cena=cena;
    }
    prikaz()
    {
        const main = document.getElementById("root");
        Elements.createBr(main);
        Elements.createBr(main);
        const trening = Elements.createDiv('trening', main, '');
        const tekst = Elements.createDiv('text', trening, '');
        tekst.innerHTML = "Trening: " + this.tip + ", Trajanje: " + this.trajanje +
        "min, Trener: " + this.trener_ime + " " + ", Klijent: " + this.klijent_ime +
        " " + this.klijent_prezime + ", Cena: " + this.cena + ", Vreme: " + this.vreme
        + " , Datum: " + this.dan + "." + this.mesec + "." + this.godina + ".";
    }
}