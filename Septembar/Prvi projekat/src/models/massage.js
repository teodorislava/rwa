import { Elements } from '../js/elements';

export class Masaza
{
    constructor(id, tip, trajanje, maser_ime,
        klijent_ime, klijent_prezime,
         dan, mesec, godina, vreme, cena)
    {
        this.id=id;
        this.tip=tip;
        this.trajanje=trajanje;
        this.klijent_prezime=klijent_prezime;
        this.maser_ime=maser_ime;
        this.klijent_ime=klijent_ime;
        this.dan=dan;
        this.mesec=mesec;
        this.godina=godina;
        this.cena=cena;
        this.vreme=vreme;
    }
    prikaz()
    {
        const main = document.getElementById("root");
        Elements.createBr(main);
        Elements.createBr(main);
        const masaza = Elements.createDiv('masaza', main, '');
        const tekst = Elements.createDiv('text', masaza, '');
        tekst.innerHTML="Masaza: "+ this.tip + ", Trajanje: " + this.trajanje + "min, Maser: "
        + this.maser_ime + " "+ ", Klijent: " + this.klijent_ime + " "+this.klijent_prezime + 
        ", Cena: " + this.cena + ", Vreme: " + this.vreme + " , Datum: "+ this.dan + "." + 
        this.mesec + "." + this.godina + ".";
    }
}
