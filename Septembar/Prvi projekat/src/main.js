import { View } from './js/view';

const view = new View();

const linkF = document.getElementById("fitnes");
linkF.onclick = function()
{
    view.searchData('fitnes');
}

const linkRev = document.getElementById("review");
linkRev.onclick = function()
{
    view.showReview();
}

const linkO = document.getElementById("oNama");
linkO.onclick = function()
{
    view.aboutUs(); 
}

const linkR = document.getElementById("rezervacijeM");
linkR.onclick = function()
{
    view.choseAction("masaza");
}

const linkSpa = document.getElementById("spa");
linkSpa.onclick = function()
{
    view.searchData('masaza');
}

const linkRF = document.getElementById("rezervacijeF");
linkRF.onclick = function()
{
    view.choseAction("fitnes");
   
}

const linkI = document.getElementById("izracunaj");
linkI.onclick = function()
{
    view.onlineTraining();
}

const c = document.getElementById("citat");
c.innerHTML = "Dobrodošli u DreamGym!";
