import { Component, OnInit } from '@angular/core';
import { Reservation } from '../../models/reservation';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { PostClient, PostReservation, getAvailableRooms } from '../../store/actions';
import { Room } from '../../models/room';
import { Subscription, interval } from 'rxjs';
import { Client } from '../../models/client';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  reservation: Reservation = new Reservation("",0,0,0,"","","", 0);
  client: Client = new Client(0, "","","","","","", "",0, "", null, null, "", 0);
  
  reservations$:Observable<Reservation[]>;
  rooms$:Observable<Room[]>;
  responseReservationSuccess$:Observable<string>;
  returnFail$:Observable<boolean>;
  postReservationFail$: Observable<string>;
  returnClientSuccess$: Observable<string>;

  error:boolean = false;
  message:string;
  type:string="pretraga";
  response:boolean = false;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  beds:number;
  res:boolean = false;
  date:Date = new Date();
  subs:Subscription;

  constructor(private store$:Store<State>) { }

  ngOnInit() {
    
    this.reservations$ = this.store$.select(state => state.reservations.reservations);
    this.responseReservationSuccess$ = this.store$.select(state => state.reservations.reservation);
    this.rooms$ = this.store$.select(state => state.reservations.rooms); 
    this.returnFail$ = this.store$.select(state => state.reservations.returnFail);
    this.postReservationFail$ = this.store$.select(state => state.reservations.postReservationFail);
    this.returnClientSuccess$ = this.store$.select(state => state.reservations.client);

  }
  
  onWifi(val)
  {
    if(val.checked)
      this.reservation.WIFI = 1;
    else
      this.reservation.WIFI = 0;
  }

  onAC(val)
  {
    if(val.checked)
      this.reservation.AC=1;
    else
      this.reservation.AC=0;
  }

  onBalcony(val)
  {
    if(val.checked)
      this.reservation.BALCONY=1;
    else
      this.reservation.BALCONY=0;
  }

  onStart(val)
  {
    let d = val.target.value;
    let s = d.split('/');
    if(s.length > 1)
    {
      if(Number(s[0])>=10 && Number(s[1])>=10)
        this.reservation.START=s[2]+'-'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])>=10)
        this.reservation.START=s[2]+'-0'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])<10)
        this.reservation.START=s[2]+'-0'+s[0]+'-0'+s[1]+' 00:00:00';
      else
        this.reservation.START=s[2]+'-'+s[0]+'-0'+s[1]+' 00:00:00';
    }
  }

  onEnd(val)
  {
    let d = val.target.value;
    let s = d.split('/');
    if(s.length > 1)
    {
      if(Number(s[0])>=10 && Number(s[1])>=10)
        this.reservation.FINISH=s[2]+'-'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])>=10)
        this.reservation.FINISH=s[2]+'-0'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])<10)
        this.reservation.FINISH=s[2]+'-0'+s[0]+'-0'+s[1]+' 00:00:00';
      else
        this.reservation.FINISH=s[2]+'-'+s[0]+'-0'+s[1]+' 00:00:00';
    }
  }

  onRequestRooms()
  {
    if(this.beds!=1 && this.beds!=2 && this.beds!=3 && this.beds!=4 && this.beds!=5)
    {
      this.error=true;
      this.message = "Morate uneti ispravan broj kreveta!";
      return;
    }
    if(this.reservation.START=='')
    {
      this.error=true;
      this.message = "Morate odabrati pocetak rezervacije!";
      return;
    }
    if(this.reservation.FINISH=='')
    {
      this.error=true;
      this.message = "Morate uneti kraj rezervacije!";
      return;
    }
    if(this.reservation.TYPE=='')
    {
      this.error=true;
      this.message = "Morate uneti tip sobe!";
      return;
    }
    this.error=false;
    this.store$.dispatch(new getAvailableRooms({reservation: this.reservation, beds:this.beds}));
    this.type = "rezultati";
  }
  onType(val)
  {
    this.reservation.TYPE = val.value;
  }

  onNumberOfBeds(val)
  {
    this.beds=val.target.value;
  }

  onRoomSelect(id:number)
  {
    this.reservation.ID_ROOM=id;
    this.type="rezervacija";
  }

  onReservation()
  {
    if(this.client.NAME=='')
    {
      this.error=true;
      this.message = "Morate uneti ime!";
      return;
    }
     if(this.client.LAST_NAME=='')
    {
      this.error=true;
      this.message = "Morate uneti prezime!";
      return;
    }
     if(this.client.EMAIL=='')
    {
      this.error=true;
      this.message = "Morate uneti mejl!";
      return;
    }
     if(this.client.PHONE_NUMBER=='')
    {
      this.error=true;
      this.message = "Morate uneti broj telefona!";
      return;
    }
     if(this.client.PHONE_NUMBER!='')
    {
      let spl = this.client.PHONE_NUMBER.split('');
      let b = true;
      spl.forEach(el => {
        if(Number(el)!=9 && Number(el)!=8 && Number(el)!=7 && Number(el)!=6 && Number(el)!=5 && Number(el)!=4 && Number(el)!=3 && Number(el)!=2 && Number(el)!=1 && Number(el)!=0)
          b = false;
      });
      if(!b)
      {
        this.error=true;
        this.message = 'Broj telefona se sastoji iskljucivo od brojeva!';
        return;
      }
    }
     if(this.client.JMBG=="" || this.client.JMBG == undefined || this.client.JMBG ==null)
    {
      this.error=true;
      this.message = "Morate uneti maticni broj!";
      return;
    }
    if(this.client.PASSPORT_NUMBER=='')
    {
      this.error=true;
      this.message = "Morate uneti broj pasosa!";
      return;
    }
    if(this.client.PASSPORT_NUMBER!='')
    {
      let spl = this.client.PASSPORT_NUMBER.split('');
      let b = true;
      spl.forEach(el => {
        if(Number(el)!=9 && Number(el)!=8 && Number(el)!=7 && Number(el)!=6 && Number(el)!=5 && Number(el)!=4 && Number(el)!=3 && Number(el)!=2 && Number(el)!=1 && Number(el)!=0)
          b = false;
      });
      if(!b)
      {
        this.error=true;
        this.message = 'Broj pasosa se sastoji iskljucivo od brojeva!';
        return;
      }
    }
     if(this.client.COUNTRY=='')
    {
      this.error=true;
      this.message = "Morate uneti drzavu!";
      return;
    }
     if(this.client.ADDRESS=='')
    {
      this.error=true;
      this.message = "Morate uneti adresu!";
      return;
    }

    this.error=false;
    this.store$.dispatch(new PostReservation(this.reservation));
    this.response=true;
    interval(1000)
    .take(15)
    .subscribe(el => {if(el==14) this.response=false; });
    this.subs = this.responseReservationSuccess$.subscribe(el => {
      if(el!="" && !this.error)
      {
        this.client.ID_RESERVATION = Number(el);
        this.resRoom();
      }
    })
  }

  resRoom()
  {
    this.subs.unsubscribe();
    this.store$.dispatch(new PostClient(this.client));

    this.response=true;
    this.type="zahvalnica";
    interval(1000)
    .take(15)
    .subscribe(el => { if(el==14) this.response=false; })
  }
  onName(val)
  {
    this.client.NAME=val.target.value;
  }
  onLastName(val)
  {
    this.client.LAST_NAME=val.target.value;
  }
  onPassport(val)
  {
    this.client.PASSPORT_NUMBER=val.target.value;
  }
  onJMBG(val)
  {
    this.client.JMBG=val.target.value;
  }
  onEmail(val)
  {
    this.client.EMAIL=val.target.value;
  }
  onCountry(val)
  {
    this.client.COUNTRY=val.value;
  }
  onPhone(val)
  {
    this.client.PHONE_NUMBER=val.target.value;
  }
  onAdress(val)
  {
    this.client.ADDRESS=val.target.value;
  }
}