import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Uses} from '../../models/uses';

@Component({
  selector: 'app-room-service',
  templateUrl: './room-service.component.html',
  styleUrls: ['./room-service.component.css']
})
export class RoomServiceComponent implements OnInit {

  @Output()
  chose:EventEmitter<Uses>= new EventEmitter<Uses>();
  
  uses:Uses = new Uses(0,0,0,0,new Date(), "", "", "", new Date(), false);
  error:boolean = false;
  message:string = "";

  constructor() { }

  ngOnInit() {

  }

  onSelect()
  {
    if(this.uses.SERVICE_NAME=='')
    {
      this.error=true;
      this.message="Morate odabrati uslugu koju želite da naručite!";
      return;
    }
    this.chose.emit(this.uses);
  }
}