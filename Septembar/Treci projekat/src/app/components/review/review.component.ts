import { Component, OnInit } from '@angular/core';
import {Review} from '../../models/review';
import {Store} from '@ngrx/store';
import {Observable, interval} from 'rxjs';
import { PostReview, GetReviews, GetLogged } from '../../store/actions';
import {State} from '../../store/reducers';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../../models/get-details';
import { Client } from '../../models/client';
import {selectors as ReviewSelector} from '../../store/reducers/reviews.reducer';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  reviewList$ : Observable<Review[]>;
  responseAddReview$: Observable<string>;
  loading$ : Observable<boolean>;
  responseAddReviewSuccess$:Observable<string>;

  addReview:boolean = false;
  log:boolean;
  error:boolean=false;
  message:string="";
  success: boolean=false;

  constructor(private store$: Store<State>, private cookieService:CookieService) {
    
   }

  ngOnInit() {
    this.log = this.cookieService.check('token');
    this.reviewList$ = this.store$.select(state => ReviewSelector.selectAll(state.reviews.reviews));
    this.loading$ = this.store$.select(state => state.reviews.loading);
    this.responseAddReview$ = this.store$.select(state => state.reviews.postReviewFail);
    this.responseAddReviewSuccess$ = this.store$.select(state => state.reviews.postReviewSuccess);

    this.store$.dispatch(new GetReviews());
  }

  showAddReview()
  {
    this.addReview=true;
    this.store$.dispatch(new GetLogged(new GetDetails(this.cookieService.get('token'), this.cookieService.get('type'))));
  }

  onReviewAdded(review: Review)
  {
    this.error=false;
    if(review.TEXT=='')
    {
      this.error=true;
      this.message="Morate uneti tekst utiska!";
      return;
    }
    if(review.RATING==0)
    {
      this.error=true;
      this.message="Morate odabrati ocenu hotela!";
      return;
    }
    this.store$.dispatch(new PostReview(review));

    this.success=true;

    interval(1000).
    take(7).
    subscribe(el => {if(el==6) this.success=false;})
  }

}
