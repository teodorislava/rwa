import { Component, OnInit } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { Landmark } from '../../models/landmarks';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import {selectors as landmarksSelectors} from '../../store/reducers/landmarks.reducer';
import { GetLandmarks, AddVisit } from '../../store/actions';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-landmarks',
  templateUrl: './landmarks.component.html',
  styleUrls: ['./landmarks.component.css']
})
export class LandmarksComponent implements OnInit {

  landmarks$:Observable<Landmark[]>;
  responseReturnLandmarks$: Observable<boolean>;
  responseAddVisit$: Observable<string>;
  fail: boolean=false;
  log:boolean = false;

  constructor(private store$: Store<State>, private cookieService: CookieService) { }

  ngOnInit() {
    if(this.cookieService.check('token'))
      this.log=true;
    this.landmarks$ = this.store$.select((state:State) => landmarksSelectors.selectAll(state.landmarks.landmarks));
    this.responseReturnLandmarks$ = this.store$.select((state: State) => state.landmarks.responseReturnLandmarks);
    this.responseAddVisit$ = this.store$.select((state: State) => state.landmarks.responseAddVisit);
    this.store$.dispatch(new GetLandmarks());
  }

  onVisit(l:Landmark)
  {
    this.store$.dispatch(new AddVisit(l));
    this.fail=true;
    interval(1000).
    take(11).
    subscribe(el => { if(el==9) this.fail=false });
  }
}
