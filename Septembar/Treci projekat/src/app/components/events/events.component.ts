import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Event} from '../../models/event';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { GetEvents, PostEvent } from '../../store/actions';
import { Attends } from '../../models/attends';
import { CookieService } from 'ngx-cookie-service';
import { selectors as EventSelectors } from '../../store/reducers/events.reducer';
import { interval } from 'rxjs';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  eventList$:Observable<Event[]>;
  responseEventSignup$:Observable<string>;
  responseReturnEvents$:Observable<boolean>;
  responseEventSignupSuccess$:Observable<string>;

  success:boolean = false;
  fail: boolean = false;

  log:boolean = this.cookieservice.check('token');
  constructor(private store$:Store<State>, private cookieservice:CookieService) { }

  ngOnInit() {
    this.eventList$ = this.store$.select((state:State) => EventSelectors.selectAll(state.events.events));
    this.responseEventSignup$ = this.store$.select((state:State) => state.events.responseEventSignup);
    this.responseReturnEvents$ = this.store$.select((state:State) => state.events.responseReturnEvents);
    this.responseEventSignupSuccess$ = this.store$.select((state:State) => state.events.responseEventSignupSuccess);
    this.store$.dispatch(new GetEvents());
  }

  onEventParticipate(id:number)
  {
    this.store$.dispatch(new PostEvent(new Attends(0, id)));
    this.success=true;
    this.fail=true;

    interval(1000)
    .take(6)
    .subscribe(el => {if(el==5) {this.success=false; this.fail=false;}})
  }
}
