import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { TryLogin } from '../../store/actions';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output()
  public login:EventEmitter<string> = new EventEmitter<string>();

  username:string;
  password:string;
  response$:Observable<Response>;
  logFail$: Observable<boolean>;

  constructor(private cookieService: CookieService,public store$:Store<State>) { }

  ngOnInit() {
    this.response$ = this.store$.select((state:State) => state.logIn.responseLogin);
    this.logFail$ = this.store$.select((state:State) => state.logIn.responseLoginFail);

    this.logFail$.subscribe(el => {
      if(el)
        this.onLoginFail();
    })
  }

  onLogin()
  {
    this.store$.dispatch(new TryLogin({USERNAME:this.username, PASSWORD:this.password}));
    this.response$.subscribe(res => {
          if(res.type !== 'default')
          {
            this.cookieService.set('token', res.text.toString());
            this.cookieService.set('type', 'client');
            this.login.emit('Uspešno ste se ulogovali!');
          }
    });
  }

  onUsername(user)
  {
    this.username=user.target.value;
  }

  onPassword(pass)
  {
    this.password=pass.target.value;
  }

  onLoginFail()
  {
    this.login.emit('Neuspešno prijavljivanje! Pokušajte kasnije!');
  }
}
