import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  myInterval = 3500;
  activeSlideIndex = 0;
 
  slides = [
    {image: 'https://awscloudfront.kempinski.com/34367946/superior_high_resolution.jpg;width=847;height=635;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/1021/top-wide-vareniki.jpg;width=1905;height=794;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/1897/slider_cafe-kranzler-day.jpg;width=847;height=635;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/2241/water-pool-day.jpg;width=1905;height=794;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/34370076/2018013008700mw-1.jpg;width=1905;height=794;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/1083/behandlung.jpg;width=1905;height=794;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/1387/suite-del-mar-dining-room.jpg;width=1200;height=675;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/34359356/brunello-breakfast-buffet.jpg;width=1905;height=794;mode=crop;anchor=middlecenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'},
    {image: 'https://awscloudfront.kempinski.com/1026/kiauh_thai-massage.jpg;width=1024;height=576;mode=crop;anchor=topcenter;autorotate=true;quality=90;scale=both;progressive=true;encoder=freeimage'}
  ];
}
