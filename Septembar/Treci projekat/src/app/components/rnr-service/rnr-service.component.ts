import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Uses } from '../../models/uses';
import { NumberValueAccessor } from '@angular/forms/src/directives';

@Component({
  selector: 'app-rnr-service',
  templateUrl: './rnr-service.component.html',
  styleUrls: ['./rnr-service.component.css']
})
export class RnrServiceComponent implements OnInit {

  @Output()
  chose: EventEmitter<Uses>= new EventEmitter<Uses>();

  @Input()
  public type:string;

  order: Uses = new Uses(0,1,0,0,new Date(),"", "", "", new Date(), false);
  date:Date= new Date();
  start:Date=new Date();
  minTime:Date = new Date();
  maxTime:Date = new Date();
  isMeridian:boolean = false;
  myTime = new Date();
  valid = true;
  error:boolean = false;
  message: string = "";

  constructor() { }

  ngOnInit() {
    this.order.START_TIME.setHours(7);
    this.order.START_TIME.setMinutes(0);
    this.minTime.setHours(7);
    this.minTime.setMinutes(0);
    this.maxTime.setHours(22);
    this.maxTime.setMinutes(0);
  }

  isValid(event: boolean): void 
  {
    this.valid = event;
  }
  
  onOrder()
  {
    this.error=false;
    if(this.order.SERVICE_NAME=='')
    {
      this.error=true;
      this.message="Morate odabrati uslugu koju želite da rezervišete!";
      return;
    }
    if(this.order.DURATION < 10)
    {
      this.error=true;
      this.message="Vreme trajanja mora biti duže od deset minuta!";
      return;
    }
    this.order.START_TIME.setDate(this.start.getDate());
    this.chose.emit(this.order);
  }

}
