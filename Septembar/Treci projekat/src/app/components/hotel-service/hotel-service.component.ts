import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { Service } from '../../models/service';
import { Observable } from 'rxjs/Observable';
import { GetServices, PostService } from '../../store/actions';
import { Uses } from '../../models/uses';
import { CookieService } from 'ngx-cookie-service';
import { interval } from 'rxjs';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-hotel-service',
  templateUrl: './hotel-service.component.html',
  styleUrls: ['./hotel-service.component.css']
})
export class HotelServiceComponent implements OnInit {
  services$:Observable<Service[]>;
  response$:Observable<string>;
  responseServiceFail$:Observable<string>;
  responsePostServiceFail$:Observable<string>;

  modalRef:BsModalRef;
  view:boolean = false;
  subs:any;
  log:boolean=false;
  success:boolean = false;
  fail:boolean = false;


  constructor(public modalService: BsModalService,
    private store$: Store<State>,
    private cookieService:CookieService) { }

  ngOnInit() {
    if(this.cookieService.check('token'))
      this.log=true;
    
    this.services$ = this.store$.select(state => state.services.services);
    this.response$ = this.store$.select(state => state.services.service);
    this.responsePostServiceFail$ = this.store$.select(state => state.services.postServiceFail);
    this.responseServiceFail$ = this.store$.select(state => state.services.returnServicesFail);
    
  }

  openModal(template: TemplateRef<any>) {
    this.success=false;
    this.fail=false;
    this.modalRef = this.modalService.show(template, { class:'modal-lg', animated: true });
  }

  openModalSmall(template: TemplateRef<any>)
  { 
    this.success=false;
    this.fail=false;
    if(this.modalRef!=undefined)
      this.modalRef.hide();
    this.modalRef = this.modalService.show(template, { class:'modal-sm', animated: true });
  }

  onMeal(message, b, l, d)
  {  
    this.modalRef.hide();
    if(message==="dorucak")
      this.openModalSmall(b);
    else if(message==="rucak")
      this.openModalSmall(l);
    else if(message==="vecera")
      this.openModalSmall(d);
  }

  onRnr(uses: Uses)
  {
    this.store$.dispatch(new GetServices());
    this.subs = this.services$.subscribe(el => {
       let s = el.filter(x => x.DESCRIPTION==uses.SERVICE_NAME);
       if(s.length!=0)
       {
          uses.ID_SERVICE = s[0].ID;
          let year = uses.START_TIME.getFullYear();
          let month = uses.START_TIME.getMonth();
          let day = uses.START_TIME.getDate();
          let hours = uses.START_TIME.getHours();
          let minutes = uses.START_TIME.getMinutes();
          let seconds = uses.START_TIME.getSeconds();
          uses.START=year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
          uses.END=null;
          this.postService(uses);
       }
    });
  }

  postService(uses:Uses)
  {
    this.subs.unsubscribe();
    this.modalRef.hide();
    this.store$.dispatch(new PostService(uses));
    this.success = true;
    this.fail= true;

    interval(1000)
    .take(6)
    .subscribe(el => { if(el==5) { this.success = false; this.fail = false;} })
  }

  onActivation(uses, type)
  {
    this.store$.dispatch(new GetServices());
    uses.SERVICE_NAME=type;
    this.subs = this.services$.subscribe(el => {
       let s = el.filter(x => x.DESCRIPTION==uses.SERVICE_NAME);
       if(s.length!=0)
       {
          uses.ID_SERVICE = s[0].ID;
          let year = uses.START_TIME.getFullYear();
          let month = uses.START_TIME.getMonth();
          let day = uses.START_TIME.getDate();
          let hours = uses.START_TIME.getHours();
          let minutes = uses.START_TIME.getMinutes();
          let seconds = uses.START_TIME.getSeconds();
          uses.START = year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
          let yeare = uses.END.getFullYear();
          let monthe = uses.END.getMonth();
          let daye = uses.END.getDate();
          let hourse = uses.END.getHours();
          let minutese = uses.END.getMinutes();
          let secondse = uses.END.getSeconds();
          uses.END_TIME = yeare+'-'+monthe+'-'+daye+' '+hourse+':'+minutese+':'+secondse; 
          this.postService(uses);
       }
    });
  }
  
  onChildCare(uses)
  {
    this.store$.dispatch(new GetServices());
    this.subs = this.services$.subscribe(el => {
       let s = el.filter(x => x.DESCRIPTION==uses.SERVICE_NAME);
       if(s.length!=0)
       {
          uses.ID_SERVICE = s[0].ID;
          let year = uses.START_TIME.getFullYear();
          let month = uses.START_TIME.getMonth();
          let day = uses.START_TIME.getDate();
          let hours = uses.START_TIME.getHours();
          let minutes = uses.START_TIME.getMinutes();
          let seconds = uses.START_TIME.getSeconds();
          uses.START = year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
          let yeare = uses.END.getFullYear();
          let monthe = uses.END.getMonth();
          let daye = uses.END.getDate();
          let hourse = uses.END.getHours();
          let minutese = uses.END.getMinutes();
          let secondse = uses.END.getSeconds();
          uses.END_TIME = yeare+'-'+monthe+'-'+daye+' '+hourse+':'+minutese+':'+secondse; 
          this.postService(uses);
       }
    });
  }

  onRoomService(uses)
  {
    this.store$.dispatch(new GetServices());
    this.subs = this.services$.subscribe(el => {
       let s = el.filter(x => x.DESCRIPTION==uses.SERVICE_NAME);
       if(s.length!=0)
       {
          uses.ID_SERVICE = s[0].ID;
          let year = uses.START_TIME.getFullYear();
          let month = uses.START_TIME.getMonth();
          let day = uses.START_TIME.getDate();
          let hours = uses.START_TIME.getHours();
          let minutes = uses.START_TIME.getMinutes();
          let seconds = uses.START_TIME.getSeconds();
          uses.START = year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
          let yeare = uses.END.getFullYear();
          let monthe = uses.END.getMonth();
          let daye = uses.END.getDate();
          let hourse = uses.END.getHours();
          let minutese = uses.END.getMinutes();
          let secondse = uses.END.getSeconds();
          uses.END_TIME = yeare+'-'+monthe+'-'+daye+' '+hourse+':'+minutese+':'+secondse; 
          this.postService(uses);
       }
    });
  }
}
