import { Component, OnInit } from '@angular/core';
import { Client } from '../../models/client';
import { Observable, interval } from 'rxjs';
import { State } from '../../store/reducers';
import { Store } from '@ngrx/store';
import { 
    GetLogged,
    GetMyEvents, 
    GetMyLandmarks, 
    GetMyServices, 
    GetMyReviews, 
    DeleteVisit, 
    UpdateProfile
} from '../../store/actions';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../../models/get-details';
import {Event} from '../../models/event';
import { Review } from '../../models/review';
import { Service } from '../../models/service';
import { Landmark } from '../../models/landmarks';
import {selectorsInterest as LandmarkSelector} from '../../store/reducers/profile.reducer';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  client$: Observable<Client>;
  tab:string = "licni";
  events$: Observable<Event[]>;
  reviews$: Observable<Review[]>;
  services$: Observable<Service[]>;
  landmarks$: Observable<Landmark[]>;
  deleteVisit$: Observable<string>;
  loading$: Observable<boolean>;
  getLoggedFail$: Observable<string>;
  updateProfileSuccess$:Observable<string>;
  updateProfileFail$:Observable<string>;

  error: boolean = false;
  username:string="";
  password:string="";
  message: string = "";
  update:boolean = false;
  errorInput: boolean = false;

  constructor(private store$: Store<State>, private cookieService:CookieService) { }

  ngOnInit() {

    this.loading$ = this.store$.select(state => state.profile.loading);
    this.client$ = this.store$.select(state => state.logIn.client);
    this.events$ = this.store$.select(state => state.profile.events);
    this.reviews$ = this.store$.select(state => state.profile.reviews);
    this.services$ = this.store$.select(state => state.profile.services);
    this.landmarks$ = this.store$.select(state => LandmarkSelector.selectAll(state.profile.landmarks));
    this.getLoggedFail$ = this.store$.select(state => state.logIn.responseGetDetails);
    this.deleteVisit$ = this.store$.select(state => state.profile.responseDeleteVisit);
    this.updateProfileFail$ = this.store$.select(state => state.profile.responseUpdateProfileFail);
    this.updateProfileSuccess$ = this.store$.select(state => state.profile.responseUpdateProfileSuccess);

    if(this.cookieService.check('token'))
    {
      let token = this.cookieService.get('token');
      let type = this.cookieService.get('type');
      let data = new GetDetails(token, type);
      this.store$.dispatch(new GetLogged(data));

      this.error=true;

      interval(1000)
      .take(8)
      .subscribe(el => { if(el==7) this.error=false; })
    }
  }

  onUpdateProfile()
  {
    if(this.username=='' || this.password=='')
    {
      this.errorInput=true;
      this.message="Popunite sva polja!";
      return;
    }
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    const body = new GetDetails(token, type);

    this.store$.dispatch(new UpdateProfile({USERNAME:this.username, PASSWORD:this.password}));
    this.store$.dispatch(new GetLogged(body));

    this.update=true;
    interval(1000)
      .take(8)
      .subscribe(el => { if(el==7) this.update=false; })
  }

  onChange(tab:string)
  {
    this.tab=tab;
    if(tab=='utisci')
      this.store$.dispatch(new GetMyReviews());
    else if(tab=='dogadjaji')
      this.store$.dispatch(new GetMyEvents());
    else if(tab=='znamenitosti')
      this.store$.dispatch(new GetMyLandmarks());
    else if(tab=='usluge')
      this.store$.dispatch(new GetMyServices());
    else if(tab=='licni')
    {
      let token = this.cookieService.get('token');
      let type = this.cookieService.get('type');
      let data = new GetDetails(token, type);
      this.store$.dispatch(new GetLogged(data));
    }
  }

  removeVisit(l: Landmark)
  {
    this.store$.dispatch(new DeleteVisit(l));
    this.error=true;

    interval(1000)
    .take(7)
    .subscribe( el => {if(el==6) this.error=false;})
  }
}
