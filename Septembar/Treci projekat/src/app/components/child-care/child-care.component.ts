import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Uses } from '../../models/uses';

@Component({
  selector: 'app-child-care',
  templateUrl: './child-care.component.html',
  styleUrls: ['./child-care.component.css']
})
export class ChildCareComponent implements OnInit {

  @Output()
  chose:EventEmitter<Uses> = new EventEmitter<Uses>();

  uses:Uses = new Uses(0,1,0,0,new Date(),"", "", "Vrtic", new Date(), false);
  minTime: Date = new Date();
  maxTime: Date = new Date();
  error:boolean = false;
  message:string = "";
  isMeridian:boolean = false;

  constructor() { }

  ngOnInit() {
    this.uses.START_TIME.setHours(7);
    this.uses.START_TIME.setMinutes(0);
    this.uses.END.setHours(7);
    this.uses.END.setMinutes(0);
    this.minTime.setHours(7);
    this.minTime.setMinutes(0);
    this.maxTime.setHours(22);
    this.maxTime.setMinutes(0);
  }

  onClick()
  {
    this.error=false;

    if(this.uses.START_TIME==null)
    {
      this.error=true;
      this.message="Morate uneti vreme pocetka!";
      return;
    }
    if(this.uses.END==null)
    {
      this.error=true;
      this.message="Morate uneti vreme kraja!";
      return;
    }

    this.chose.emit(this.uses);
  }
  
}
