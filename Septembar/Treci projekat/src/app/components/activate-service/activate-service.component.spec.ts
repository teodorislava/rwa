import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivateServiceComponent } from './activate-service.component';

describe('ActivateServiceComponent', () => {
  let component: ActivateServiceComponent;
  let fixture: ComponentFixture<ActivateServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivateServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivateServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
