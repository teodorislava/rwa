import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import {Router} from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import { interval, Observable, of } from 'rxjs';

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.css']
})
export class StartpageComponent implements OnInit {

  modalRef:BsModalRef;
  public msg:string="";
  log:boolean = false;
  showRes$: Observable<boolean>;
  profile:boolean=false;

  constructor(public modalService: BsModalService, private router:Router, private cookieService: CookieService) { }

  ngOnInit() {
    if(this.cookieService.check('token'))
      this.log=true;

      interval(500)
      .subscribe(el => {
        if(this.cookieService.check('token'))
        {
          this.log=true;
          this.showRes$=of(true);
        }
        else
          this.showRes$=of(false);
      })
  }

  openProfile()
  {
    this.router.navigate(['./startpage']);
    this.profile=true;
  }

  openModal(template: TemplateRef<any>) {
    if(this.modalRef!=undefined)
      this.modalRef.hide();
    this.modalRef = this.modalService.show(template, { class:'modal-sm', animated: true });
  }


  onLoggedIn(msg:string, tempOld: TemplateRef<any>, tempNew:TemplateRef<any>)
  {
    this.msg=msg;
    this.openModal(tempNew);
  }

  onMsgClosed()
  {
    this.modalRef.hide();
    this.router.onSameUrlNavigation='reload';
    this.router.navigate(['./startpage']);
  }

  logOut(temp: TemplateRef<any>)
  {
    if(this.cookieService.check('token'))
    {
      this.cookieService.delete('token');
      this.cookieService.delete('type');
      this.msg="Uspešno ste se odjavili!";
      this.profile = false;
      this.log=false;
      this.openModal(temp);
    }
    else
    {
      this.msg="Niste prijavljeni!";
      this.openModal(temp);
    }
  }
  
}
