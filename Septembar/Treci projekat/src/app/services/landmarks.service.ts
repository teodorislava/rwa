import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Landmark } from '../models/landmarks';
import { Interest } from '../models/interest';
import { CookieService } from 'ngx-cookie-service';
import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/throw';

@Injectable()
export class LandmarksService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getLandmarks(): Observable<Landmark[]>
  {
    const url='http://localhost:8000/api/landmark';
    return this.http.get<Landmark[]>(url).
      pipe(catchError((error:Response) => Observable.throw("Greska prilikom citanja znamenitosti!")));
  }

  addVisit(payload: Landmark): Observable<Landmark>
  {
    const interest = new Interest(0,0,payload.id);
    const url="http://localhost:8000/api/interests";
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.post<Landmark>(url, payload, {
      headers: {
        'Authorization':token, 
        'Pragma':type
      }
    }).
      pipe(catchError((error:Response) => Observable.throw("Greska prilikom dodavanja posete!")));
  }

  deleteVisit(payload: number)
  {
    if(payload!=undefined)
    {
      const token = this.cookieService.get('token');
      const type = this.cookieService.get('type');
      const url="http://localhost:8000/api/interests/"+payload;
      return this.http.delete<Landmark[]>(url, {
        headers: {
          'Authorization':token,
          'Pragma':type
        }
      }).
      pipe(catchError((error:Response) => Observable.throw("Greska prilikom brisanja posete!")));
    }
  }

}
