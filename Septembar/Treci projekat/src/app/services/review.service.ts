import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Review } from '../models/review';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../models/get-details';
import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/throw';

@Injectable()
export class ReviewService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  createReview(payload:Review) 
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    let url='http://localhost:8000/api/review';
    return this.http.post<Review>(url, payload, {
      headers: {
        'Authorization':token, 
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom kreiranja utiska!")));
  }

  getReviews(): Observable<Review[]>
  {
    let url='http://localhost:8000/api/review';
    return this.http.get<Review[]>(url).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja utisaka!")));
  }
}
