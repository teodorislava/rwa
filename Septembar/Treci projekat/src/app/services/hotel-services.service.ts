import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Service } from '../models/service';
import { HttpClient } from '@angular/common/http';
import { Uses } from '../models/uses';
import { CookieService } from 'ngx-cookie-service';
import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/throw';

@Injectable()
export class HotelServicesService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getServices(): Observable<Service[]>
  {
    let url='http://localhost:8000/api/service';
    return this.http.get<Service[]>(url).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom pribavljanja servisa!")));
  }

  createService(payload:Uses) : Observable<Response>
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    let url='http://localhost:8000/api/uses';
    return this.http.post<Response>(url, payload, {
      headers: {
        'Authorization':token, 
        'Pragma':type
      }
    })
      .pipe(catchError((error:any) => Observable.throw("Greska prilikom rezervacije usluga!")));
  }

  getUses(): Observable<Uses[]>
  {
    let url='http://localhost:8000/api/uses';
    return this.http.get<Uses[]>(url).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom pribavljanja uplacenih usluga!")));
  }
}
