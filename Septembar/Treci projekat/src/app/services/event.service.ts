import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Event } from '../models/event';
import { HttpClient } from '@angular/common/http';
import { Attends } from '../models/attends';
import {CookieService} from 'ngx-cookie-service';
import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/throw';

@Injectable()
export class EventService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getEvents(): Observable<Event[]>
  {
    let url='http://localhost:8000/api/event';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.get<Event[]>(url, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja dogadjaja!")));
  }

  createEvent(payload:Attends) 
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    let url='http://localhost:8000/api/attends';
    return this.http.post<Event>(url, payload, {
      headers: {
        'Authorization':token, 
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom prijavljivanja na dogadjaj!")));
  }
}