import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';
import { catchError } from 'rxjs/operators/catchError';
import { Landmark } from '../models/landmarks';
import { Service } from '../models/service';
import { Review } from '../models/review';
import { Event } from '../models/event';
import { Client } from '../models/client';
import { GetDetails } from '../models/get-details';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http:HttpClient, private cookieService: CookieService) { }

  getMyEvents(): Observable<Event[]>
  {
    let url='http://localhost:8000/api/myevents';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.get<Event[]>(url, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja dogadjaja!")));
  }

  getMyLandmarks(): Observable<Landmark[]>
  {
    let url='http://localhost:8000/api/mylandmarks';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.get<Landmark[]>(url, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja dogadjaja!")));
  }

  getMyServices(): Observable<Service[]>
  {
    let url='http://localhost:8000/api/myservices';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.get<Service[]>(url, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja dogadjaja!")));
  }

  getMyReviews(): Observable<Review[]>
  {
    let url='http://localhost:8000/api/myreviews';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.get<Review[]>(url, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja dogadjaja!")));
  }

  updateProfile(payload): Observable<Response>
  {
    let url='http://localhost:8000/api/change-credentials';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    return this.http.put<Response>(url, payload, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom azuriranja profila!")));
  }
}
