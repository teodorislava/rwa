import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Reservation } from '../models/reservation';
import { HttpClient } from '@angular/common/http';
import { Room } from '../models/room';
import { CookieService } from 'ngx-cookie-service';
import { Client } from '../models/client';
import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/throw';

@Injectable()
export class ReservationsService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getReservations(): Observable<Reservation[]>
  {
    const url='http://localhost:8000/api/reservation';
    return this.http.get<Reservation[]>(url).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja rezervacija!")));
  }

  getRooms(): Observable<Room[]>
  {
    let url='http://localhost:8000/api/room';
    return this.http.get<Room[]>(url).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja soba!")));
  }

  createReservation(payload:Reservation): Observable<Response>
  {
    let url='http://localhost:8000/api/reservation';
    return this.http.post<Response>(url, payload).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom kreiranja rezervacije!")));
  }

  createClient(payload:Client): Observable<Response>
  {
    let url='http://localhost:8000/api/client';
    return this.http.post<Response>(url, payload).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom kreiranja klijenta!")));
  }
}
