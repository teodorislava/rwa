import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from '../models/client';

import { catchError } from 'rxjs/operators/catchError';
import 'rxjs/add/observable/throw';

@Injectable()
export class LoginService {

  constructor(private http:HttpClient) { }

  getDetails(token:string, type:string): Observable<Client>
  {
    let url='http://localhost:8000/api/get-details';
    return this.http.get<Client>(url, {
      headers: {
        'Authorization':token,
        'Pragma':type
      }
    }).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom citanja ulogovanog korisnika!")));
  }

  tryLogin(payload) : Observable<Response>
  {
    let url='http://localhost:8000/api/login';
    return this.http.post<Response>(url, payload).
      pipe(catchError((error:any) => Observable.throw("Greska prilikom logovanja!")));
  }
}