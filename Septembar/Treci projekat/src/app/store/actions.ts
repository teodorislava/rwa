import {Action} from '@ngrx/store';

//models
import {Review} from '../models/review';
import { Event } from '../models/event';
import { Attends } from '../models/attends';
import { Uses } from '../models/uses';
import { Service } from '../models/service';
import { Client } from '../models/client';
import { Reservation } from '../models/reservation';
import { Room } from '../models/room';
import { GetDetails } from '../models/get-details';
import { Landmark } from '../models/landmarks';
import { Interest } from '../models/interest';

//action types 

export const POST_REVIEW = "Post review";
export const POST_REVIEW_SUCCESS = "Post review success";
export const POST_REVIEW_FAIL = "Post review fail";
export const GET_REVIEWS = "Get reviews";
export const RETURN_REVIEWS = "Return reviews";
export const RETURN_REVIEWS_FAIL = "Return reviews fail";

export const GET_EVENTS="Get events";
export const RETURN_EVENTS="Return events";
export const RETURN_EVENTS_FAIL="Return events fail";
export const EVENT_SIGNUP="Event sign up";
export const EVENT_SIGNUP_SUCCESS="Event sign up succes";
export const EVENT_SIGNUP_FAIL="Event signup fail";

export const GET_SERVICES="Get services";
export const RETURN_SERVICES="Return services";
export const RETURN_SERVICES_FAIL="Return services fail";
export const POST_SERVICE="Post service";
export const POST_SERVICE_SUCCESS="Post service success";
export const POST_SERVICE_FAIL="Post service fail";
export const GET_USES="Get uses";
export const RETURN_USES="Return uses";
export const RETURN_USES_FAIL="Return uses fail";

export const TRY_LOGIN="Try login";
export const LOGIN_RESPONSE="Login response";
export const LOGIN_FAIL="Login fail";
export const GET_LOGGED="Get logged";
export const RETURN_LOGGED="Return logged";
export const RETURN_LOGGED_FAIL="Return logged fail";

export const POST_RESERVATION="Post reservation";
export const POST_RESERVATION_SUCCESS="Post reservation response";
export const POST_RESERVATION_FAIL="Post reservation fail";
export const GET_RESERVATIONS="Get reservations";
export const RETURN_RESERVATIONS="Return reservations";
export const RETURN_RESERVATIONS_FAIL="Return reservations fail";

export const GET_ROOMS="Get rooms";
export const RETURN_ROOMS="Return rooms";
export const RETURN_ROOMS_FAIL="Return rooms fail";

export const POST_CLIENT="Post client";
export const POST_CLIENT_SUCCESS="Post client response";
export const POST_CLIENT_FAIL="Post client fail";

export const GET_LANDMARKS="Get landmarks";
export const RETURN_LANDMARKS="Return landmarks";
export const RETURN_LANDMARKS_FAIL="Return landmarks fail";
export const ADD_VISIT = "Add visit";
export const ADD_VISIT_SUCCESS="Add visit success";
export const ADD_VISIT_FAIL="Add visit fail";
export const DELETE_VISIT = "Delete visit";
export const DELETE_VISIT_RESPONSE = "Delete visit response";
export const DELETE_VISIT_FAIL = "Delete visit fail";
export const GET_INTEREST="Get interests";
export const RETURN_INTEREST="Return interests";
export const RETURN_INTEREST_FAIL="Return interests fail";

export const GET_MY_REVIEWS="Get my reviews";
export const RETURN_MY_REVIEWS_SUCCESS="Return my reviews success";
export const RETURN_MY_REVIEWS_FAIL="Return my reviews fail";
export const GET_MY_SERVICES="Get my services";
export const RETURN_MY_SERVICES_SUCCESS="Return my services success";
export const RETURN_MY_SERVICES_FAIL="Return my services fail";
export const GET_MY_LANDMARKS="Get my landmarks";
export const RETURN_MY_LANDMARKS_SUCCESS="Return my landmarks success";
export const RETURN_MY_LANDMARKS_FAIL="Return my landmarks fail";
export const GET_MY_EVENTS="Get my events";
export const RETURN_MY_EVENTS_SUCCESS="Return my events success";
export const RETURN_MY_EVENTS_FAIL="Return my events fail";
export const UPDATE_PROFILE="Update profile";
export const UPDATE_PROFILE_SUCCESS="Update profile success";
export const UPDATE_PROFILE_FAIL="Update profile fail";

export const GET_AVAILABLE_ROOMS = "get available rooms";
export const GET_AVAILABLE_ROOMS_SUCCESS="get available rooms success";
export const GET_AVAILABLE_ROOMS_FAIL="get available rooms fail";

export class PostEvent implements Action
{
    type = EVENT_SIGNUP;
    constructor(public payload: Attends) {}
}
export class PostEventSuccess implements Action
{
    type = EVENT_SIGNUP_SUCCESS;
    constructor(public payload: Event) {}
}
export class PostEventFail implements Action
{
    type = EVENT_SIGNUP_FAIL;
    constructor(public payload: string) {}
}
export class GetEvents implements Action
{
    type = GET_EVENTS;
    constructor() {}
}
export class ReturnEvents implements Action
{
    type = RETURN_EVENTS;
    constructor(public payload: Event[]) {}
}
export class ReturnEventsFail implements Action
{
    type = RETURN_EVENTS_FAIL;
    constructor(public payload: string) {}
}

export class PostReview implements Action
{
    type = POST_REVIEW;
    constructor(public payload: Review) {}
}
export class PostReviewSuccess implements Action
{
    type = POST_REVIEW_SUCCESS;
    constructor(public payload: Review) {}
}
export class PostReviewFail implements Action
{
    type = POST_REVIEW_FAIL;
    constructor(public payload: string) {}
}
export class GetReviews implements Action
{
    type = GET_REVIEWS;
    constructor() {}
}
export class ReturnReviews implements Action
{
    type = RETURN_REVIEWS;
    constructor(public payload: Review[]) {}
}
export class ReturnReviewsFail implements Action
{
    type = RETURN_REVIEWS_FAIL;
    constructor(public payload: string) {}
}

export class GetServices implements Action
{
    type = GET_SERVICES;
    constructor() {}
}
export class ReturnServices implements Action
{
    type = RETURN_SERVICES;
    constructor(public payload: Service[]) {}
}
export class ReturnServicesFail implements Action
{
    type = RETURN_SERVICES_FAIL;
    constructor(public payload: string) {}
}
export class PostService implements Action
{
    type = POST_SERVICE;
    constructor(public payload: Uses) {}
}
export class PostServiceSuccess implements Action
{
    type = POST_SERVICE_SUCCESS;
    constructor(public payload: string) {}
}
export class PostServiceFail implements Action
{
    type = POST_SERVICE_FAIL;
    constructor(public payload: string) {}
}
export class GetUses implements Action
{
    type = GET_USES;
    constructor() {}
}
export class ReturnUses implements Action
{
    type = RETURN_USES;
    constructor(public payload: Uses[]) {}
}
export class ReturnUsesFail implements Action
{
    type = RETURN_USES_FAIL;
    constructor(public payload: string) {}
}

export class GetLogged implements Action
{
    type = GET_LOGGED;
    constructor(public payload:GetDetails) {}
}
export class ReturnLogged implements Action
{
    type = RETURN_LOGGED;
    constructor(public payload: Client) {}
}
export class ReturnLoggedFail implements Action
{
    type = RETURN_LOGGED_FAIL;
    constructor(public payload: string) {}
}
export class TryLogin implements Action
{
    type = TRY_LOGIN;
    constructor(public payload) {}
}
export class LoginResponse implements Action
{
    type = LOGIN_RESPONSE;
    constructor(public payload: Response) {}
}
export class LoginFail implements Action
{
    type = LOGIN_FAIL;
    constructor(public payload: string) {}
}

export class GetReservations implements Action
{
    type = GET_RESERVATIONS;
    constructor() {}
}
export class ReturnReservations implements Action
{
    type = RETURN_RESERVATIONS;
    constructor(public payload: Reservation[]) {}
}
export class ReturnReservationsFail implements Action
{
    type = RETURN_RESERVATIONS_FAIL;
    constructor(public payload: string) {}
}
export class PostReservation implements Action
{
    type = POST_RESERVATION;
    constructor(public payload: Reservation) {}
}
export class PostReservationSuccess implements Action
{
    type = POST_RESERVATION_SUCCESS;
    constructor(public payload: string) {}
}
export class PostReservationFail implements Action
{
    type = POST_RESERVATION_FAIL;
    constructor(public payload: string) {}
}

export class GetRooms implements Action
{
    type = GET_ROOMS;
    constructor() {}
}
export class ReturnRooms implements Action
{
    type = RETURN_ROOMS;
    constructor(public payload: Room[]) {}
}
export class ReturnRoomsFail implements Action
{
    type = RETURN_ROOMS_FAIL;
    constructor(public payload: string) {}
}

export class PostClient implements Action
{
    type = POST_CLIENT;
    constructor(public payload: Client) {}
}
export class PostClientSuccess implements Action
{
    type = POST_CLIENT_SUCCESS;
    constructor(public payload: string) {}
}
export class PostClientFail implements Action
{
    type = POST_CLIENT_FAIL;
    constructor(public payload: string) {}
}

export class GetLandmarks implements Action
{
    type = GET_LANDMARKS;
    constructor() {}
}
export class ReturnLandmarks implements Action
{
    type = RETURN_LANDMARKS;
    constructor(public payload: Landmark[]) {}
}
export class ReturnLandmarksFail implements Action
{
    type = RETURN_LANDMARKS_FAIL;
    constructor(public payload: string) {}
}
export class GetInterest implements Action
{
    type = GET_INTEREST;
    constructor() {}
}
export class ReturnInterest implements Action
{
    type = RETURN_INTEREST;
    constructor(public payload: Interest[]) {}
}
export class ReturnInterestFail implements Action
{
    type = RETURN_INTEREST_FAIL;
    constructor(public payload: string) {}
}
export class AddVisit implements Action
{
    type = ADD_VISIT;
    constructor(public payload: Landmark) {}
}
export class AddVisitSuccess implements Action
{
    type = ADD_VISIT_SUCCESS;
    constructor(public payload: Landmark) {}
}
export class AddVisitFail implements Action
{
    type = ADD_VISIT_FAIL;
    constructor(public payload: string) {}
}
export class DeleteVisit implements Action
{
    type = DELETE_VISIT;
    constructor(public payload: Landmark) {}
}
export class DeleteVisitResponse implements Action
{
    type = DELETE_VISIT_RESPONSE;
    constructor(public payload: Landmark[]) {}
}
export class DeleteVisitFail implements Action
{
    type = DELETE_VISIT_FAIL;
    constructor(public payload: string) {}
}

export class GetMyEvents implements Action
{
    type = GET_MY_EVENTS;
    constructor() {}
}
export class ReturnMyEvents implements Action
{
    type = RETURN_MY_EVENTS_SUCCESS;
    constructor(public payload: Event[]) {}
}
export class ReturnMyEventsFail implements Action
{
    type = RETURN_MY_EVENTS_FAIL;
    constructor(public payload: string) {}
}
export class GetMyReviews implements Action
{
    type = GET_MY_REVIEWS;
    constructor() {}
}
export class ReturnMyReviews implements Action
{
    type = RETURN_MY_REVIEWS_SUCCESS;
    constructor(public payload: Review[]) {}
}
export class ReturnMyReviewsFail implements Action
{
    type = RETURN_MY_REVIEWS_FAIL;
    constructor(public payload: string) {}
}
export class GetMyLandmarks implements Action
{
    type = GET_MY_LANDMARKS;
    constructor() {}
}
export class ReturnMyLandmarks implements Action
{
    type = RETURN_MY_LANDMARKS_SUCCESS;
    constructor(public payload: Landmark[]) {}
}
export class ReturnMyLandmarksFail implements Action
{
    type = RETURN_MY_LANDMARKS_FAIL;
    constructor(public payload: string) {}
}
export class GetMyServices implements Action
{
    type = GET_MY_SERVICES;
    constructor() {}
}
export class ReturnMyServices implements Action
{
    type = RETURN_MY_SERVICES_SUCCESS;
    constructor(public payload: Service[]) {}
}
export class ReturnMyServicesFail implements Action
{
    type = RETURN_MY_SERVICES_FAIL;
    constructor(public payload: string) {}
}
export class UpdateProfile implements Action
{
    type = UPDATE_PROFILE;
    constructor(public payload) {}
}
export class UpdateProfileSuccess implements Action
{
    type = UPDATE_PROFILE_SUCCESS;
    constructor(public payload: string) {}
}
export class UpdateProfileFail implements Action
{
    type = UPDATE_PROFILE_FAIL;
    constructor(public payload: string) {}
}

export class getAvailableRooms implements Action {
    type = GET_AVAILABLE_ROOMS;
    constructor(public payload: {reservation:Reservation, beds: number}) {}
}
export class getAvailableRoomsSuccess implements Action {
    type = GET_AVAILABLE_ROOMS_SUCCESS;
    constructor(public payload) {console.log(payload)}
}
export class getAvailableRoomsFail implements Action {
    type = GET_AVAILABLE_ROOMS_FAIL;
    constructor(public payload: string) {} 
}