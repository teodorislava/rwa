import * as ReservationActions from '../actions';
import {Action} from '@ngrx/store';
import { Reservation } from '../../models/reservation';
import { Room } from '../../models/room';

export interface ReservationsState
{
    loading:boolean,
    reservations:Reservation[],
    rooms: Room[],
    reservation:string,
    client: string,
    returnFail:boolean,
    postClientFail:boolean, 
    postReservationFail:string
}

const initialState: ReservationsState = {
    loading:true, 
    reservations:[],
    rooms:[],
    reservation: "",
    client: "",
    returnFail:false,
    postClientFail:false, 
    postReservationFail:""
}

export function reservationsReducer(state: ReservationsState = initialState, action:Action)
{
    switch(action.type) 
    {
        case ReservationActions.GET_ROOMS:
            return {...state};
        case ReservationActions.RETURN_ROOMS:
        {
            const {payload} = (action as ReservationActions.ReturnRooms);
            return {...state, rooms:payload, loading:false};
        }
        case ReservationActions.RETURN_ROOMS_FAIL:
        {
            const {payload} = (action as ReservationActions.ReturnRoomsFail);
            return {...state, returnFail:true, loading:false};
        }
        case ReservationActions.GET_RESERVATIONS:
            return {...state};
        case ReservationActions.RETURN_RESERVATIONS:
        {
            const {payload} = (action as ReservationActions.ReturnReservations);
            return {...state, reservations:payload,  loading:false};
        }
        case ReservationActions.RETURN_RESERVATIONS_FAIL:
        {
            const {payload} = (action as ReservationActions.ReturnReservationsFail);
            return {...state, returnFail:true, loading:false}
        }
        case ReservationActions.POST_CLIENT_SUCCESS:
        {
            const {payload} = (action as ReservationActions.PostClientSuccess);
            return {...state, client:payload, loading:false}
        }
        case ReservationActions.POST_CLIENT_FAIL:
        {
            const {payload} = (action as ReservationActions.PostClientFail);
            return {...state, returnFail:true, loading:false}
        }
        case ReservationActions.POST_RESERVATION_SUCCESS:
        {
            const {payload} = (action as ReservationActions.PostReservationSuccess);
            return {...state, reservation: payload, loading:false};
        }
        case ReservationActions.POST_RESERVATION_FAIL:
        {
            const {payload} = (action as ReservationActions.PostReservationFail);
            return {...state, postReservationFail: payload, loading:false};
        }
        case ReservationActions.GET_AVAILABLE_ROOMS_SUCCESS: 
        {
            const {payload} = (action as ReservationActions.getAvailableRoomsSuccess);
            return {...state, rooms: payload, loading: false};
        }
        case ReservationActions.GET_AVAILABLE_ROOMS_FAIL: 
        {
            const {payload} = (action as ReservationActions.getAvailableRoomsFail);
            return {...state, reservation: payload, loading:false};
        }
        default:
            return state;
    }
}