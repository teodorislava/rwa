import {Event} from '../../models/event';
import {Action} from '@ngrx/store';
import * as EventActions from '../actions';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

interface Events extends EntityState<Event> {};
const adapter: EntityAdapter<Event> = createEntityAdapter<Event>();

const initialEvents: Events = {
    ids: [],
    entities: {}
}

export interface EventsState
{
    loading:boolean,
    events:Events,
    event:Event
    responseEventSignup:string,
    responseReturnEvents:boolean,
    responseEventSignupSuccess:string
}

const initialState:EventsState = {
    loading: true,
    events: initialEvents,
    event: {
        NAME:"",
        START: new Date(),
        id: 0,
        CAPACITY:0,
        PRICE:0,
        OPIS:"",
    },
    responseEventSignup:"",
    responseReturnEvents:false,
    responseEventSignupSuccess:""
};

export function eventsReducer(state: EventsState = initialState, action:Action)
{
    switch(action.type) {
        case EventActions.EVENT_SIGNUP:
            return {...state};
        case EventActions.EVENT_SIGNUP_SUCCESS:
        {
            const {payload} = (action as EventActions.PostEventSuccess);
            const adapt = adapter.updateOne({id:payload.id, changes:payload}, state.events);
            return {...state, events:adapt, responseEventSignupSuccess:'Uspesno ste se prijavili na dogadjaj!'};
        }
        case EventActions.EVENT_SIGNUP_FAIL:
        {
            const {payload} = (action as EventActions.PostEventFail);
            return {...state, responseEventSignup:payload};
        }
        case EventActions.RETURN_EVENTS:
        {
            const {payload} = (action as EventActions.ReturnEvents);
            const adapt = adapter.addAll(payload, state.events);
            return {...state, events:adapt};
        }
        case EventActions.RETURN_EVENTS_FAIL:
        {
            const {payload} = (action as EventActions.ReturnEventsFail);
            return {...state, responseReturnEvents:true};
        }
        default:
            return state;
    }
}

export const selectors = adapter.getSelectors();