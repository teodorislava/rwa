import * as ReviewActions from '../actions';
import {Review} from '../../models/review';
import {Action} from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

interface Reviews extends EntityState<Review> {};
const adapter: EntityAdapter<Review> = createEntityAdapter<Review>();

export interface ReviewsState {
    loading: boolean,
    reviews: Reviews,
    review: Review,
    postReviewFail:string,
    postReviewSuccess:string
}

const initialReviews: Reviews = {
    ids: [],
    entities: {}
}

const initialState: ReviewsState = {
    loading: true,
    reviews: initialReviews,
    review: {
        id: 0,
        RATING: 0,
        TEXT:"",
        ID_CLIENT:0
    },
    postReviewFail:"",
    postReviewSuccess:""
}

export function reviewsReducer(state: ReviewsState = initialState, action:Action)
{
    switch(action.type) 
    {
        case ReviewActions.POST_REVIEW:
            return state;
            
        case ReviewActions.POST_REVIEW_SUCCESS:
        {
            const {payload} = (action as ReviewActions.PostReviewSuccess);
            const adapt = adapter.addOne(payload, state.reviews);
            return Object.assign({...state, reviews:adapt, loading:false, postReviewSuccess:'Uspesno ste dodali utisak!'});
        }
        case ReviewActions.POST_REVIEW_FAIL:
        {
            const {payload} = (action as ReviewActions.PostReviewFail);
            return {...state, postReviewFail:payload, loading:false};
        }
        case ReviewActions.RETURN_REVIEWS:
        {
            const {payload} = (action as ReviewActions.ReturnReviews);
            const adapt = adapter.addAll(payload, state.reviews);
            return Object.assign({...state, reviews:adapt, loading:false});
        }
        case ReviewActions.RETURN_REVIEWS_FAIL:
        {
            const {payload} = (action as ReviewActions.ReturnReviewsFail);
            return {...state, loading:true};
        }
        default:
            return state;
    }
}

export const selectors = adapter.getSelectors();