import * as ProfileActions from '../actions';
import {Action} from '@ngrx/store';
import { Review } from '../../models/review';
import { Event } from '../../models/event';
import { Service } from '../../models/service';
import {EntityState, createEntityAdapter, EntityAdapter} from '@ngrx/entity';
import { Landmark } from '../../models/landmarks';

export interface LandmarksInterest extends EntityState<Landmark> {};
const adapterI: EntityAdapter<Landmark> = createEntityAdapter<Landmark> ();

export const initialInterests: LandmarksInterest = {
    ids: [],
    entities: {

    }
}

export interface ProfileState
{
    loading: boolean,
    reviews: Review[],
    services: Service[],
    events: Event[],
    landmarks: LandmarksInterest,
    responseDeleteVisit:string,
    responseUpdateProfileSuccess:string,
    responseUpdateProfileFail:string
}

const initialState: ProfileState = {
    loading:false,
    reviews: [],
    services: [],
    events: [],
    landmarks: initialInterests,
    responseDeleteVisit:"",
    responseUpdateProfileSuccess:"",
    responseUpdateProfileFail:""
}

export function profileReducer(state: ProfileState = initialState, action:Action)
{
    switch(action.type) 
    {
        case ProfileActions.RETURN_MY_EVENTS_SUCCESS:
        {
            const {payload} = (action as ProfileActions.ReturnMyEvents);
            return {...state, events:payload,  loading:false};
        }
        case ProfileActions.RETURN_MY_EVENTS_FAIL:
        {
            const {payload} = (action as ProfileActions.ReturnMyEventsFail);
            return {...state, responseMyEvents:payload, loading:true};
        }
        case ProfileActions.RETURN_MY_LANDMARKS_SUCCESS:
        {
            const {payload} = (action as ProfileActions.ReturnMyLandmarks);
            const ad = adapterI.addAll(payload, state.landmarks);
            return {...state, landmarks:ad, loading:false};
        }
        case ProfileActions.RETURN_MY_LANDMARKS_FAIL:
        {
            const {payload} = (action as ProfileActions.ReturnMyLandmarksFail);
            return {...state, responseMyLandmarks:payload, loading:true}
        }
        case ProfileActions.RETURN_MY_REVIEWS_SUCCESS:
        {
            const {payload} = (action as ProfileActions.ReturnMyReviews);
            return {...state, reviews:payload, loading:false}
        }
        case ProfileActions.RETURN_MY_REVIEWS_FAIL:
        {
            const {payload} = (action as ProfileActions.ReturnMyReviewsFail);
            return {...state, responseMyReviews:payload, loading:true};
        }
        case ProfileActions.RETURN_MY_SERVICES_SUCCESS:
        {
            const {payload} = (action as ProfileActions.ReturnMyServices);
            return {...state, services: payload, loading:false};
        }
        case ProfileActions.RETURN_MY_SERVICES_FAIL:
        {
            const {payload} = (action as ProfileActions.ReturnMyServicesFail);
            return {...state, responseMyServices: payload, loading:true};
        }
        case ProfileActions.DELETE_VISIT_RESPONSE:
        {
            const {payload} = (action as ProfileActions.DeleteVisitResponse);
            const ad =  adapterI.addAll(payload, state.landmarks);
            return Object.assign({...state, landmarks:ad, loading:false});
        }
        case ProfileActions.DELETE_VISIT_FAIL:
        {
            const {payload} = (action as ProfileActions.DeleteVisitFail);
            return {...state, responseDeleteVisit:payload, loading:true};
        }
        case ProfileActions.UPDATE_PROFILE_SUCCESS:
        {
            const {payload} = (action as ProfileActions.UpdateProfileSuccess);
            return Object.assign({...state, responseUpdateProfileSuccess:payload, loading:false});
        }
        case ProfileActions.UPDATE_PROFILE_FAIL:
        {
            const {payload} = (action as ProfileActions.UpdateProfileFail);
            return {...state, responseUpdateProfileFail:payload, loading:true};
        }
        default:
            return state;
    }
}

export const selectorsInterest = adapterI.getSelectors();