import * as ServiceActions from '../actions';
import {Service} from '../../models/service';
import {Action} from '@ngrx/store';

export interface ServicesState
{
    loading:boolean,
    services:Service[],
    service: string,
    returnServicesFail:string, 
    postServiceFail:string
}

const initialState:ServicesState = {
    loading:true,
    services:[],
    service: "",
    returnServicesFail:"", 
    postServiceFail:""
}

export function servicesReducer(state: ServicesState = initialState, action:Action)
{
    switch(action.type) 
    {   
        case ServiceActions.RETURN_SERVICES:
        {
            const {payload} = (action as ServiceActions.ReturnServices);
            return {...state, services:payload, loading:false};
        }
        case ServiceActions.RETURN_SERVICES_FAIL:
        {
            const {payload} = (action as ServiceActions.ReturnServicesFail);
            return {...state, returnServicesFail:payload};
        }
        case ServiceActions.POST_SERVICE_SUCCESS:
        {
            const {payload} = (action as ServiceActions.PostServiceSuccess);
            return {...state, service: payload, loading:false};
        }
        case ServiceActions.POST_SERVICE_FAIL:
        {
            const {payload} = (action as ServiceActions.PostServiceFail);
            return {...state, postServiceFail: payload, loading:false};
        }
        default:
            return state;
    }
}
