import * as LoginActions from '../actions';
import {Service} from '../../models/service';
import {Action} from '@ngrx/store';
import { Client } from '../../models/client';

export interface LoginState
{
    loading:boolean,
    client:Client,
    responseLogin:Response,
    responseLoginFail:boolean,
    responseGetDetails: string
}

const initialState: LoginState = {
    loading:true,
    responseLogin: new Response(),
    responseGetDetails:"",
    responseLoginFail: false,
    client: new Client(0,"","","","","","","",0,"",0,0,"",0)
};

export function loginReducer(state: LoginState = initialState, action:Action)
{
    switch(action.type) {
        case LoginActions.LOGIN_RESPONSE:
        {
            const {payload} = (action as LoginActions.LoginResponse);
            return {...state, responseLogin:payload,  loading:false};
        }
        case LoginActions.LOGIN_FAIL:
        {
            const {payload} = (action as LoginActions.LoginFail);
            return {...state, responseLoginFail:true, loading:false};
        }
        case LoginActions.RETURN_LOGGED:
        {
            const {payload} = (action as LoginActions.ReturnLogged);
            return {...state, client: payload, loading:false};
        }
        case LoginActions.RETURN_LOGGED_FAIL:
        {
            const {payload} = (action as LoginActions.ReturnLoggedFail);
            return {...state, responseGetDetails:payload, loading:false};
        }
        default:
            return state;
    }
}