
import { ActionReducerMap } from '@ngrx/store'
import { reviewsReducer } from './reviews.reducer';
import { ReviewsState } from './reviews.reducer';
import { eventsReducer, EventsState } from './events.reducer';
import { ServicesState, servicesReducer } from './services.reducer';
import { LoginState, loginReducer } from './login.reducer';
import { ReservationsState, reservationsReducer } from './reservations.reducer';
import { LandmarkState, landmarksReducer } from './landmarks.reducer';
import { ProfileState, profileReducer } from './profile.reducer';

export interface State {
   reviews : ReviewsState,
   events: EventsState,
   services: ServicesState, 
   logIn: LoginState,
   reservations: ReservationsState,
   landmarks: LandmarkState,
   profile: ProfileState
}

export const rootReducer: ActionReducerMap<State> = {
    reviews: reviewsReducer,
    events: eventsReducer,
    services: servicesReducer,
    logIn: loginReducer,
    reservations:reservationsReducer,
    landmarks: landmarksReducer,
    profile: profileReducer
}
