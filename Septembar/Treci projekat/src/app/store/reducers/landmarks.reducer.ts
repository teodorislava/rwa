import * as LandmarkActions from '../actions';
import {Landmark} from '../../models/landmarks';
import {Action} from '@ngrx/store';
import {EntityState, createEntityAdapter, EntityAdapter} from '@ngrx/entity';

export interface Landmarks extends EntityState<Landmark> {};
const adapter: EntityAdapter<Landmark> = createEntityAdapter<Landmark> ();


const initialLandmarks: Landmarks = {
    ids:[],
    entities: {
    }
}


export interface LandmarkState
{
    landmarks: Landmarks,
    responseAddVisit:string,
    responseReturnLandmarks:boolean
}

const initialState: LandmarkState = {
    landmarks:initialLandmarks,
    responseAddVisit:"",
    responseReturnLandmarks:false
}

export function landmarksReducer(state: LandmarkState = initialState, action:Action)
{
    switch(action.type) {
        case LandmarkActions.RETURN_LANDMARKS:
        {
            const {payload} = (action as LandmarkActions.ReturnLandmarks);
            const ad =  adapter.addAll(payload, state.landmarks);
            return Object.assign({...state, landmarks: ad});
        }
        case LandmarkActions.RETURN_LANDMARKS_FAIL:
        {
            const {payload} = (action as LandmarkActions.ReturnLandmarksFail);
            return {...state, responseReturnLandmarks:true};
        }
        case LandmarkActions.ADD_VISIT_SUCCESS:
        {
            const {payload} = (action as LandmarkActions.AddVisitSuccess);
            const ad =  adapter.updateOne({id:payload.id, changes:payload}, state.landmarks);
            return {...state, landmarks:ad};
        }
        case LandmarkActions.ADD_VISIT_FAIL:
        {
            const {payload} = (action as LandmarkActions.AddVisitFail);
            return {...state, responseAddVisit:payload};
        }
        default:
        {
            return state;
        }
    }
}

export const selectors = adapter.getSelectors();