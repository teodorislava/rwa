import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap, catchError} from 'rxjs/operators';

import * as LoginActions from '../actions';
import { LoginService } from '../../services/login.service';
import { of } from 'rxjs';

@Injectable()
export class GetLoggedEffect
{
    constructor(private actions:Actions, private loginService:LoginService) {}

    @Effect()
    getLogged$ = this.actions.ofType(LoginActions.GET_LOGGED)
        .map((action:LoginActions.GetLogged) => action.payload)
        .pipe(switchMap((el) =>
            {
                return this.loginService.getDetails(el.token, el.type)
                    .pipe(map( l => new LoginActions.ReturnLogged(l)),
                    catchError(er => of(new LoginActions.ReturnLoggedFail('Greska prilikom citanja detalja o klijentu!'))));
            }
        ));
}

@Injectable()
export class LoginEffects
{
    constructor(private actions:Actions, private loginService: LoginService) {}

    @Effect()
    createLog$ = this.actions.ofType(LoginActions.TRY_LOGIN)
        .map((action:LoginActions.TryLogin) => action.payload)
        .pipe(switchMap((el) => {
            return this.loginService.tryLogin(el)
            .pipe(map(el => new LoginActions.LoginResponse(el)),
                catchError(er => of(new LoginActions.LoginFail('Greska prilikom logovanja!'))));
        } ));
}