import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap, catchError} from 'rxjs/operators';

import * as ReservationsActions from '../actions';
import { ReservationsService } from '../../services/reservations.service';
import { of, forkJoin, pipe } from 'rxjs';
import { Room } from '../../models/room';

@Injectable()
export class GetReservationsEffect
{
    constructor(private actions:Actions, private rService:ReservationsService) {}

    @Effect()
    getReservations$ = this.actions.ofType(ReservationsActions.GET_RESERVATIONS)
        .pipe(switchMap(() =>
            {
                return this.rService.getReservations()
                    .pipe(map( r => new ReservationsActions.ReturnReservations(r)),
                    catchError(er => of(new ReservationsActions.ReturnReservationsFail('Greska prilikom citanja rezervacija!'))));
            }
        ));
}

@Injectable()
export class ReservationsEffect
{
    constructor(private actions:Actions, private rService: ReservationsService) {}

    @Effect()
    createReservation$ = this.actions.ofType(ReservationsActions.POST_RESERVATION)
        .map((action:ReservationsActions.PostReservation) => action.payload)
        .pipe(switchMap((el) => {
            return this.rService.createReservation(el)
            .pipe(map(el => new ReservationsActions.PostReservationSuccess(el.text.toString()),
            catchError(error => of(new ReservationsActions.PostReservationFail('Greska prilikom kreiranja rezervacije!')))))
        }));
}

@Injectable()
export class ClientEffect
{
    constructor(private actions:Actions, private rService: ReservationsService) {}

    @Effect()
    createClient$ = this.actions.ofType(ReservationsActions.POST_CLIENT)
        .map((action:ReservationsActions.PostClient) => action.payload)
        .pipe(switchMap((el) => {
            return this.rService.createClient(el)
            .pipe(map(el => new ReservationsActions.PostClientSuccess(el.text.toString())),
                catchError(error => of(new ReservationsActions.PostClientFail('Greska prilikom kreiranja klijenta!'))))
        }));
}

@Injectable()
export class GetRoomsEffect
{
    constructor(private actions:Actions, private rService:ReservationsService) {}

    @Effect()
    getRooms$ = this.actions.ofType(ReservationsActions.GET_ROOMS)
        .pipe(switchMap(() =>
            {
                return this.rService.getRooms()
                    .pipe(map( r => new ReservationsActions.ReturnRooms(r)),
                    catchError(er => of(new ReservationsActions.ReturnRoomsFail('Greska prilikom citanja soba!'))));
            }
        ));
}

@Injectable() 
export class GetAvailableRooms 
{
    constructor(private actions:Actions, private rService:ReservationsService) {}

    @Effect()
    getAvailableRooms$ = this.actions.ofType(ReservationsActions.GET_AVAILABLE_ROOMS)
        .map((action:ReservationsActions.getAvailableRooms) => action.payload)
        .pipe(switchMap((options: any) =>
            forkJoin([
                of(options),
                this.rService.getRooms()
            ])))
        .pipe(switchMap((data: any) => {
            const [options, rooms] = data;
            return this.rService.getReservations()
                .pipe(map(reservations => {
                    const filteredRooms = rooms.filter(room => (room.TYPE == options.reservation.TYPE && room.AC == options.reservation.AC
                    && room.BALCONY == options.reservation.BALCONY && room.NUMBER_OF_BEDS == options.beds));
                    const rooms_id = rooms.map(r => r.ID);
                    const ress = reservations.filter(el => (rooms_id.includes(el.ID_ROOM)));
                    const sr = new Date(options.reservation.START);
                    const er = new Date(options.reservation.FINISH);
                    const final = ress.filter(el => (!(sr <= new Date(el.START) && er <= new Date(el.START)) &&
                    !(er >= new Date(el.FINISH) && sr >= new Date(el.FINISH))));
                    const ok_res = final.map(el => el.ID_ROOM);
                    const available = filteredRooms.filter(el => !(ok_res.includes(el.ID)));
                    return new ReservationsActions.getAvailableRoomsSuccess(available)
                }))
        }));
}