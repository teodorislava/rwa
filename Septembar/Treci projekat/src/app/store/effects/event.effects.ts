import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import {map, switchMap} from 'rxjs/operators';
import {catchError} from 'rxjs/operators/catchError';
import * as EventActions from '../actions';
import { EventService } from '../../services/event.service';
import { of } from 'rxjs';

@Injectable()
export class GetEventsEffect
{
    constructor(private actions:Actions, private eventService:EventService) {}

    @Effect()
    getEvents$ = this.actions.ofType(EventActions.GET_EVENTS)
        .pipe(switchMap(() =>
            {
                return this.eventService.getEvents()
                    .pipe(map( ev => new EventActions.ReturnEvents(ev)),
                    catchError(er => of(new EventActions.ReturnEventsFail('Greska prilikom citanja dogadjaja!'))));
            }
        ));
}

@Injectable()
export class EventEffects
{
    constructor(private actions:Actions, private eventService: EventService) {}

    @Effect()
    createEvent$ = this.actions.ofType(EventActions.EVENT_SIGNUP)
        .map((action:EventActions.PostEvent) => action.payload)
        .pipe(switchMap((el) => {
            return this.eventService.createEvent(el)
            .pipe(
                map(el => new EventActions.PostEventSuccess(el)),
                catchError(error => of(new EventActions.PostEventFail('Greska prilikom prijavljivanja na dogadjaj!'))))
        }));
}