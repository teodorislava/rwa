import {GetReviewEffects, ReviewEffects} from './review.effects';
import { GetEventsEffect, EventEffects } from './event.effects';
import { GetServicesEffect, ServicesEffects, GetUsesEffects } from './services.effect';
import { LoginEffects, GetLoggedEffect } from './login.effects';
import { ReservationsEffect, GetReservationsEffect, GetRoomsEffect, ClientEffect, GetAvailableRooms } from './reservations.effects';
import { LandmarkEffects, DeleteInterestEffect, InterestEffect } from './landmarks.effects';
import { GetMyEventsEffects, GetMyServicesEffects, GetMyLandmarksEffects, GetMyReviewsEffects, UpdateProfileEffect } from './profile.effects';

export const effects : any[] = [GetReviewEffects, 
                                ReviewEffects, 
                                GetEventsEffect, 
                                EventEffects,
                                GetServicesEffect,
                                ServicesEffects,
                                LoginEffects,
                                GetLoggedEffect,
                                GetReservationsEffect,
                                ReservationsEffect,
                                GetRoomsEffect,
                                ClientEffect,
                                LandmarkEffects,
                                GetUsesEffects,
                                DeleteInterestEffect,
                                InterestEffect,
                                GetMyLandmarksEffects,
                                GetMyEventsEffects,
                                GetMyServicesEffects,
                                GetMyReviewsEffects,
                                UpdateProfileEffect,
                                GetAvailableRooms];
