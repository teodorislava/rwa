import { Injectable } from "@angular/core";
import { ProfileService } from "../../services/profile.service";
import { Effect, Actions } from "@ngrx/effects";
import { switchMap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import * as ProfileActions from '../actions';



@Injectable()
export class GetMyReviewsEffects
{
    constructor(private actions:Actions, private profileService:ProfileService) {}

    @Effect()
    getMyReviews$ = this.actions.ofType(ProfileActions.GET_MY_REVIEWS)
        .pipe(switchMap(() =>
            {
                return this.profileService.getMyReviews()
                    .pipe(map( rev => new ProfileActions.ReturnMyReviews(rev)),
                    catchError(er => of(new ProfileActions.ReturnMyReviewsFail('Greska prilikom citanja utisaka!'))));
        }
        ));
}

@Injectable()
export class GetMyEventsEffects
{
    constructor(private actions:Actions, private profileService:ProfileService) {}

    @Effect()
    getMyEvents$ = this.actions.ofType(ProfileActions.GET_MY_EVENTS)
        .pipe(switchMap(() =>
            {
                return this.profileService.getMyEvents()
                    .pipe(map(rev => new ProfileActions.ReturnMyEvents(rev)),
                    catchError(er => of(new ProfileActions.ReturnMyEventsFail('Greska prilikom citanja dogadjaja!'))));
        }
        ));
}

@Injectable()
export class GetMyLandmarksEffects
{
    constructor(private actions:Actions, private profileService:ProfileService) {}

    @Effect()
    getMyLandmarks$ = this.actions.ofType(ProfileActions.GET_MY_LANDMARKS)
        .pipe(switchMap(() =>
            {
                return this.profileService.getMyLandmarks()
                    .pipe(map(rev => new ProfileActions.ReturnMyLandmarks(rev)),
                    catchError(er => of(new ProfileActions.ReturnMyLandmarksFail('Greska prilikom citanja znamenitosti!'))));
        }
        ));
}

@Injectable()
export class GetMyServicesEffects
{
    constructor(private actions:Actions, private profileService:ProfileService) {}

    @Effect()
    getMyServices$ = this.actions.ofType(ProfileActions.GET_MY_SERVICES)
        .pipe(switchMap(() =>
            {
                return this.profileService.getMyServices()
                    .pipe(map(rev => new ProfileActions.ReturnMyServices(rev)),
                    catchError(er => of(new ProfileActions.ReturnMyServicesFail('Greska prilikom citanja usluga!'))));
        }
        ));
}

@Injectable()
export class UpdateProfileEffect
{
    constructor(private actions:Actions, private profileService:ProfileService) {}

    @Effect()
    updateProfile$ = this.actions.ofType(ProfileActions.UPDATE_PROFILE)
        .map((action:ProfileActions.UpdateProfile) => action.payload)
        .pipe(switchMap((el) =>
            {
                return this.profileService.updateProfile(el)
                    .pipe(map(rev => new ProfileActions.UpdateProfileSuccess(rev.text.toString())),
                    catchError(() => of(new ProfileActions.ReturnMyServicesFail('Greska prilikom azuriranja profila!'))));
        }
        ));
}