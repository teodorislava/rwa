import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap, catchError} from 'rxjs/operators';

import * as ServicesActions from '../actions';
import { HotelServicesService } from '../../services/hotel-services.service';
import { of } from 'rxjs';

@Injectable()
export class GetServicesEffect
{
    constructor(private actions:Actions, private hsService:HotelServicesService) {}

    @Effect()
    getServices$ = this.actions.ofType(ServicesActions.GET_SERVICES)
        .pipe(switchMap(() =>
            {
                return this.hsService.getServices()
                    .pipe(map( s => new ServicesActions.ReturnServices(s)),
                    catchError(er => of(new ServicesActions.ReturnServicesFail('Greska prilikom citanja servisa!'))))
            }
        ));
}
@Injectable()
export class ServicesEffects
{
    constructor(private actions:Actions, private hsService: HotelServicesService) {}

    @Effect()
    createService$ = this.actions.ofType(ServicesActions.POST_SERVICE)
        .map((action:ServicesActions.PostService) => action.payload)
        .pipe(switchMap((el) => {
            return this.hsService.createService(el)
            .pipe(map(el => new ServicesActions.PostServiceSuccess(el.text.toString())),
            catchError(error => of(new ServicesActions.PostServiceFail('Greska prilikom rezervacije usluge!'))))
        }));
}

@Injectable()
export class GetUsesEffects
{
    constructor(private actions:Actions, private hsService:HotelServicesService) {}

    @Effect()
    getUses$ = this.actions.ofType(ServicesActions.GET_SERVICES)
        .pipe(switchMap(() =>
            {
                return this.hsService.getUses()
                    .pipe(map( s => new ServicesActions.ReturnUses(s)),
                    catchError(er => of(new ServicesActions.ReturnUsesFail('Greska prilikom citanja rezervisanih usluga!'))));
            }
        ));
}