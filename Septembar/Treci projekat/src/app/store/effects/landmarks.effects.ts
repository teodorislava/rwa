import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap, catchError} from 'rxjs/operators';

import * as LandmarkActions from '../actions';
import { LandmarksService } from '../../services/landmarks.service';
import { of } from 'rxjs';


@Injectable()
export class LandmarkEffects
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    getLandmarks$ = this.actions.ofType(LandmarkActions.GET_LANDMARKS)
        .pipe(switchMap(() => {
            return this.lService.getLandmarks()
            .pipe(map(el => new LandmarkActions.ReturnLandmarks(el)),
            catchError(er => of(new LandmarkActions.ReturnLandmarksFail('Greska prilikom citanja znamenitosti!'))));
        } ));
}

@Injectable()
export class InterestEffect
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    createInterest$ = this.actions.ofType(LandmarkActions.ADD_VISIT)
        .map((action:LandmarkActions.AddVisit) => action.payload)
        .pipe(switchMap((el) => {
            return this.lService.addVisit(el)
            .pipe(map(el => new LandmarkActions.AddVisitSuccess(el)),
                catchError(error => of(new LandmarkActions.AddVisitFail('Greska prilikom dodavanja posete!'))));
        }));
}

@Injectable()
export class DeleteInterestEffect
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    deleteInterest$ = this.actions.ofType(LandmarkActions.DELETE_VISIT)
        .map((action:LandmarkActions.DeleteVisit) => action.payload)
        .pipe(switchMap((el) => {
            return this.lService.deleteVisit(el.id)
            .pipe(map(el => new LandmarkActions.DeleteVisitResponse(el)),
                catchError(error => of(new LandmarkActions.DeleteVisitFail('Greska prilikom brisanja posete'))));
        }));
}
