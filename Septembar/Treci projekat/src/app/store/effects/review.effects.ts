import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap, catchError} from 'rxjs/operators';
import * as ReviewActions from '../actions';
import { ReviewService } from '../../services/review.service';
import { of } from 'rxjs';


@Injectable()
export class ReviewEffects
{
    constructor(private actions:Actions, private reviewService: ReviewService) {}

    @Effect()
    createReview$ = this.actions.ofType(ReviewActions.POST_REVIEW)
        .map((action:ReviewActions.PostReview) => action.payload)
        .pipe(switchMap((el) => {
            return this.reviewService.createReview(el)
            .pipe(map(el=> new ReviewActions.PostReviewSuccess(el)),
            catchError(error => of(new ReviewActions.PostReviewFail('Greska prilikom kreiranja utiska!'))))
        } ));
}

@Injectable()
export class GetReviewEffects
{
    constructor(private actions:Actions, private reviewService:ReviewService) {}

    @Effect()
    getReviews$ = this.actions.ofType(ReviewActions.GET_REVIEWS)
        .pipe(switchMap(() =>
            {
                return this.reviewService.getReviews()
                    .pipe(map( rev => new ReviewActions.ReturnReviews(rev)),
                    catchError(er => of(new ReviewActions.ReturnReviewsFail('Greska prilikom citanja utiska!'))));
        }
        ));
}