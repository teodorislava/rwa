import {StoreModule} from '@ngrx/store';
import { BrowserModule } from '@angular/platform-browser';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {rootReducer} from './store/reducers';
import { StartpageComponent } from './components/startpage/startpage.component';
import 'hammerjs';
import { MyMaterialModule } from './material';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { ShowOnDirtyErrorStateMatcher, ErrorStateMatcher } from '@angular/material';
import { LoginComponent } from './components/login/login.component';
import { ReviewComponent } from './components/review/review.component';
import { EffectsModule } from '@ngrx/effects';
import { AddReviewComponent } from './components/add-review/add-review.component';
import { ReviewService } from './services/review.service';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';
import { effects } from './store/effects';
import { EventsComponent } from './components/events/events.component';
import {ModalModule} from 'ngx-bootstrap';
import { EventService } from './services/event.service';
import { AppRoutingModule } from './app-routing.module';
import { HotelServiceComponent } from './components/hotel-service/hotel-service.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { RoomServiceComponent } from './components/room-service/room-service.component';
import { ActivateServiceComponent } from './components/activate-service/activate-service.component';
import { RnrServiceComponent } from './components/rnr-service/rnr-service.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { ChildCareComponent } from './components/child-care/child-care.component';
import { HotelServicesService } from './services/hotel-services.service';
import { LoginService } from './services/login.service';
import { CookieService } from 'ngx-cookie-service';
import { ReservationsService } from './services/reservations.service';
import { MainComponent } from './components/main/main.component';
import { LandmarksService } from './services/landmarks.service';
import { LandmarksComponent } from './components/landmarks/landmarks.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AlertModule } from 'ngx-bootstrap';
import { ProfileService } from './services/profile.service';
import { TimepickerModule } from 'ngx-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import { GalleryComponent } from './components/gallery/gallery.component';
import { CarouselModule } from 'ngx-bootstrap';
import { RoomsComponent } from './components/rooms/rooms.component';

@NgModule({
  declarations: [
    AppComponent,
    StartpageComponent,
    HomeComponent,
    ReservationsComponent,
    LoginComponent,
    ReviewComponent,
    AddReviewComponent,
    EventsComponent,
    HotelServiceComponent,
    PageNotFoundComponent,
    RestaurantComponent,
    AboutUsComponent,
    RoomServiceComponent,
    ActivateServiceComponent,
    RnrServiceComponent,
    ChildCareComponent,
    MainComponent,
    LandmarksComponent,
    ProfileComponent,
    GalleryComponent,
    RoomsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    CarouselModule.forRoot(),
    TimepickerModule.forRoot(),
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument(),
    AppRoutingModule,
    ModalModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    AlertModule.forRoot()
  ],
  providers: [{provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}, ProfileService, LandmarksService, ReservationsService, CookieService, LoginService, HotelServicesService, EventService, ReviewService, {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}], //treba da se implementira kako da ne dolazi do gresaka prilikom mejl, telefon ...
  bootstrap: [AppComponent]
})
export class AppModule { }
