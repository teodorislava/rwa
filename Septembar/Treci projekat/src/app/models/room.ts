export class Room {
    constructor(
        public ID:number,
        public RESERVED:number,
        public PHOTO:string, 
        public PRICE:number,
        public WIFI:number, 
        public LAST_RENOVATED:string, 
        public TYPE:string, 
        public DESCRIPTION:string,
        public AC:number, 
        public BALCONY:number, 
        public TYPE_EXCELSIOR:string, 
        public NUMBER_OF_ROOMS:number, 
        public ID_HOTEL:number, 
        public ROOM_NUMBER:number,
        public NUMBER_OF_BEDS:number
    ) {}
}