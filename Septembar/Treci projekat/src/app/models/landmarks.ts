export class Landmark {
    constructor(
        public id:number,
        public LANDMARK:string, 
        public DESCRIPTION:string,
        public VISITS:number
    ) {}
}