export class Review
{
    constructor(username, utisak, vreme, tezina, koktel, tip, id)
    {
        this.username=username;
        this.utisak=utisak;
        this.vreme = vreme;
        this.tezina = tezina;
        this.koktel = koktel;
        this.tip = tip;
        this.id = id;
    }
}