export const LOGIN_REQUEST = "LOGIN_REQUEST"; 
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";

export const GET_COCKTAILS = "GET_COCKTAILS";
export const RETURN_COCKTAILS = "RETURN_COCKTAILS";

export const SHOW_STEP="SHOW_STEP";
export const SHOW_ALL_STEPS="SHOW_ALL_STEPS";

export const CLEAR_STATE="CLEAR_STATE";

export const CREATE_IMPRESSION="CREATE_IMPRESSION";
export const CREATE_IMPRESSION_SUCCESS="CREATE_IMPRESSION_SUCCESS";
export const CREATE_IMPRESSION_FAIL="CREATE_IMPRESSION_FAIL";
export const GET_REVIEWS="GET_REVIEWS";
export const RETURN_REVIEWS="RETURN_REVIEWS";

export const GET_USER="GET_USER";
export const RETURN_USER="RETURN_USER";
export const UPDATE_USER="UPDATE_USER";

export const GET_TITLE="GET_TITLE";
export const RETURN_TITLE="RETURN_TITLE";

export const DELETE_REVIEW="DELETE_REVIEW";
export const DELETE_REVIEW_SUCCESS="DELETE_REVIEW_SUCCESS";
export const DELETE_REVIEW_FAIL="DELETE_REVIEW_FAIL";

export const CHANGE_REVIEW="CHANGE_REVIEW";
export const CHANGE_REVIEW_SUCCESS="CHANGE_REVIEW_SUCCESS";
export const CHANGE_REVIEW_FAIL="CHANGE_REVIEW_FAIL";

export const REGISTER="REGISTER";
export const REGISTER_SUCCESS="REGISTER_SUCCESS";
export const REGISTER_FAIL="REGISTER_FAIL";

export const GET_ARTICLES="GET_ARTICLES";
export const RETURN_ARTICLES="RETURN_ARTICLES";
export const UPDATE_ARTICLE="UPDATE_ARTICLE";
export const UPDATE_ARTICLE_SUCCESS="UPDATE_ARTICLE_SUCCESS";
export const UPDATE_ARTICLE_FAIL="UPDATE_ARTICLE_FAIL";

export function loginRequest(username, password)
{
    return {
        type:LOGIN_REQUEST, 
        payload: {username:username, password:password}
    }
}
export function loginSuccess(id, name)
{
    return {
        type:LOGIN_SUCCESS,
        payload: {id: id, name: name}
    }
}
export function loginFail(msg)
{
    return {
        type:LOGIN_FAIL,
        payload: msg
    }
}

export function getCocktails(data)
{
    return {
        type: GET_COCKTAILS,
        payload: data
    }
}
export function returnCocktails(data)
{
    return {
        type: RETURN_COCKTAILS,
        payload: data
    }
}
export function showStep(step, index)
{
    return {
        type: SHOW_STEP,
        payload: {
            opis:step.opis,
            index:index
        }
    }
}
export function showAllSteps(steps)
{
    return {
        type: SHOW_ALL_STEPS,
        payload: steps
    }
}
export function clearState()
{
    return {
        type:CLEAR_STATE
    }
}
export function createImpression(content, time, user, cocktail)
{
    return {
        type: CREATE_IMPRESSION,
        payload: {content:content, time:time, user: user, cocktail:cocktail}
    }
}
export function createImpressionSuccess(data, message)
{   
    return {
        type:CREATE_IMPRESSION_SUCCESS,
        payload:{ review:data, message:message }
    }
}
export function createImpressionFail(data)
{
    return {
        type:CREATE_IMPRESSION_FAIL,
        payload:data
    }
}
export function getReviews(username, cocktail)
{
    return {
        type:GET_REVIEWS,
        payload: {username:username, cocktail:cocktail}
    }
}
export function returnReviews(reviews)
{
    return {
        type:RETURN_REVIEWS,
        payload: reviews
    }
}
export function getUser(id)
{
    return {
        type:GET_USER,
        payload:id
    }
}
export function returnUser(data)
{
    return {
        type:RETURN_USER,
        payload: data
    }
}
export function getTitle(data)
{
    return {
        type:GET_TITLE,
        payload:data
    }
}
export function returnTitle(data)
{
    return {
        type:RETURN_TITLE,
        payload:data
    }
}
export function deleteReview(id)
{
    return {
        type:DELETE_REVIEW,
        payload: id
    }
}
export function deleteReviewSuccess(data)
{
    return {
        type:DELETE_REVIEW_SUCCESS,
        payload:data
    }
}
export function deleteReviewFail()
{
    return {
        type:DELETE_REVIEW_FAIL
    }
}
export function changeReview(review)
{
    return {
        type:CHANGE_REVIEW,
        payload: review
    }
}
export function changeReviewSuccess(review)
{
    return {
        type:CHANGE_REVIEW_SUCCESS,
        payload: review 
    }
}
export function changeReviewFail()
{
    return {
        type:CHANGE_REVIEW_FAIL
    }
}

export function register(user)
{
    return {
        type:REGISTER,
        payload: user
    }
}
export function registerSuccess( data )
{
    return {
        type:REGISTER_SUCCESS,
        payload: data
    }
}
export function registerFail(data)
{
    return {
        type:REGISTER_FAIL,
        payload:data
    }
}
export function updateUser(id, data, old) {
    return {
        type:UPDATE_USER,
        payload: {id: id, user:data, oldPoints: old}
    }
}

export function getArticles() {
    return {
        type:GET_ARTICLES
    }
}
export function returnArticles(data) {
    return {
        type: RETURN_ARTICLES,
        payload: data
    }
}
export function updateArticle(article) {
    return {
        type: UPDATE_ARTICLE,
        payload: article
    }
}
export function updateArticleSuceess(data) {
    return {
        type: UPDATE_ARTICLE_SUCCESS,
        payload: data
    }
}
export function updateArticleFail(data) {
    return {
        type:UPDATE_ARTICLE_FAIL,
        payload: data
    }
}