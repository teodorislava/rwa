import {call, put, takeLatest} from 'redux-saga/effects';
import { REGISTER, registerSuccess, registerFail } from '../actions';
import { createUser, fetchData } from '../../services/login-service';

function* tryRegister(action)
{
    try {
        const users = yield call(fetchData);
        const u = users.filter(u => u.username === action.payload.username);
        if(u.length > 0) {
            alert('Korsničko ime postoji, pokušajte ponovo!');
            return;
        }
        const uu = users.filter(u => u.mail === action.payload.mail);
        if(uu.length > 0)
        {
            alert('Mejl već postoji, pokušajte ponovo!');
            return;
        }

        const data = yield call(createUser, action.payload);
        if(data.status === 201 || data.status === 200)
            yield put(registerSuccess('Registracija je uspešna, možete da se prijavite!'));
        else
            yield put(registerFail('Registracija je neuspešna, pokušajte kasnije!'));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaRegister()
{
    yield takeLatest(REGISTER, tryRegister);
}