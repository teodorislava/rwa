import {call, put, takeLatest} from 'redux-saga/effects';
import {GET_COCKTAILS, returnCocktails} from '../actions';
import { fetchAllCocktails } from '../../services/cocktail-service';


function* getCocktailsData(action)
{
    try {
        const data = yield call(fetchAllCocktails);
        const type = action.payload.slice(1, action.payload.length);
        const cocktails = data.filter(cocktail => cocktail.tip === type);
        yield put(returnCocktails(cocktails));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaCocktails()
{
    yield takeLatest(GET_COCKTAILS, getCocktailsData);
}