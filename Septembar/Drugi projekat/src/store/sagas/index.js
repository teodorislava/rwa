import { all, fork } from 'redux-saga/effects';
import { sagaLogin } from './login.saga';
import { sagaCocktails } from './cocktails.saga';
import { sagaReview, sagaGetReview, sagaGetTitle, sagaChangeReview, sagaDeleteReview } from './review.saga';
import { sagaGetUser, sagaUpdateUser } from './user.saga';
import { sagaRegister } from './register.saga';
import { sagaGetArticles, sagaUpdateArticle } from './articles.saga';

export default function* root() {
    yield all([
      fork(sagaLogin),
      fork(sagaCocktails),
      fork(sagaReview),
      fork(sagaGetReview),
      fork(sagaGetUser),
      fork(sagaGetTitle),
      fork(sagaChangeReview),
      fork(sagaDeleteReview),
      fork(sagaRegister),
      fork(sagaUpdateUser),
      fork(sagaGetArticles),
      fork(sagaUpdateArticle)
    ])
}