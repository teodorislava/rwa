import {call, put, takeLatest} from 'redux-saga/effects';
import {LOGIN_REQUEST, loginSuccess, loginFail} from '../actions';
import { fetchData } from '../../services/login-service';

function* tryLogin(action)
{
    try {
        const data = yield call(fetchData);
        console.log(data);
        const user = data.filter(user =>
             (action.payload.username===user.username && action.payload.password===user.password));
        console.log(action, user);
        if(user.length === 1)
            yield put(loginSuccess(user[0].id, user[0].username));
        else
            yield put(loginFail('Neuspešna prijava!'));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaLogin()
{
    yield takeLatest(LOGIN_REQUEST, tryLogin);
}