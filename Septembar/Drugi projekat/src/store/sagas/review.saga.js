import {call, put, takeLatest} from 'redux-saga/effects';
import { CREATE_IMPRESSION, createImpressionSuccess, createImpressionFail, GET_REVIEWS, returnReviews, GET_TITLE, returnTitle, CHANGE_REVIEW, changeReviewSuccess, changeReviewFail, DELETE_REVIEW, deleteReviewSuccess, deleteReviewFail } from '../actions';
import { putImpression, fetchImpressions, fetchTitles, updateImpression, removeImpression } from '../../services/review-service';
import { getUser } from '../../services/user-service';

function* createNewImpression(action)
{
    try {
        const data = yield call(putImpression, action.payload.content, action.payload.time,
                                action.payload.user, action.payload.cocktail);
        if(data.status===200 || data.status===201)
            yield put(createImpressionSuccess(data.review, 'Kreiranje utiska je uspešno!'));
        else
            yield put(createImpressionFail('Kreiranje utiska je neuspešno!'));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaReview()
{
    yield takeLatest(CREATE_IMPRESSION, createNewImpression);
}

function* getAllReviews(action)
{
    try {
        if(action.payload.username !== '') {
            const data = yield call(fetchImpressions);
            const result = data.filter(review => (action.payload.username === review.username));
            yield put(returnReviews(result));
        }
        else if(action.payload.cocktail !== '') {
            const data = yield call(fetchImpressions);
            yield put(returnReviews(data));
        }   
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaGetReview()
{
    yield takeLatest(GET_REVIEWS, getAllReviews);
}

function* getProfileTitle(action)
{
    try {
        const data = yield call(fetchTitles);
        const user = yield call(getUser, action.payload);
        const title = data.filter(el => (user.brojPoena >= el.poeniD && user.brojPoena<= el.poeniG));
        if(title.length === 0)
            yield put(returnTitle('Koktel šampion!'));
        else
            yield put(returnTitle(title[0].naziv));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaGetTitle()
{
    yield takeLatest(GET_TITLE, getProfileTitle);
}

function* changeYourReview(action)
{
    try {
        console.log(action);
        const data = yield call(updateImpression, action.payload);
        if(data.status===200 || data.status===201)
            yield put(changeReviewSuccess(action.payload));
        else
            yield put(changeReviewFail());
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaChangeReview()
{
    yield takeLatest(CHANGE_REVIEW, changeYourReview);
}

function* deleteYourReview(action)
{
    try {
        const data = yield call(removeImpression, action.payload);
        if(data.status===200 || data.status===201)
            yield put(deleteReviewSuccess(action.payload));
        else
            yield put(deleteReviewFail());
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaDeleteReview()
{
    yield takeLatest(DELETE_REVIEW, deleteYourReview);
}