import {call, put, takeLatest} from 'redux-saga/effects';
import { returnArticles, GET_ARTICLES, UPDATE_ARTICLE, updateArticleSuceess, updateArticleFail } from '../actions';
import { readArticles, putArticle } from '../../services/articles-service';


function* getAllArticles(action)
{
    try {
            const data = yield call(readArticles);
            yield put(returnArticles(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaGetArticles()
{
    yield takeLatest(GET_ARTICLES, getAllArticles);
}

function* updateArticleData(action)
{
    try {
        const data = yield call(putArticle, action.payload.id,
                                action.payload.naslov,
                                action.payload.sadrzaj,
                                action.payload.likes,
                                action.payload.dislikes,
                                action.payload.datum);

        if(data.status===200 || data.status===201)
            yield put(updateArticleSuceess(action.payload));
        else
            yield put(updateArticleFail('Neuspešna izmena članka!'));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaUpdateArticle()
{
    yield takeLatest(UPDATE_ARTICLE, updateArticleData);
}
