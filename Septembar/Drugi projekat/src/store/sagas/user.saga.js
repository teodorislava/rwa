import {call, put, takeLatest} from 'redux-saga/effects';
import { returnUser, GET_USER, UPDATE_USER, returnTitle } from '../actions';
import { getUser } from '../../services/user-service';
import { updateUser } from '../../services/user-service';
import { fetchTitles } from '../../services/review-service';

function* getUserData(action)
{
    try {
        const data = yield call(getUser, action.payload);
        yield put(returnUser(data));
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaGetUser()
{
    yield takeLatest(GET_USER, getUserData);
}

function* updateUserData(action) 
{
    try {
        const user = yield call(getUser, action.payload.id);
        user.brojPoena = user.brojPoena - action.payload.oldPoints + action.payload.user;
        const resp = yield call(updateUser, action.payload.id, user, action.payload.user);
        const titles = yield call(fetchTitles);
        const title = titles.filter(el => (user.brojPoena >= el.poeniD && user.brojPoena <= el.poeniG));
        if(resp.status === 200 || resp.status === 201)
        {
            if(title.length === 0)
                yield put(returnTitle('Koktel šampion!'));
            else
                yield put(returnTitle(title[0].naziv));
            yield put(returnUser(user));
        }
    }
    catch(e)
    {
        console.log(e);
    }
}
export function* sagaUpdateUser()
{
    yield takeLatest(UPDATE_USER, updateUserData);
}