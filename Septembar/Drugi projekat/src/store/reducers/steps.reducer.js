import * as actions from '../actions';

const initialState = {
    data:{},
    loading: true,
    index:-1
}
export default function (state = initialState, action) {
    switch (action.type) {

        case actions.SHOW_STEP:
        {
            return {...state, data: action.payload.opis, loading:false, index:action.payload.index};
        }
        case actions.SHOW_ALL_STEPS:
        {
            return {...state, data:action.payload, loading: false};
        }
        case actions.CLEAR_STATE:
        {
            return {data:{}, loading:true, index:-1};
        }
        default: 
        {
            return state;
        }
    }
}