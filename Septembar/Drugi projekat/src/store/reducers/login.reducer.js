import * as actions from '../actions';

const initialState = {
    id: -1, 
    name: '',
    message: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case action.LOGIN_REQUEST: 
        {
            return {...state, message:''};
        }
        case actions.LOGIN_SUCCESS:
        {
            return {id: action.payload.id, message:'Prijava je uspešna!', name:action.payload.name};
        }
        case actions.LOGIN_FAIL:
        {
            return {...state, message:'Prijava je neuspešna!'};
        }
        case actions.REGISTER: 
        {
            return {...state, message:''};
        }
        case actions.REGISTER_SUCCESS: 
        {
            return {...state, message:action.payload};
        }
        case actions.REGISTER_FAIL: 
        {
            return {...state, message:action.payload};
        }
        case actions.CLEAR_STATE: 
        {
            return {id:-1, name:'', message:''};
        }
        default: 
        {
            return state;
        }
    }
}