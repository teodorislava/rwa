import {combineReducers} from 'redux';
import cocktailsReducer from './cocktails.reducer';
import stepsReducer from './steps.reducer';
import loginReducer from './login.reducer';
import reviewReducer from './review.reducer';
import userReducer from './user.reducer';
import articlesReducer from './articles.reducer';

export default combineReducers({
    cocktails: cocktailsReducer,
    step: stepsReducer, 
    login: loginReducer,
    review: reviewReducer,
    user: userReducer,
    articles: articlesReducer
});