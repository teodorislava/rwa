import * as actions from '../actions';

export default function (state = { user: {} }, action) {
    switch (action.type) {

        case actions.RETURN_USER:
        {
            return { user: action.payload };
        }
        default: 
        {
            return state;
        }
    }
}