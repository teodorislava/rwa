import * as actions from '../actions';

const initialState = {
    data:[],
    loading: true
};

export default function (state = initialState, action) {
    switch (action.type) {

        case actions.RETURN_COCKTAILS:
        {
            return {...state, data: [...action.payload], loading:false};
        }
        case actions.CLEAR_STATE:
        {
            return {data: [], loading:false};
        }
        default: 
        {
            return state;
        }
    }
}