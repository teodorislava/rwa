import * as actions from '../actions';


const initialState = {
    message:'',
    articles: [],
    notifications: 0
}
export default function (state = initialState, action) {
    
    switch (action.type) {
        case actions.RETURN_ARTICLES:
        {
            return {...state, articles:action.payload };
        }
        case actions.UPDATE_ARTICLE_SUCCESS:
        {  
            let n = state.notifications;
            n++;
            const newState = state.articles.map( el => {
                if(action.payload.id === el.id)
                    return action.payload;
                else
                    return el;
            });
            return {...state, articles: newState, notifications: n };
        }
        case actions.UPDATE_ARTICLE_FAIL:
        {   
            return {...state, message: action.payload};
        }
        default: 
        {
            return state;
        }
    }
}