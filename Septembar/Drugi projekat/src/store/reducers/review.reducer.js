import * as actions from '../actions';


const initialState = {
    title: '',
    message: '',
    reviews:[],
    error: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case actions.CREATE_IMPRESSION_SUCCESS:
        {
            return {...state, reviews: [...state.reviews, action.payload.review], error:false};
        }
        case actions.CREATE_IMPRESSION_FAIL:
        {
            return {message:action.payload, reviews:[], error:true};
        }
        case actions.GET_REVIEWS:
        {
            return {message:'', reviews:[], error: false};
        }
        case actions.RETURN_REVIEWS:
        {
            return {...state, reviews:[...action.payload], error: false};
        }
        case actions.RETURN_TITLE:
        {
            return {...state, title:action.payload };
        }
        case actions.DELETE_REVIEW:
        {
            return {...state, message: '', error: false};
        }
        case actions.DELETE_REVIEW_SUCCESS:
        {
            const newReviews = state.reviews.filter(review => review.id !== action.payload);
            return {...state, reviews:newReviews, error: false};
        }
        case actions.DELETE_REVIEW_FAIL:
        {
            return {...state, message:'Utisak nije obrisan!', error:true};
        }
        case actions.CHANGE_REVIEW:
        {
            return {...state, message: '', error: false};
        }
        case actions.CHANGE_REVIEW_SUCCESS:
        {   
            const newState = state.reviews.map( el => {
                if(action.payload.id === el.id)
                    return action.payload;
                else
                    return el;
            });
            return {...state, reviews: newState, error:false};
        }
        case actions.CHANGE_REVIEW_FAIL:
        {   
            return {...state, message:'Utisak nije izmenjen!', error:true};
        }
        default: 
        {
            return state;
        }
    }
}