import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './store/sagas';
import reducer from './store/reducers';
import Start from './components/start';
import { Router, Route} from 'react-router-dom';
import {createBrowserHistory} from 'history';
import cocktailPage from './components/cocktailPage';
import ReviewPage from './components/reviewPage';
import Profile from './components/profile';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { CookiesProvider } from 'react-cookie';
import pink from '@material-ui/core/colors/pink';
import lime from '@material-ui/core/colors/lime';
import register from './components/register';
import startPage from './components/startPage';
import articles from './components/articles';

const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
  );
sagaMiddleware.run(rootSaga);
store.runSaga = sagaMiddleware.run;

export const theme = createMuiTheme({
    pallete:{
        primary: pink,
        secondary: lime,
    },
    typography: {
     "fontFamily": 'Popins',
     "fontSize":25,
     "fontWeightLight": 300,
     "fontWeightRegular": 400,
     "fontWeightMedium": 500
    }
});

ReactDOM.render((
    <MuiThemeProvider theme={theme}>
        <Provider store={store}>
            <CookiesProvider>
                <Router history={history}> 
                    <div>
                        <Route path="/" component = {startPage} />
                        <Route path="/start" component = {Start} />
                        <Route path="/bloodymary" component = {cocktailPage} />
                        <Route path="/martini" component = {cocktailPage} />
                        <Route path="/manhattan" component = {cocktailPage} />
                        <Route path="/daiquiri" component = {cocktailPage} />
                        <Route path="/vodka" component = {cocktailPage} />
                        <Route path="/bourbon" component = {cocktailPage} />
                        <Route path="/gin" component = {cocktailPage} />
                        <Route path="/shots" component = {cocktailPage} />
                        <Route path="/margarita" component = {cocktailPage} />
                        <Route path="/punch" component = {cocktailPage} />
                        <Route path="/champagne" component = {cocktailPage} />
                        <Route path="/tequila" component = {cocktailPage} />
                        <Route path="/review" component = {ReviewPage} />
                        <Route path="/profile" component = {Profile} meta={{ requiresAuth: true}}/>
                        <Route path="/register" component = {register} />
                        <Route path="/articles" component= {articles} />
                    </div>
                </Router>   
            </CookiesProvider>
        </Provider>
    </MuiThemeProvider>), document.getElementById('root'));
registerServiceWorker();

history.listen( location =>  {
    console.log(location);
});