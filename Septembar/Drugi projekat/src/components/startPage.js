import React, {Component} from 'react';
import { Well } from 'react-bootstrap';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';
import { Redirect } from 'react-router';
import Login from './login';
import Register from './register';
import MainPage from './mainPage';
import '../css/startPage.css';

class StartPage extends Component
{
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    constructor(props) {
        super(props);
        const {cookies} = this.props;
        this.user = cookies.get('name') || '';
        this.id = cookies.get('id') || -1;
        if(this.user!=='' && this.id!==-1)
            this.state = {login: false, register: false, open: false, logged: true, loggedOut:false};
        else
            this.state = {login: false, register: false, open: false, logged: false, loggedOut:false};
    }
    render()
    {
        if(this.state.logged) {
            return (
                <div>
                    <MainPage handleLogout={this.handleLogout} ></MainPage>
                </div>
            )
        }
        if(this.state.login) {
            return (
                <div>
                    {this.state.open? <Login handleClose={this.handleClose}></Login>: null}
                </div>
            )
        }
        if(this.state.register) {
            return (
                <div>
                    {this.state.open? <Register handleClose={this.handleClose}></Register>: null}
                </div>
            )
        }
        return (
            <div className="start-page-container">
                <Well>
                    {this.state.loggedOut? <Redirect to=''></Redirect> : null}
                    <h1> Dobrodošli u Mixology! </h1>
                    <button className="show-recipe-btn" onClick={() => this.handleClickOpen('login')} >Prijavi se</button>
                    <button className="show-recipe-btn" onClick={() => this.handleClickOpen('register')}>Registruj se</button>
                </Well>
            </div>
        )
    }
    handleClickOpen = (type) => {
        if(type === 'register')
            this.setState({open:true, register:true, loggedOut:false});
        else
            this.setState({open:true, login:true, loggedOut:false});
    }
    handleLogout = () => {
        this.setState({open:false,logged:false, login:false, register: false, loggedOut:true});
    }
    handleClose = (id, name, type) => {
        
        if(type === 'login') {
            const {cookies} = this.props;
            if(id!==-1 && name!== '')
            {
                cookies.set('id', id);
                cookies.set('name', name);
                this.setState({open:false, logged:true, loggedOut:false});
            }
            else
                this.setState({open: false, login: false, loggedOut:false});
        }   
        else  if(type === 'register') {
            this.setState({open:false, register: false, login: false, loggedOut:false})
        } 
    }
}
export default (withCookies(StartPage));