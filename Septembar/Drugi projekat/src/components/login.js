import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginRequest, clearState } from '../store/actions';
import '../css/login.css';

class Login extends Component
{
    constructor(props)
    {
        super(props);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword= this.handlePassword.bind(this);
        this.state = {username:'', password: ''};
    }
    render()
    {
        return (
            <div>
                <Dialog
                open={true}
                onClose={() => this.handleLogin()}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title"> Prijava </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    {this.props.login.message===''? 
                    'Da biste se prijavili unesite vaše korisničko ime i šifru.' :
                    this.props.login.message}
                    </DialogContentText>
                    { this.props.login.message===''?
                    <div>
                    <TextField
                    margin="dense"
                    value={this.state.username}
                    onChange={this.handleUsername}
                    id="name"
                    label="Korisničko ime"
                    type="text"
                    fullWidth
                    />
                    <TextField
                    margin="dense"
                    value={this.state.password}
                    onChange={this.handlePassword}
                    id="pass"
                    label="Šifra"
                    type="password"
                    fullWidth
                    />
                    </div> 
                    : null }
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.handleLogin()} color="primary">
                    Zatvori
                    </Button>
                    {this.props.login.message===''? 
                    <Button onClick={() => this.props.tryLogin(this.state.username,this.state.password)} color="primary">
                    Prijavi se
                    </Button>: null }
                </DialogActions>
                </Dialog>
            </div>
        )
    }
    handleLogin()
    {
        this.props.clearState();
        this.props.handleClose(this.props.login.id, this.props.login.name, 'login');
    }
    handleUsername(event)
    {
        this.setState({username:event.target.value});
    }
    handlePassword(event)
    {
        this.setState({password:event.target.value}); 
    }
}
function mapStateToProps(state)
{
    return {
        login: state.login
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({tryLogin: loginRequest, clearState: clearState}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);