import React, {Component} from 'react';
import {Well} from 'react-bootstrap';
import '../css/cocktailRecipe.css';
import {Observable} from "rxjs";
import "rxjs/add/observable/interval";
import {take} from 'rxjs/operators';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showStep } from '../store/actions';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Button, Glyphicon, ButtonGroup, ButtonToolbar, OverlayTrigger, Tooltip} from 'react-bootstrap';

class Recipe extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {stop:false};
    }
    componentDidMount()
    {
        this.dataSub =  Observable.interval(3000)
        .pipe(take(this.props.recipe.length))
        .subscribe(val => this.props.showStep(this.props.recipe[val], val));
    }
    componentWillUnmount()
    {
        if(this.dataSub !== undefined)
            this.dataSub.unsubscribe();
    }
    render()
    {
            const tooltipLeft = (
                <Tooltip id="tooltipLeft">
                <strong> Vratite se korak unazad! </strong>
                </Tooltip>
            );
            const tooltipStop = (
                <Tooltip id="tooltipStop">
                    {this.state.stop?  <strong> Nastavite pripremu! </strong> : <strong> Zaustavite pripremu! </strong> }
                </Tooltip>
            );
            const tooltipRight = (
                <Tooltip id="tooltipRight">
                <strong> Pređite na sledeći korak! </strong>
                </Tooltip>
            );
            if(this.props.step.loading)
                return (
                    <div>
                        <h2 className="recipe-title">Priprema</h2>
                    <Well>
                        <h2 className="step-title">Pripremite sve neophodne sastojke!</h2>
                        <LinearProgress color="secondary" />
                    </Well>
                    </div>
                );
            return(
                <div> 
                    <h2 className="recipe-title">Priprema</h2>
                    <Well>
                        <h2 className="step-title"> {this.props.step.data} </h2>
                        <ButtonToolbar className="buttons-group">
                            <ButtonGroup bsSize="large">
                                {this.props.step.index > 0 ?
                                    (<OverlayTrigger placement="bottom" overlay={tooltipLeft}>
                                        <Button id="left" 
                                            onClick={() => this.countdownReturn(this.props.step.index)}
                                        > 
                                            <Glyphicon glyph="arrow-left" /> 
                                        </Button>
                                    </OverlayTrigger>) : null
                                }                          
                                <OverlayTrigger placement="bottom" overlay={tooltipStop}>
                                    <Button id="stop" onClick={() => this.countdownStop(this.props.step.index)}>
                                        <Glyphicon glyph="stop" />
                                    </Button>
                                </OverlayTrigger>

                                {this.props.step.index !== (this.props.recipe.length-1)? 
                                    (
                                        <OverlayTrigger placement="bottom" overlay={tooltipRight}>
                                            <Button id="right" onClick={() => this.countdownNext(this.props.step.index)}> 
                                                <Glyphicon glyph="arrow-right" />
                                            </Button>
                                        </OverlayTrigger>
                                    ) : null
                                }  
                            </ButtonGroup>
                        </ButtonToolbar>
                    </Well>
                </div>
            )
    }
    countdownReturn(index)
    {
        const ind = index - 1;
        this.dataSub.unsubscribe();
        this.props.showStep(this.props.recipe[ind], ind);
        let steps = this.props.recipe.filter((el, ind) => (ind >= index));

        this.dataSub = Observable.interval(3000)
        .pipe(take(steps.length))
        .subscribe(val => 
            {
                val = val + index;
                this.props.showStep(this.props.recipe[val], val);
            }
        );
    }
    countdownNext(index)
    {
        const indeks = index + 1;
        this.dataSub.unsubscribe();
        this.props.showStep(this.props.recipe[indeks], indeks);
        let steps = this.props.recipe.filter((el, ind) => (ind > indeks));
        this.dataSub = Observable.interval(3000)
        .pipe(take(steps.length))
        .subscribe(val => 
            {
                val = val+indeks+1;
                this.props.showStep(this.props.recipe[val], val);
            }
        );
    }
    countdownStop(index)
    {
        if(index === (this.props.recipe.length-1))
            return;
        const stopped = this.state.stop;
        if(stopped)
        {
            index++;
            this.setState({stop:false});
            this.props.showStep(this.props.recipe[index], index);
            let steps = this.props.recipe.filter((el, ind) => (ind > index));

            this.dataSub = Observable.interval(3000)
            .pipe(take(steps.length))
            .subscribe(val => 
                { 
                    val = val + index;
                    this.props.showStep(this.props.recipe[val], val);
                }
            );
        }
        else
        {
            this.dataSub.unsubscribe();
            this.setState({stop:true});
        }
    }
}

function mapStateToProps(state)
{
    return {
        step: state.step
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({ showStep:showStep }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Recipe);