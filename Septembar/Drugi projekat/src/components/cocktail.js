import React, {Component} from 'react';
import {Jumbotron, Row, Col, Image } from 'react-bootstrap';
import '../css/cocktail.css';
import CocktailRecipe from '../components/cocktailRecipe';
import CocktailSteps from './cocktailSteps';

class Cocktail extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {showRecipe:false, showFullRecipe:false, recipe:null};
        this.grades = [1,2,3,4,5];
    }
    render()
    {
        return (
            <div>
                <Jumbotron>
                    <Row className="show-grid">
                        <Col xs={12} md={8}>
                        <h1>{this.props.cocktail.naziv}</h1>
                        <h3>Tip čase u kojoj je preporučeno služiti koktel: {this.props.cocktail.tip_case} </h3>
                        <h3>Sastojci: </h3>
                        <ul>
                            {this.props.cocktail.sastojci.map((el, index) => {
                                return (
                                    <li key={index} className="sastojak"> <span className="ingredient-name"><i>Ime sastojka: </i> </span>
                                    <b>{el.ime}</b> <span className="ingredient-quantity"><i>Količina:</i></span> <b>{el.kolicina}</b> </li>
                                )
                            })}
                        </ul>
                        <h3>Vreme potrebno za pripremu koktela: {this.props.cocktail.vreme_pripreme} min.</h3>
                        <h3>Broj koraka za pripremu koktela: {this.props.cocktail.broj_koraka}</h3>
                        <h3>Popularnost koktela: {this.props.cocktail.rejting} </h3>
                        <h3> Možete odabrati pripremu ili prikaz celog recepta! </h3>
                        <p className="buttons">
                            <button className="show-recipe-btn" onClick={() => this.showRecipe(this.props.cocktail.koraci)}> Započni pripremu! </button>
                            <button className="show-recipe-btn" onClick={() => this.showFullRecipe(this.props.cocktail.koraci)}> Prikaži recept! </button>
                        </p>
                        </Col>
                        <Col xs={6} md={4}>
                        <Image circle className="cocktailImg" src={this.props.cocktail.img} alt={this.props.cocktail.naziv}/> 
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col>
                        {this.state.showRecipe? <CocktailRecipe recipe = {this.state.recipe}></CocktailRecipe>: null}
                        </Col>
                    </Row>
                    <Row className="show-grid">
                        <Col>
                        {this.state.showFullRecipe? <CocktailSteps recipe = {this.state.recipe}></CocktailSteps>: null}
                        </Col>
                    </Row>
                </Jumbotron>
            </div>
        )
    }
    showRecipe(recipe)
    {
        this.props.clear();
        this.setState({showRecipe:true, showFullRecipe:false, recipe:recipe});
    }
    showFullRecipe(recipe)
    {
        this.props.clear();
        this.setState({showFullRecipe:true, showRecipe: false, recipe:recipe});
    }
}
export default Cocktail;