import React, {Component} from 'react';
import '../css/reviewPage.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { Tab, Col, NavItem, Row, Nav, Thumbnail, Button, Alert, Panel, PanelGroup, Well } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'redux';
import { getCocktails, createImpression, getReviews, getUser, getTitle, changeReview, deleteReview, updateUser} from '../store/actions';
import {debounceTime, switchMap} from 'rxjs/operators';
import {fromEvent} from 'rxjs/observable/fromEvent';
import Review from './review';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';
import { returnCocktails } from '../services/cocktail-service';

const drawerWidth = 500;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: 'red'
  },
  drawerPaper: {
    position: 'relative',
    float:'right',
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    fontSize:16,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0, 
  },
  search: {
    fontColor:'red',
  },
  toolbar: theme.mixins.toolbar,
});

class ReviewPage extends Component
{
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props)
    {
        super(props);
        this.state = {cocktail:'', mode:'', review:null, reviewContent:'', reviewTime:'', results:[], content:'', open:false, reviewCocktail:null};
    }
    componentDidMount()
    {
        this.handleSearch();
        const {cookies} = this.props;
        let user = cookies.get('name');
        let id = cookies.get('id');
        if(user && id)
        {
            this.props.getReviews(user, '');
            this.props.getUser(id);
            this.props.getTitle(id);
        }
    }
    handleChangeReview(review)
    {
        review.utisak = this.state.reviewContent;
        review.vreme = this.state.reviewTime;
        this.handleClickOpen(review, 'izmena');
    }
    handleDeleteReview(review)
    {
        const {cookies} = this.props;
        let id = cookies.get('id');
        let points = this.countPoints(review.vreme, review.tezina);
        this.props.updateUserPoints(id, 0, points);
        this.props.deleteReview(review.id);
    }
    handleClickOpen = (data, mode) => {
        if(mode === 'novi')
        {
            const rev = {utisak:'', vreme:''};
            this.setState({ open: true, reviewCocktail:data, review:rev, mode:mode});
        }
        else
        {
            this.setState({ open: true, review:data, mode:mode});
        }
    };
    handleClose = (content, time, data, mode) => {
        console.log(content, time, data, mode);
        let points = 0;
        const {cookies} = this.props;
        let user = cookies.get('name');
        let id = cookies.get('id');
        if(content==='' && time===0 && data===null)
        {
            this.setState({open:false});
            return;
        }
        if(mode==='novi')
        {
            points = this.countPoints(time, data.tezina);
            this.props.submitReview(content, Number(time), user, data);
            this.props.updateUserPoints(id, points, 0);
            this.setState({ open: false, mode:'novi'});
        }
        else
        {
            let oldPoints = this.countPoints(time, data.tezina);
            points = this.countPoints(data.vreme, data.tezina);
            this.props.updateUserPoints(id, points, oldPoints);
            this.props.changeReview(data);
            this.setState({ open: false, mode:'izmena', reviewContent: data.utisak, reviewTime: data.vreme });
        }
    }; 
    countPoints(time, tezina) {
        switch(time)
        {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5: 
            {
                if(tezina==='lako')
                    return 8;
                else if(tezina==='srednje')
                    return 9;
                else 
                    return 10;
            }
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:  
            {
                if(tezina==='lako')
                    return 6;
                else if(tezina==='srednje')
                    return 7;
                else 
                    return 8;
            }
            case 11:
            case 12:
            case 13:
            case 14:
            case 15: 
            {
                if(tezina==='lako')
                    return 4;
                else if(tezina==='srednje')
                    return 5;
                else 
                    return 6;
            }
            default: {
                if(tezina==='lako')
                    return 1;
                else if(tezina==='srednje')
                    return 2;
                else 
                    return 3;
            }
        }
    }
    render()
    {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <AppBar position="absolute" className={classes.appBar}>
                    <Toolbar>
                    <Typography variant="title" color="inherit" noWrap>
                        Ocenite koktele
                    </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    classes={{
                    paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.toolbar}/>
                        <h2> Vaš profil: </h2>
                        <p> <i>Ime:</i> &nbsp;&nbsp; {this.props.user.user.ime}</p>
                        <p> <i>Prezime:</i> &nbsp;&nbsp; {this.props.user.user.prezime}</p>
                        <p> <i>Godine:</i> &nbsp;&nbsp; {this.props.user.user.godine}</p>
                        <p> <i>Mail:</i> &nbsp;&nbsp; {this.props.user.user.mail}</p>
                        <p> <i>Poeni:</i> &nbsp;&nbsp; {this.props.user.user.brojPoena}</p>
                        <p> <i>Titula:</i> &nbsp;&nbsp; {this.props.review.title}</p>
                        <Divider />
                        <h2>Ocene:</h2>
                        { this.props.review.error ?
                        <Alert>
                        <strong> {this.props.review.message} </strong>
                        </Alert> : null}
                        <PanelGroup accordion id="accordion-example">
                        {this.props.review.reviews.map( (el, index) => {
                            return (
                                <Panel eventKey={index}>
                                    <Panel.Heading onClick={() => this.openReview(el)}>
                                        <Panel.Title toggle> {el.koktel} </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <Well>Sadržaj: &nbsp;&nbsp; {el.utisak} </Well>
                                        <Well>Vreme pripreme: &nbsp;&nbsp; {el.vreme} </Well>
                                        <Well>
                                            <Button onClick={() => this.handleChangeReview(el)}>Izmenite</Button> &nbsp;&nbsp;
                                            <Button onClick={() => this.handleDeleteReview(el)}>Obrišite</Button>
                                        </Well>
                                    </Panel.Body>
                                </Panel>
                            )
                        })}
                        </PanelGroup>
                    <Divider />
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.toolbar} />
                    <Typography noWrap>{'Unesite naziv koktela koji želite da ocenite.'}</Typography>
                    <div class="search">
                        <span class="fa fa-search"></span>
                        <input id="searchCocktails" type="search" placeholder="Pretraži"/>
                    </div>
                    <Tab.Container id="left-tabs-example" defaultActiveKey={0}>
                    <Row className="clearfix">
                    <Col sm={4}>
                    {this.state.results.map((res, index) => {
                        return (
                            <Nav bsStyle="pills" stacked>
                                <NavItem eventKey={index}>{res.naziv}</NavItem>
                            </Nav>
                    )})}
                    </Col>
                    <Col sm={4}>
                    {this.state.results.map((res, index) => {
                        return (
                            <Tab.Content animation>
                                <Tab.Pane eventKey={index}>
                                <Thumbnail src={res.img} alt={res.naziv}>
                                    <h3>{res.naziv}</h3>
                                    <p>Ocena: {res.rejting}</p>
                                    <p>
                                        <Button bsStyle="default" onClick={() => this.handleClickOpen(res, 'novi')}>
                                            Ocenite 
                                        </Button>&nbsp;&nbsp;&nbsp;
                                    </p>
                                </Thumbnail>
                                </Tab.Pane>
                            </Tab.Content>
                    )})}           
                    </Col>
                    </Row>
                    </Tab.Container>
                </main>
                <div>
                    { this.state.open ? <Review mode={this.state.mode} review={this.state.review} cocktail={this.state.reviewCocktail} handleClose={this.handleClose}></Review> : null }
                </div>
            </div>
        );
    }
    openReview(review)
    {
        this.setState({reviewContent:review.utisak, reviewTime: review.vreme});
    }
    handleDismiss()
    {
        this.setState({open:false});
    }
    handleSearch()
    {
        let search = document.getElementById('searchCocktails');

        fromEvent(search, "input")
        .pipe(debounceTime(500))
        .pipe(switchMap(el => returnCocktails()))
        .subscribe(result => {
            if(search.value !== this.state.content)
                    this.setState({results:[]});
            let text = search.value.toLowerCase();
            result.forEach(el => {
                    let name = el.naziv.toLowerCase();
                    if(search.value!=="" && name.indexOf(text)!==-1)
                    {
                        let pom = this.state.results;
                        pom.push(el);
                        this.setState({results: pom, content: search.value});
                    }
            })
        })
    }
}
function mapStateToProps(state)
{
    return {
        cocktails: state.cocktails,
        review: state.review, 
        user: state.user
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({fetchCocktails:getCocktails,
                               submitReview:createImpression,
                                getReviews: getReviews,
                                getUser:getUser,
                                getTitle:getTitle,
                                changeReview:changeReview,
                                deleteReview:deleteReview,
                                updateUserPoints: updateUser}, dispatch);
}
export default compose(connect(mapStateToProps, mapDispatchToProps), withStyles(styles))(withCookies(ReviewPage));

ReviewPage.propTypes = {
    classes: PropTypes.object.isRequired,
}; 