import React, {Component} from 'react';
import { instanceOf } from 'prop-types';
import {Navbar, Nav, MenuItem, NavItem, NavDropdown, PageHeader} from 'react-bootstrap';
import '../css/mainPage.css';
import {Link} from 'react-router-dom';
import { withCookies, Cookies } from 'react-cookie';

class MainPage extends Component 
{
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    render()
    {
        return(
            <div id="pocetna">
                <PageHeader class="mixology-title">
                    Mixology
                </PageHeader>
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                    <Navbar.Brand>
                        <Link to="/start"> Početna </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={2}>
                        <MenuItem><Link className="grade" to='/articles'> Članci </Link></MenuItem>
                        </NavItem>
                        <NavDropdown eventKey={3} title="Kokteli" id="basic-nav-dropdown">
                        <MenuItem><Link to='/bloodymary'>Bloody Mary </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/martini'> Martini </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/manhattan'> Manhattan </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/daiquiri'> Daiquiri </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/vodka'> Vodka </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/bourbon'> Bourbon </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/gin'> Gin </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/shots'> Shots </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/margarita'> Margarita </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/punch'> Punch </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/champagne'> Champagne </Link> </MenuItem>
                        <MenuItem divider />
                        <MenuItem><Link to='/tequila'> Tequila </Link> </MenuItem>
                        </NavDropdown>
                        <NavItem eventKey={4}>
                        <Link className="grade" to="/review"> Utisci </Link>
                        </NavItem>
                    </Nav>
                    <Nav pullRight>
                        <NavItem eventKey={1}>
                            <Link className="grade" to="/profile"> Profil </Link>
                        </NavItem>
                        <NavItem eventKey={2} onClick={() => this.logOut()}>
                            Odjavi se
                        </NavItem>
                    </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
    logOut()
    {
        const {cookies} = this.props;
        cookies.remove('name');
        cookies.remove('id');
        this.props.handleLogout();     
    }
} 
export default (withCookies(MainPage));