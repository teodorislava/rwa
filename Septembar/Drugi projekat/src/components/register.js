import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Alert } from 'react-bootstrap';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import '../css/register.css';
import { TextField } from '@material-ui/core';
import { register, clearState } from '../store/actions';

class Register extends Component
{
    constructor(props) {
        super(props);
        this.handleName = this.handleName.bind(this);
        this.handleLastName = this.handleLastName.bind(this);
        this.handleAge = this.handleAge.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword= this.handlePassword.bind(this);
        this.handlePasswordCheck = this.handlePasswordCheck.bind(this);
        this.state = {
            error: false,
            message: '',
            name:'', 
            lastName:'', 
            age:'', 
            username:'', 
            password:'', 
            passwordCheck:'', 
            email:''
        };
    }
    render()
    {
        return (
            <Dialog
                open={true}
                onClose={() => this.handleRegistration()}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title"> Registracija </DialogTitle>
                <DialogContent>
                    {this.state.error? 
                    <Alert>
                        <strong>Obaveštenje!</strong> {this.state.message}
                    </Alert>:null}
                    {this.props.registration.message === ''?
                        <form>
                            <p class="input-field"><TextField
                                id="formControlsText"
                                type="text"
                                onChange={this.handleName}
                                value={this.state.name}
                                label="Ime"
                                placeholder="Unesite ime"
                            /></p>
                            <p class="input-field">
                            <TextField
                                id="formControlsText"
                                type="text"
                                onChange={this.handleLastName}
                                value={this.state.lastName}
                                label="Prezime"
                                placeholder="Unesite prezime"
                            /></p>
                            <p class="input-field">
                            <TextField
                                id="formControlsText"
                                type="text"
                                onChange={this.handleAge}
                                value={this.state.age}
                                label="Godine"
                                placeholder="Unesite godine"
                            /></p>
                            <p class="input-field">
                            <TextField
                                id="formControlsText"
                                type="text"
                                onChange={this.handleUsername}
                                value={this.state.username}
                                label="Korisničko ime"
                                placeholder="Unesite korisničko ime"
                            /></p>
                            <p class="input-field">
                            <TextField
                                id="formControlsEmail"
                                type="email"
                                onChange={this.handleEmail}
                                value={this.state.email}
                                label="Mejl"
                                placeholder="Unesite mejl"
                            /></p>
                            <p class="input-field">
                            <TextField
                                id="formControlsText"
                                type="password"
                                onChange={this.handlePassword}
                                value={this.state.password}
                                label="Šifra"
                                placeholder="Unesite šifru"
                            /></p>
                            <p class="input-field">
                            <TextField
                                id="formControlsText"
                                type="password"
                                onChange={this.handlePasswordCheck}
                                value={this.state.passwordCheck}
                                label="Šifra"
                                placeholder="Ponovite šifru"
                            /></p>
                    </form>: <Alert><strong>Obaveštenje! </strong> {this.props.registration.message} </Alert> }
                </DialogContent>
                <DialogActions>
                    <button className="show-recipe-btn" onClick={() => this.handleRegistration()}>Zatvori</button>
                    {this.props.registration.message === ''? <button onClick={() => this.register()} className="show-recipe-btn">
                     Registruj se </button> : null}
                </DialogActions>
                </Dialog>
        )
    }
    register() {
        if(this.state.name === '' || this.state.lastName === '' || this.state.age === '' || this.state.email === ''
            || this.state.password === '' || this.state.passwordCheck === '' || this.state.email === '') {
                this.setState({error:true, message: 'Popunite sva polja!'});
                return;
            }
        if(this.state.password !== this.state.passwordCheck) {
            this.setState({error:true, message: 'Šifra mora da se slaže!'});
            return;
        }
        if(Number(this.state.age) < 18) {
            this.setState({error:true, message:'Morate biti punoletni da biste pristupili našem sajtu!'});
            return;
        }
        if(this.state.email.indexOf('@') === -1) {
            this.setState({error:true, message: 'Mejl mora biti u ispravnom formatu! Primer: a@b.com'});
            return;
        }
        this.setState({error:false});

        this.props.register({
            username: this.state.username,
            password: this.state.password,
            ime: this.state.name,
            prezime: this.state.lastName,
            godine: Number(this.state.age),
            mail: this.state.email
        })
    }
    handleUsername(event)
    {
        this.setState({username:event.target.value});
    }
    handlePassword(event)
    {
        this.setState({password:event.target.value}); 
    }
    handleEmail(event) 
    {
        this.setState({email: event.target.value});
    }
    handleName(event) {
        this.setState({name:event.target.value});
    }
    handleLastName(event) {
        this.setState({lastName:event.target.value});
    }
    handleAge(event) {
        this.setState({age:event.target.value});
    }
    handlePasswordCheck(event) {
        this.setState({passwordCheck:event.target.value});
    }
    handleRegistration() {
        this.props.clearState();
        this.props.handleClose(-1, '', 'register');
    }
}
function mapStateToProps(state)
{
    return {
        registration: state.login
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({register: register, clearState:clearState}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);