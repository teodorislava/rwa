import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Well, Glyphicon, Badge } from 'react-bootstrap';
import { getArticles, updateArticle } from '../store/actions';
import '../css/articles.css';

class Articles extends Component
{
    componentDidMount() {
        this.props.getArticles();
    }
    render()
    {
        return (
            <div>
                <h1 className="reviews-title"> Članci
                    {this.props.articles.notifications!== 0? 
                                <Badge pullRight>{this.props.articles.notifications}</Badge>: null}
                </h1>
                {this.props.articles.articles.map(el => {
                    return (
                        <Well>
                            <h3>{el.naslov} </h3>
                            <p>{el.sadrzaj}</p>
                            <p>{el.datum}</p>
                            <p>Lajkovi: {el.likes}</p>
                            <p>Dislajkovi: {el.dislikes}</p>
                            <button className="show-recipe-btn" onClick={() => this.update(el, 'like')}>
                                <Glyphicon glyph="thumbs-up" />
                            </button>
                            <button className="show-recipe-btn" onClick={() => this.update(el, 'dislike')}>
                                <Glyphicon glyph="thumbs-down" />
                            </button>
                        </Well>
                    )
                })}}
            </div>
        )
    }
    update(article, type) {
        if(type === 'like')
            article.likes++;
        else if(type === 'dislike')
            article.dislikes++;
        this.props.updateArticle(article);
    }
}
function mapStateToProps(state)
{
    return {
        articles: state.articles
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getArticles: getArticles, updateArticle:updateArticle}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Articles);