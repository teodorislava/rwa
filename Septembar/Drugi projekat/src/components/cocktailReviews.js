import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Well, Row, Jumbotron } from 'react-bootstrap';
import { getArticles, updateArticle } from '../store/actions';
import '../css/articles.css';

class CocktailReviews extends Component
{
    componentDidMount() {

    }
    render()
    {
        return (
            <Jumbotron>
                <Row>
                    <h1 className="reviews-title">Šta su ostali rekli o ovom koktelu..</h1>
                    {this.props.reviews.map( rev => {
                        if(rev.koktel === this.props.cocktail.naziv)
                            return (
                                <Well>
                                    <p>Utisak: {rev.utisak}</p>
                                    <p>Vreme pripreme: {rev.vreme} minuta</p>
                                    <p>Autor:  {rev.username}</p>
                                </Well>
                            )
                    })}
                </Row>
            </Jumbotron>
        )
    }
}
function mapStateToProps(state)
{
    return {
        articles: state.articles
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getArticles: getArticles, updateArticle:updateArticle}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(CocktailReviews);