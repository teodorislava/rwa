import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Alert} from 'react-bootstrap';

class Review extends Component
{
    constructor(props)
    {
        super(props);
        this.handleContent = this.handleContent.bind(this);
        this.handleTime = this.handleTime.bind(this);
        this.state = {content:'', time:'', error:false};
    }
    componentDidMount()
    {
        this.setState({content:this.props.review.utisak, time:this.props.review.vreme});
    }
    render()
    {
        return (
            <div>
                <Dialog
                open={true}
                onClose={() => this.handleEnd()}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title"> Utisak </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    {this.state.error? 
                    <Alert>
                    <strong>Upozorenje!</strong> Sadržaj koji ste uneli je nevalidan.
                    </Alert> : null}
                    Pošaljite nam Vaša iskustva sa pripremom ovog koktela. Unesite Vaš komentar i vreme koje Vam je
                    bilo potrebno da pripremite koktel i pogledajte da li se Vaša titula povećala.
                    </DialogContentText>
                    <div>
                        <TextField
                        margin="dense"
                        multiline
                        value={this.state.content}
                        onChange={this.handleContent}
                        id="name"
                        label="Utisak"
                        type="text"
                        fullWidth
                        />
                        <TextField
                        margin="dense"
                        value={this.state.time}
                        onChange={this.handleTime}
                        id="name"
                        label="Vreme pripreme"
                        type="text"
                        fullWidth
                        />
                    </div> 
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.handleEnd('close')} color="default">
                    Zatvori
                    </Button>
                    <Button onClick={() => this.handleEnd('submit')} color="default">
                    {this.props.mode==='novi'? 'Pošalji': 'Izmeni'}
                    </Button>
                </DialogActions>
                </Dialog>
            </div>
        )
    }
    handleEnd(type)
    {
        if(type==='close')
            this.props.handleClose('', 0, null, this.props.mode);
        else
        {
            if(this.state.content!=='' && this.state.time!=='')
            {
                if(! Number(this.state.time) > 0)
                    this.setState({error:true});
                if(this.props.mode==='novi')
                {
                    console.log(this.state.content, Number(this.state.time), this.props.cocktail, this.props.mode);
                    this.props.handleClose(this.state.content, Number(this.state.time), this.props.cocktail, this.props.mode);
                }
                else
                {
                    console.log(this.props.review);
                    const time = this.props.review.vreme;
                    const rev = this.props.review;
                    rev.utisak = this.state.content;
                    rev.vreme = Number(this.state.time);
                    console.log(rev);
                    this.props.handleClose('', time, rev, this.props.mode);
                }
            }
                
            else
                this.setState({error:true});
        }
    }
    handleContent(event)
    {
        this.setState({content:event.target.value, error:false});
    }
    handleTime(event)
    {
        this.setState({time:event.target.value, error:false});
    }
}
export default Review;