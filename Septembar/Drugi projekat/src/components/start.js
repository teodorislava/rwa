import React, {Component} from 'react';
import {Grid, Row, Col, Image} from 'react-bootstrap';
import '../css/start.css';

class Start extends Component 
{
    render()
    {
        return(
            <div id="pocetna">
                <Grid>
                    <Row className="show-grid">
                        <Col xs={6} sm={3}>
                            <Image className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2018/05/03111734/9-Great-Cheap-Bottles-That-Bartenders-Swear-By-720x720-article.jpg" circle /> 
                         </Col>
                        <Col xs={6} sm={3}>
                            <Image className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2018/07/05134700/6-Frozen-Cocktails-to-Drink-in-Bars-Now-720x720-article.jpg" circle />
                        </Col>
                        <Col xsHidden sm={3}>
                            <Image className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2017/07/13090635/teal-quila-sunrise-720x720-recipe.jpg" circle />
                        </Col>
                        <Col xsHidden sm={3}>
                            <Image className="mainImg" src="https://cdn.liquor.com/wp-content/uploads/2018/07/05074911/snap-shot-rock-bar-in-bali-720x720-article.jpg" circle />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
} 
export default Start; 