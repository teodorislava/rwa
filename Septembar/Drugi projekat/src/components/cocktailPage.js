import React, {Component} from 'react';
import '../css/mainPage.css';
import { getCocktails, clearState, getReviews } from '../store/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Image, Tooltip, OverlayTrigger } from 'react-bootstrap';
import Cocktail from './cocktail';
import LinearProgress from '@material-ui/core/LinearProgress';
import CocktailReviews from './cocktailReviews';
import '../css/cocktailPage.css';

class CocktailPage extends Component 
{
    constructor(props)
    {
        super(props);
        this.state = {showCocktail: false, cocktail:null};
    }
    componentDidMount()
    {
        this.props.getCocktails(this.props.location.pathname);
        this.props.getReviews('', this.props.location.pathname);
    }
    componentWillUnmount()
    {
        this.props.clear();
    }
    render()
    {
        const tooltip = (
            <Tooltip id="tooltip">
              <strong>Kliknite za detalje!</strong>
            </Tooltip>
        );

        if(!this.props.cocktails.loading)
        {
            if(this.state.showCocktail)
                return( 
                    <div>
                        <Cocktail clear={this.props.clear} cocktail={this.state.cocktail}></Cocktail>
                        <CocktailReviews cocktail={this.state.cocktail} reviews={this.props.reviews.reviews}></CocktailReviews>
                    </div>
                );

            return(
                <div> 
                    <div className="cocktail-container">
                    {this.props.cocktails.data.map( (el, index) => {
                        return (
                            <div className="single-cocktail-container" key={index} onClick={() => this.showCocktail(el)}>
                                <div className="cocktail-info-container">
                                    <p className="cocktail-title"> 
                                        <i>{el.naziv}</i> 
                                    </p> 
                                    <div className="image-container">
                                    <OverlayTrigger placement="bottom" overlay={tooltip}>
                                        <Image className="cocktail-image" src={el.img} circle />
                                    </OverlayTrigger>
                                    </div>                          
                                </div>
                            </div>
                        )
                    })}
                    </div>
                </div>)
        }
        return (
            <div> 
                <LinearProgress color="secondary" />
            </div>
        )    
    }
    showCocktail(cocktail)
    {
        this.setState({showCocktail:true, cocktail: cocktail});
    }
}
function mapStateToProps(state)
{
    return {
        cocktails: state.cocktails,
        reviews: state.review
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getCocktails:getCocktails, clear:clearState, getReviews: getReviews}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(CocktailPage);