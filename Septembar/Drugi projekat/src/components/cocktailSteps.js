import React, {Component} from 'react';
import {Well} from 'react-bootstrap';
import '../css/cocktailRecipe.css';

class CocktailSteps extends Component
{
    render()
    {
        return(
            <div>
                <h2 class="recipe-title">Recept</h2>
                {this.props.recipe.map((el, index) => {
                    return (
                        <Well key={index}>  <h2> {index+1}. {el.opis} </h2> </Well>
                    )
                })}
            </div>
        )
    }
}
export default CocktailSteps;