import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUser, getTitle} from '../store/actions';
import { Well } from 'react-bootstrap';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';
import '../css/profile.css';

class Profile extends Component
{
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    componentDidMount()
    {
        const {cookies} = this.props;
        const id = cookies.get('id');
        this.props.getUser(id);
        this.props.getTitle(id);
    }
    render()
    {
        return (
            <Well className="profile-container">
                <h2 className="profile-title"> Vaš profil: </h2>
                    <p> <i>Ime:</i> &nbsp;&nbsp; {this.props.user.user.ime}</p>
                    <p> <i>Prezime:</i> &nbsp;&nbsp; {this.props.user.user.prezime}</p>
                    <p> <i>Godine:</i> &nbsp;&nbsp; {this.props.user.user.godine}</p>
                    <p> <i>Mail:</i> &nbsp;&nbsp; {this.props.user.user.mail}</p>
                    <p> <i>Poeni:</i> &nbsp;&nbsp; {this.props.user.user.brojPoena}</p>
                    <p> <i>Titula:</i> &nbsp;&nbsp; {this.props.title}</p>
            </Well>
        )
    }
}
function mapStateToProps(state)
{
    return {
        user: state.user,
        title: state.review.title
    }
}
function mapDispatchToProps(dispatch)  
{
    return bindActionCreators({getUser:getUser, getTitle:getTitle}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Profile));