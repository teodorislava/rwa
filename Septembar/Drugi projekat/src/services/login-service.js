export const fetchData =  async () => {
    try{
        const response = await fetch('http://localhost:4000/korisnici');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}

export const createUser = async (user) => {
    try {
        const response = await fetch('http://localhost:4000/korisnici',  {method: 'post',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                username:user.username,
                password:user.password,
                ime:user.ime, 
                prezime:user.prezime,
                godine:user.godine,
                mail:user.mail,
                brojPoena: 0
            })});
        return response;
    }
    catch(e)
    {
        console.log(e);
    }
}

