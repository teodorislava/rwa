export const putArticle = async (id, title, content, likes, dislikes, date) => {
    try {
        const response = await fetch('http://localhost:4000/clanci/'+id,  {method: 'put',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({sadrzaj:content,naslov:title, likes: likes, dislikes:dislikes, datum:date})});
        return response;
    }
    catch(e)
    {
        console.log(e);
    }
}

export const readArticles = async () => {
    try{
        const response = await fetch('http://localhost:4000/clanci');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}  