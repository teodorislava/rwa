export const getUser = async (id) => {
    try {
        const response = await fetch('http://localhost:4000/korisnici/'+id);
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}

export const updateUser = async (id, user) => {
    try {
        const response = await fetch('http://localhost:4000/korisnici/'+id, 
            {
            method: 'put',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                    username:user.username,
                    password:user.password,
                    ime:user.ime, 
                    prezime:user.prezime,
                    godine:user.godine,
                    mail:user.mail,
                    brojPoena:user.brojPoena})
        });
        return response;
    }
    catch(e) {

    }
}