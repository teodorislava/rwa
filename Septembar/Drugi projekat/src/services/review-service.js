export const fetchImpressions =  async () => {
    try
    {
        const response = await fetch('http://localhost:4000/utisci');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}
export const putImpression = async (content, time, user, koktel) => {
    try
    {
        const response = await fetch('http://localhost:4000/utisci', {
            method: 'post',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({tezina:koktel.tezina, username:user,utisak:content, vreme:time, koktel:koktel.naziv, tip:koktel.tip})
        });
        const data = await response.json();
        return {review: data, status: response.status};
    }
    catch(e)
    {

    }
}

export const updateImpression = async (impr) => {
    try
    {
        const response = await fetch(`http://localhost:4000/utisci/${impr.id}`, {
            method: 'put',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            },
            body:JSON.stringify({username:impr.username,
                                utisak:impr.utisak,
                                vreme:impr.vreme,
                                koktel:impr.koktel,
                                tezina:impr.tezina,
                                tip:impr.tip})
        });
        console.log(response);
        return response;
    }
    catch(e)
    {

    }
}
export const fetchTitles = async () => {
    try
    {
        const response = await fetch('http://localhost:4000/titule');
        const data = await response.json();
        return data;
    }
    catch(e)
    {

    }
}
export const removeImpression = async (id) => {
    try
    {
        const response = await fetch(`http://localhost:4000/utisci/${id}`, {
            method: 'delete',
            headers: {
            'Accept': 'application/json,  text/plain',
            'Content-Type': 'application/json'
            }
        });
        return response;
    }
    catch(e)
    {

    }
}

export const readImpressions = async () => {
    try {
        const response = await fetch('http://localhost:4000/utisci');
        const data = await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }
}