import * as Rxjs from 'rxjs';
import { IfObservable } from 'rxjs/observable/IfObservable';
import {Trening} from './klase.js';
import {Masaza} from './klase.js';
import { dugmeSpaFitnes, dugmeRew } from './prikaz.js';

export class DatabaseService {
  constructor() {}
  static prikazMasaza(od, doo, maser)
  {
    let splOd=od.split("-");
    let splDo=doo.split("-");
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let lab=document.createElement("label");
    lab.innerHTML="Pronadjene su ove rezervacije: ";
    div.appendChild(lab);
    const url="http://localhost:3000/masaze";
    const obs=Rxjs.Observable.fromPromise(fetch(url).then(response=>
    {
        response.json().then(el=> 
        {
          el.forEach(el=>{
            let m=new Masaza(el.id, el.tip, el.trajanje, el.maser_ime,
            el.klijent_ime, el.klijent_prezime,
            el.dan, el.mesec, el.godina, el.vreme, el.cena);
            if(m.godina>=splOd[0] && m.mesec>=splOd[1] && m.dan>=splOd[2] && m.godina<=splDo[0] && m.mesec<=splDo[1] && m.dan<=splDo[2] && m.maser_ime===maser)
            {
              m.prikaz();
            }
          });
        });
    }));
    
  }
  static prikazTreninga(od, doo, trener)
  {
    let splOd=od.split("-");
    let splDo=doo.split("-");
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let lab=document.createElement("label");
    lab.innerHTML="Pronadjene su ove rezervacije: ";
    div.appendChild(lab);
    const url="http://localhost:3000/treninzi";
    const obs=Rxjs.Observable.fromPromise(fetch(url).then(response=>
    {
        response.json().then(el=> 
        {
            el.forEach(el=>{
            let m=new Trening(el.id, el.tip, el.trajanje, el.trener_ime,
            el.klijent_ime, el.klijent_prezime,
            el.dan, el.mesec, el.godina, el.vreme, el.cena);
            if(m.godina>=splOd[0] && m.mesec>=splOd[1] && m.dan>=splOd[2] && m.godina<=splDo[0] && m.mesec<=splDo[1] && m.dan<=splDo[2] && m.trener_ime===trener)
            {
              m.prikaz();
            }
          });
        });
    }));
  }
  static get(tip)
  {
    if(tip==="masaza")
    {
      return fetch('http://localhost:3000/masaze').then( response =>
          response.json());
    }
    else 
      return fetch('http://localhost:3000/treninzi').then( response =>
      response.json());
  }
  static getQuote(num)
  {
    fetch('http://localhost:3000/citati').then( response =>
      response.json().then(el=> {

        let citat = document.getElementById("citat");
        citat.innerHTML=el[num].tekst;

      }));
  }
  static getExcercises(grupe, trajanje)
  {
    fetch('http://localhost:3000/vezbe').then( response =>
      response.json().then(el=> {
        let duration = 0;
        let i=0;
        let div=document.getElementById("dodatak");
        div.innerHTML="";
        while(duration < trajanje && i<5)
        {
          grupe.forEach(gr => {  
              let m=el[gr];
              console.log(m); 
              let p=document.createElement("p");
              p.className="vezbe";
              p.innerHTML="Vezba: "+m[i].naziv+", Trajanje: "+m[i].trajanje+"min, Broj ponavljaja: "+m[i].ponavljaji+"puta \n";
              duration=duration+m[i].trajanje;
              div.appendChild(p);
          });
          i++;
        }
      }));
  }
  static submitReview(ime, prezime, tekst, zaposleni)
  {     
    fetch('http://localhost:3000/review', {
            method: 'post',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({zaposleni:zaposleni, klijent_ime:ime,klijent_prezime:prezime,tekst:tekst})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Ocena dodata!";
                }
                else
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Neuspeh prilikom dodavanja ocene!!";
                }
                dugmeRew();
        });
  }
  static getEmployees()
  {
    return Rxjs.Observable.fromPromise(fetch('http://localhost:3000/zaposleni').then( response =>
        response.json()));
  }
}
