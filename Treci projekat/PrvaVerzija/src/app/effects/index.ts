import {GetReviewEffects, ReviewEffects} from './review.effects';
import { GetEventsEffect, EventEffects } from './event.effects';
import { GetServicesEffect, ServicesEffects } from './services.effect';
import { LoginEffects, GetLoggedEffect } from './login.effects';

export const effects : any[] = [GetReviewEffects, 
                                ReviewEffects, 
                                GetEventsEffect, 
                                EventEffects,
                                GetServicesEffect,
                                ServicesEffects,
                                LoginEffects,
                                GetLoggedEffect];
