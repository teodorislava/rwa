import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';

import * as LoginActions from '../store/actions';
import { LoginService } from '../services/login.service';

@Injectable()
export class GetLoggedEffect
{
    constructor(private actions:Actions, private loginService:LoginService) {}

    @Effect()
    getLogged$ = this.actions.ofType(LoginActions.GET_LOGGED)
        .pipe(switchMap(() =>
            {
                return this.loginService.getDetails()
                    .pipe(map( l => new LoginActions.ReturnLogged(l)))
            }
        ));
}

@Injectable()
export class LoginEffects
{
    constructor(private actions:Actions, private loginService: LoginService) {}

    @Effect()
    createLog$ = this.actions.ofType(LoginActions.TRY_LOGIN)
        .map((action:LoginActions.TryLogin) => action.payload)
        .pipe(switchMap((el) => {
            return this.loginService.tryLogin(el)
            .pipe(map(el => new LoginActions.LoginResponse(el)));
        } ));
}