import { Component, OnInit } from '@angular/core';
import {Review} from '../../models/review';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import { PostReview, GetReviews } from '../../store/actions';
import {State} from '../../store';
import { ReviewsState } from '../../store/reducers/reviews.reducer';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  public reviewList$ : Observable<Review[]>;
  public loading$ : Observable<boolean>;
  public addReview:boolean = false;

  constructor(private store$: Store<State>) {
    
   }

  ngOnInit() {
    this.reviewList$ = this.store$.select(state => state.reviews).map(el => el.reviews);
    this.loading$ = this.store$.select(state => state.reviews).map(el => el.loading);
    this.store$.dispatch(new GetReviews());
  }

  showAddReview()
  {
    this.addReview=true;
  }

  onReviewAdded(review)
  {
    this.store$.dispatch(new PostReview(review));
    this.store$.dispatch(new GetReviews());
  }

}
