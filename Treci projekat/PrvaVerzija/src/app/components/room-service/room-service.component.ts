import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Service } from '../../models/service';
import {Uses} from '../../models/uses';

@Component({
  selector: 'app-room-service',
  templateUrl: './room-service.component.html',
  styleUrls: ['./room-service.component.css']
})
export class RoomServiceComponent implements OnInit {

  @Output()
  chose:EventEmitter<Uses>= new EventEmitter<Uses>();
  
  uses:Uses = new Uses(0,1,0,0,new Date(), "", "", "", new Date(), false);

  constructor() { }

  ngOnInit() {
  }

  onSelect()
  {
    this.chose.emit(this.uses);
  }
}
//datum je los za jedan dan manji activate service sredi sutra poy