import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { TryLogin, GetLogged } from '../../store/actions';
import { Observable } from 'rxjs/Observable';
import { timeInterval } from 'rxjs/operators';
import { Client } from '../../models/client';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;
  response$:Observable<Response>;
  client$:Observable<Client>;
  
  constructor(private cookieService: CookieService,public store$:Store<State>) { }

  ngOnInit() {
    this.store$.select((state:State) => state.logIn);
    this.response$=this.store$.select((state:State) => state.logIn).map(el => el.response);
    this.client$=this.store$.select((state:State) => state.logIn).map(el => el.client);
  }

  onLogin()
  {
    this.store$.dispatch(new TryLogin({USERNAME:this.username, PASSWORD:this.password}));
    this.response$.subscribe(res => {
      if(res!=undefined)
      {
        console.log(this.response$);
        this.cookieService.set('User', res.text.toString());
      }
    });
  }

  onUsername(user)
  {
    this.username=user.target.value;
  }

  onPassword(pass)
  {
    this.password=pass.target.value;
  }

  onClick()
  {
    this.store$.dispatch(new GetLogged());
    let cookie = this.cookieService.get('User');
    console.log(cookie);
  }
}
