import * as ServiceActions from '../actions';
import {Service} from '../../models/service';
import {Action} from '@ngrx/store';

export interface ServicesState
{
    loading:boolean,
    service:Service[],
    response:Response
}

export function servicesReducer(state: ServicesState, action:Action)
{
    switch(action.type) {
        case ServiceActions.RETURN_SERVICES:
        {
            const {payload} = (action as ServiceActions.ReturnServices);
            return {...state, service:payload,  loading:false};
        }
        case ServiceActions.POST_SERVICE_SUCCESS:
        {
            const {payload} = (action as ServiceActions.PostServiceSuccess);
            return {...state, response: payload, loading:false};
        }
        default:
            return {...state, loading:true};
    }
}