import * as ReviewActions from '../actions';
import {Review} from '../../models/review';
import { Observable } from 'rxjs/Observable';
export type Action = ReviewActions.ReturnReviews;

export interface ReviewsState {
    loading: boolean,
    reviews: Review[]
}
export function reviewsReducer(state: ReviewsState, action:Action)
{
    switch(action.type) {
        case ReviewActions.RETURN_REVIEWS:
            return {...state, reviews: action.payload, loading:false};
        default:
            return {...state, loading:true};
    }
}