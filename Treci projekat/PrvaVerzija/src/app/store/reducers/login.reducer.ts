import * as LoginActions from '../actions';
import {Service} from '../../models/service';
import {Action} from '@ngrx/store';
import { Client } from '../../models/client';

export interface LoginState
{
    loading:boolean,
    client:Client,
    response:Response
}

export function loginReducer(state: LoginState, action:Action)
{
    switch(action.type) {
        case LoginActions.LOGIN_RESPONSE:
        {
            const {payload} = (action as LoginActions.LoginResponse);
            return {...state, response:payload,  loading:false};
        }
        case LoginActions.RETURN_LOGGED:
        {
            const {payload} = (action as LoginActions.ReturnLogged);
            return {...state, client: payload, loading:false};
        }
        default:
            return {...state, loading:true};
    }
}