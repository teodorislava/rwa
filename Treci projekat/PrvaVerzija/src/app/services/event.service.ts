import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Event } from '../models/event';
import { HttpClient } from '@angular/common/http';
import { Attends } from '../models/attends';

@Injectable()
export class EventService {

  constructor(private http:HttpClient) { }

  getEvents(): Observable<Event[]>
  {
    let url='http://localhost:8000/api/event';
    return this.http.get<Event[]>(url);
  }

  createEvent(payload:Attends) 
  {
    let url='http://localhost:8000/api/attends';
    return this.http.post<Response>(url, payload);
  }
}
