import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Review } from '../models/review';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class ReviewService {

  constructor(private http:HttpClient) { }

  createReview(payload:Review) 
  {
    let url='http://localhost:8000/api/review';
    return this.http.post<Response>(url, payload);
  }

  getReviews(): Observable<Review[]>
  {
    let url='http://localhost:8000/api/review';
    return this.http.get<Review[]>(url);
  }
}
