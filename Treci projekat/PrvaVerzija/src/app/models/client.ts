export class Client
{
    constructor(
        public ID:number,
        public NAME:string, 
        public LAST_NAME:string,
        public EMAIL:string,
        public JMBG:string,
        public PASSPORT_NUMBER:string, 
        public COUNTRY:string, 
        public PHONE_NUMBER:string, 
        public PAID:number, 
        public ADDRESS:string, 
        public ID_RESERVATION:number, 
        public REGISTERED:number,
        public USERNAME:string,
        public BILL:number
    ) {}
}