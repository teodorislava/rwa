export class Reservation
{
    constructor(
        public NAME: string,
        public LAST_NAME: string,
        public EMAIL:string, 
        public JMBG:string,
        public PASSPORT_NUMBER:string,
        public COUNTRY:string,
        public PHONE_NUMBER:string,
        public ADRESS : string,
        public TYPE: string,
        public WIFI: number,
        public AC:number,
        public BALCONY:number,
        public START:Date,
        public FINISH:Date
    ) {}
} 