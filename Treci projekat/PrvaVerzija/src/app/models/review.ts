
export class Review
{
    constructor(
        public TEXT: string,
        public RATING: number,
        public ID_CLIENT:number
    ) {}
}