import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Event} from '../../models/event';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { GetEvents, PostEvent } from '../../store/actions';
import { Attends } from '../../models/attends';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  public eventList$:Observable<Event[]>;
  
  constructor(private store$:Store<State>) { }

  ngOnInit() {
    this.eventList$ = this.store$.select((state:State) => state.events).map(ev => ev.events);
    console.log(this.eventList$);
    this.store$.dispatch(new GetEvents());
  }

  onEventParticipate(id:number, klijent:number)
  {
    console.log(id, klijent);
    this.store$.dispatch(new PostEvent(new Attends(klijent, id)));
  }
}
