import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RnrServiceComponent } from './rnr-service.component';

describe('RnrServiceComponent', () => {
  let component: RnrServiceComponent;
  let fixture: ComponentFixture<RnrServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RnrServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RnrServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
