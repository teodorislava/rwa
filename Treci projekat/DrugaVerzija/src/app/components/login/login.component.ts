import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { TryLogin, GetLogged } from '../../store/actions';
import { Observable } from 'rxjs/Observable';
import { Client } from '../../models/client';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../../models/get-details';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;
  response$:Observable<Response>;
  client$:Observable<Client>;
  
  constructor(private cookieService: CookieService,public store$:Store<State>) { }

  ngOnInit() {
    this.store$.select((state:State) => state.logIn);
    this.response$=this.store$.select((state:State) => state.logIn).map(el => el.response);
    this.client$=this.store$.select((state:State) => state.logIn).map(el => el.client);
  }

  onLogin()
  {
    this.store$.dispatch(new TryLogin({USERNAME:this.username, PASSWORD:this.password}));
    this.response$.subscribe(res => {
        this.cookieService.set('token', res.text.toString());
        this.cookieService.set('type', 'client');
    });
  }

  onUsername(user)
  {
    this.username=user.target.value;
  }

  onPassword(pass)
  {
    this.password=pass.target.value;
  }

  onClick()
  {
    const getlogin = new GetDetails(this.cookieService.get('token'), this.cookieService.get('type'));
    this.store$.dispatch(new GetLogged(getlogin));
  }
}
