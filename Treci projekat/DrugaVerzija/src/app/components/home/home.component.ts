import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public Prikaz:boolean = false;
  public Tekst:string;

  
  constructor() {
    this.Tekst="Hotel Excelsior Lux nalazi se u samom srcu grada Nisa. Hotel trenutno radi sa 100 smeštajnih jedinica, dok je za kraj 2018. godine predviđeno povećanje kapaciteta na 170 soba i apartmana. Moderan hotelski restoran nudi jela domaće i internacionalne kuhinje, kao i poslastice za sve sladokusce. Tri moderno opremljene, multifunkcionalne konferencijske sale su na raspolaganju gostima iz zemlje i inostranstva, za organizaciju sastanaka, konferencija, seminara, prezentacija, intervjua. U prizemlju hotela nalazi se fitnes & spa centar, kao i vrtic za decu. Ukoliko postanete gost naseg hotela, sve ove usluge bice dostupne i Vama!"; }

  ngOnInit() {

  }

  onClickHome()
  {
    if(this.Prikaz==true)
      this.Prikaz=false;
    else
      this.Prikaz=true;
  }
  onClickClear()
  {
    this.Prikaz=false;
  }

}
