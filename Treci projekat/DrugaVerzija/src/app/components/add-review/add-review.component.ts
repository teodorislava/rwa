import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Review} from '../../models/review';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit {

  review:Review = new Review("", 0, 1);
  
  @Output()
  add: EventEmitter<Review> = new EventEmitter<Review>();

  constructor() { }

  ngOnInit() {
  }

  onAddReview()
  {
    this.add.emit(this.review);
  }

  generator()
  {
    return [1,2,3,4,5];
  }

  rating(i:number)
  {
    console.log(i);
    this.review.RATING=i;
  }
}
