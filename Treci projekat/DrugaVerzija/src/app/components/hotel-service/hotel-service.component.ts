import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { Service } from '../../models/service';
import { Observable } from 'rxjs/Observable';
import { GetServices, PostService } from '../../store/actions';
import { Uses } from '../../models/uses';
import { map } from 'rxjs/operators';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-hotel-service',
  templateUrl: './hotel-service.component.html',
  styleUrls: ['./hotel-service.component.css']
})
export class HotelServiceComponent implements OnInit {

  modalRef:BsModalRef;
  view:boolean = false;
  services$:Observable<Service[]>;
  response$:Observable<Response>;
  subs:any;

  constructor(public modalService: BsModalService, public store$: Store<State>) { }

  ngOnInit() {
    this.services$=this.store$.select((state:State) => state.services).map(el => el.service);
    this.response$=this.store$.select((state:State) => state.services).map(el => el.response);
  }

  openModal(template: TemplateRef<any>) {
    console.log(template);
    this.modalRef = this.modalService.show(template, { class:'modal-lg', animated: true });
  }

  openModalSmall(template: TemplateRef<any>)
  { 
    this.modalRef = this.modalService.show(template, { class:'modal-sm', animated: true });
  }

  onMeal(message, b, l, d)
  {  
    this.modalRef.hide();
    if(message==="dorucak")
      this.openModalSmall(b);
    else if(message==="rucak")
      this.openModalSmall(l);
    else if(message==="vecera")
      this.openModalSmall(d);
  }

  onRnr(uses: Uses, template:TemplateRef<any>)
  {
    console.log(uses);
    this.store$.dispatch(new GetServices("vrati usluge"));
    console.log(this.services$);
    this.subs = this.services$.subscribe(el => {
      if(el!==undefined)
      {
       let s = el.filter(x => x.DESCRIPTION==uses.SERVICE_NAME);
       uses.ID_SERVICE = s[0].ID;
       let year = uses.START_TIME.getFullYear();
       let month = uses.START_TIME.getMonth();
       let day = uses.START_TIME.getDay();
       let hours = uses.START_TIME.getHours()-2;
       let minutes = uses.START_TIME.getMinutes();
       let seconds = uses.START_TIME.getSeconds();
       uses.START=year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
       uses.END=null;
       this.postService(uses, template);
      }
    });
  }

  postService(uses:Uses, template:TemplateRef<any>)
  {
    this.subs.unsubscribe();
    this.store$.dispatch(new PostService(uses));
    this.response$.subscribe(r => {
      if(r!=undefined)
        this.openModalSmall(template);
    });
  }

  onActivation(uses, type, temp)
  {
    //type za wifi dorucak rucak vecera poy
    console.log(uses);
  }
  
  onChildCare(uses, temp)
  {
    console.log(uses);
  }

  onRoomService(uses, temp)
  {
    console.log(uses);
  }
}
