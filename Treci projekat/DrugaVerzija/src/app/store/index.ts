
import {ActionReducerMap, Action} from '@ngrx/store'
import { Review } from '../models/review';
import { reviewReducer } from './reducers/review.reducer';
import { reviewsReducer } from './reducers/reviews.reducer';
import {Observable} from 'rxjs/Observable';
import {ReviewsState} from './reducers/reviews.reducer';
import {Event} from '../models/event';
import { eventsReducer, EventsState } from './reducers/events.reducer';
import { Attends } from '../models/attends';
import { eventReducer } from './reducers/event.reducer';
import { ServicesState, servicesReducer } from './reducers/services.reducer';
import { LoginState, loginReducer } from './reducers/login.reducer';
import { Reservation } from '../models/reservation';
import { ReservationsState, reservationsReducer } from './reducers/reservations.reducer';

export interface State {
   review: Review
   reviews : ReviewsState,
   events: EventsState,
   event: Attends,
   services: ServicesState, 
   logIn:LoginState,
   reservations:ReservationsState
}

export const rootReducer: ActionReducerMap<State> = {
    review: reviewReducer,
    reviews: reviewsReducer,
    events: eventsReducer,
    event: eventReducer,
    services: servicesReducer,
    logIn: loginReducer,
    reservations:reservationsReducer
}