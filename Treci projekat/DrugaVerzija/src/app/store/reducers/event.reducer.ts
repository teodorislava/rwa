import * as Actions from '../actions';
import {Attends} from '../../models/attends';

export type Action = Actions.AllEventS;

export function eventReducer(state: Attends, action:Action)
{
    switch(action.type) {
        case Actions.EVENT_SIGNUP:
            return {...state};
        case Actions.EVENT_SIGNUP_SUCCESS:
            return {...state, ...action.payload};
        default:
            return state;
    }
}