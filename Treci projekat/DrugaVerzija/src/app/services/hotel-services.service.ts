import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Service } from '../models/service';
import { HttpClient } from '@angular/common/http';
import { Uses } from '../models/uses';

@Injectable()
export class HotelServicesService {

  constructor(private http:HttpClient) { }

  getServices(): Observable<Service[]>
  {
    let url='http://localhost:8000/api/service';
    return this.http.get<Service[]>(url);
  }

  createService(payload:Uses) : Observable<Response>
  {
    let url='http://localhost:8000/api/uses';
    return this.http.post<Response>(url, payload);
  }
}
