import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Reservation } from '../models/reservation';
import { HttpClient } from '@angular/common/http';
import { RoomServiceComponent } from '../components/room-service/room-service.component';
import { Room } from '../models/room';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../models/get-details';
import { Client } from '../models/client';

@Injectable()
export class ReservationsService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getReservations(): Observable<Reservation[]>
  {
    const url='http://localhost:8000/api/get-reservation';
    const type = this.cookieService.get('type');
    const user = this.cookieService.get('token');
    const log = new GetDetails(user, type);
    console.log(log);
    return this.http.post<Reservation[]>(url, log);
  }

  getRooms(): Observable<Room[]>
  {
    let url='http://localhost:8000/api/room';
    return this.http.get<Room[]>(url);
  }

  createReservation(payload:Reservation): Observable<Response>
  {
    let url='http://localhost:8000/api/reservation';
    return this.http.post<Response>(url, payload);
  }

  createClient(payload:Client): Observable<Response>
  {
    console.log(payload);
    let url='http://localhost:8000/api/client';
    return this.http.post<Response>(url, payload);
  }
}
