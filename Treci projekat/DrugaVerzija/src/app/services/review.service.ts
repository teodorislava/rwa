import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Review } from '../models/review';
import { Observable } from 'rxjs/internal/Observable';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../models/get-details';
import { Store } from '@ngrx/store';
import { State } from '../store';
import { GetLogged } from '../store/actions';

@Injectable()
export class ReviewService {

  constructor(private http:HttpClient, private cookieService:CookieService, private store$:Store<State>) { }

  createReview(payload:Review) 
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    const log = new GetDetails(token, type);

    const body = {review: payload, token:token, type:type};
    console.log(payload);
    payload.ID_CLIENT=31;
    
    let url='http://localhost:8000/api/review';
    return this.http.post<Response>(url, body);
  }

  getReviews(): Observable<Review[]>
  {
    let url='http://localhost:8000/api/review';
    return this.http.get<Review[]>(url);
  }
}
