import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Event } from '../models/event';
import { HttpClient } from '@angular/common/http';
import { Attends } from '../models/attends';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class EventService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getEvents(): Observable<Event[]>
  {
    let url='http://localhost:8000/api/event';
    return this.http.get<Event[]>(url);
  }

  createEvent(payload:Attends) 
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');

    const body = {attends: payload, token:token, type:type};
    payload.ID_CLIENT=31;

    let url='http://localhost:8000/api/attends';
    return this.http.post<Response>(url, body);
  }
}
