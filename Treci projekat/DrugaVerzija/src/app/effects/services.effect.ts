import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';

import * as ServicesActions from '../store/actions';
import { HotelServicesService } from '../services/hotel-services.service';

@Injectable()
export class GetServicesEffect
{
    constructor(private actions:Actions, private hsService:HotelServicesService) {}

    @Effect()
    getServices$ = this.actions.ofType(ServicesActions.GET_SERVICES)
        .pipe(switchMap(() =>
            {
                return this.hsService.getServices()
                    .pipe(map( s => new ServicesActions.ReturnServices(s)))
            }
        ));
}

@Injectable()
export class ServicesEffects
{
    constructor(private actions:Actions, private hsService: HotelServicesService) {}

    @Effect()
    createService$ = this.actions.ofType(ServicesActions.POST_SERVICE)
        .map((action:ServicesActions.PostService) => action.payload)
        .pipe(switchMap((el) => {
            return this.hsService.createService(el)
            .pipe(map(el=> { console.log(el); return new ServicesActions.PostServiceSuccess(el)}));
        } ));
}