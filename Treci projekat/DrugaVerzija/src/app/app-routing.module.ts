import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { EventsComponent } from './components/events/events.component';
import { ReviewComponent } from './components/review/review.component';
import { HomeComponent } from './components/home/home.component';
import { StartpageComponent } from './components/startpage/startpage.component';
import { HotelServiceComponent } from './components/hotel-service/hotel-service.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';
import {MainComponent} from './components/main/main.component';

const routes : Routes = [
  {
    path:'startpage',
    component: StartpageComponent
  },
  {
    path:'reservations',
    component: ReservationsComponent
  },
  {
    path:'events',
    component: EventsComponent
  },
  {
    path:'reviews',
    component: ReviewComponent
  },
  {
    path:'services',
    component: HotelServiceComponent
  },
  {
    path:'services/restaurant',
    component: RestaurantComponent
  },
  {
    path:'main',
    component: MainComponent
  },
  {
    path:'',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path:'**',
    component:PageNotFoundComponent
  }
]
@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
