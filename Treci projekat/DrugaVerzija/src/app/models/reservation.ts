export class Reservation
{
    constructor(
        public TYPE: string,
        public WIFI: number,
        public AC:number,
        public BALCONY:number,
        public START:string,
        public FINISH:string,
        public RESERVATION_DATE: string,
        public ID_ROOM: number
    ) {}
} 