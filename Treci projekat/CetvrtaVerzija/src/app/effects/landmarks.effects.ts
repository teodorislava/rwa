import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';
import {Landmark} from '../models/landmarks';

import * as LandmarkActions from '../store/actions';
import { LandmarksService } from '../services/landmarks.service';


@Injectable()
export class LandmarkEffects
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    getLandmarks$ = this.actions.ofType(LandmarkActions.GET_LANDMARKS)
        .pipe(switchMap(() => {
            return this.lService.getLandmarks()
            .pipe(map(el => new LandmarkActions.ReturnLandmarks(el)));
        } ));
}

@Injectable()
export class InterestEffect
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    createClient$ = this.actions.ofType(LandmarkActions.POST_INTEREST)
        .map((action:LandmarkActions.PostInterest) => action.payload)
        .pipe(switchMap((el) => {
            return this.lService.addVisit(el)
            .pipe(map(el => new LandmarkActions.PostInterestSuccess(el)));
        } ));
}

@Injectable()
export class DeleteInterestEffect
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    createClient$ = this.actions.ofType(LandmarkActions.DELETE_VISIT)
        .map((action:LandmarkActions.DeleteVisit) => action.payload)
        .pipe(switchMap((el) => {
            return this.lService.deleteVisit(el.id)
            .pipe(map(el => new LandmarkActions.DeleteVisitResponse(el)));
        } ));
}
