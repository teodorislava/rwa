import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';

import * as ReservationsActions from '../store/actions';
import { ReservationsService } from '../services/reservations.service';

@Injectable()
export class GetReservationsEffect
{
    constructor(private actions:Actions, private rService:ReservationsService) {}

    @Effect()
    getReservations$ = this.actions.ofType(ReservationsActions.GET_RESERVATIONS)
        .pipe(switchMap(() =>
            {
                return this.rService.getReservations()
                    .pipe(map( r => new ReservationsActions.ReturnReservations(r)))
            }
        ));
}

@Injectable()
export class ReservationsEffect
{
    constructor(private actions:Actions, private rService: ReservationsService) {}

    @Effect()
    createReservation$ = this.actions.ofType(ReservationsActions.POST_RESERVATION)
        .map((action:ReservationsActions.PostReservation) => action.payload)
        .pipe(switchMap((el) => {
            return this.rService.createReservation(el)
            .pipe(map(el => new ReservationsActions.PostReservationResponse(el)));
        } ));
}

@Injectable()
export class ClientEffect
{
    constructor(private actions:Actions, private rService: ReservationsService) {}

    @Effect()
    createClient$ = this.actions.ofType(ReservationsActions.POST_CLIENT)
        .map((action:ReservationsActions.PostClient) => action.payload)
        .pipe(switchMap((el) => {
            return this.rService.createClient(el)
            .pipe(map(el => new ReservationsActions.PostClientResponse(el)));
        } ));
}

@Injectable()
export class GetRoomsEffect
{
    constructor(private actions:Actions, private rService:ReservationsService) {}

    @Effect()
    getRooms$ = this.actions.ofType(ReservationsActions.GET_ROOMS)
        .pipe(switchMap(() =>
            {
                return this.rService.getRooms()
                    .pipe(map( r => new ReservationsActions.ReturnRooms(r)))
            }
        ));
}