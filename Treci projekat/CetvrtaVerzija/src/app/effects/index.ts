import {GetReviewEffects, ReviewEffects} from './review.effects';
import { GetEventsEffect, EventEffects } from './event.effects';
import { GetServicesEffect, ServicesEffects, GetUsesEffects } from './services.effect';
import { LoginEffects, GetLoggedEffect } from './login.effects';
import { ReservationsEffect, GetReservationsEffect, GetRoomsEffect, ClientEffect } from './reservations.effects';
import { LandmarkEffects, DeleteInterestEffect, InterestEffect } from './landmarks.effects';

export const effects : any[] = [GetReviewEffects, 
                                ReviewEffects, 
                                GetEventsEffect, 
                                EventEffects,
                                GetServicesEffect,
                                ServicesEffects,
                                LoginEffects,
                                GetLoggedEffect,
                                GetReservationsEffect,
                                ReservationsEffect,
                                GetRoomsEffect,
                                ClientEffect,
                                LandmarkEffects,
                                GetUsesEffects,
                                DeleteInterestEffect,
                                InterestEffect];
