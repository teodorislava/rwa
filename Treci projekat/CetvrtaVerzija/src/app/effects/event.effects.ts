import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/Observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';
import {Event} from '../models/event';

import * as EventActions from '../store/actions';
import { EventService } from '../services/event.service';

@Injectable()
export class GetEventsEffect
{
    constructor(private actions:Actions, private eventService:EventService) {console.log("dosao sam u ovaj efekat")}

    @Effect()
    getEvents$ = this.actions.ofType(EventActions.GET_EVENTS)
        .pipe(switchMap(() =>
            {
                return this.eventService.getEvents()
                    .pipe(map( ev => new EventActions.ReturnEvents(ev)))
        }
        ));
}

@Injectable()
export class EventEffects
{
    constructor(private actions:Actions, private eventService: EventService) {}

    @Effect()
    createReview$ = this.actions.ofType(EventActions.EVENT_SIGNUP)
        .map((action:EventActions.PostEvent) => action.payload)
        .pipe(switchMap((el) => {
            return this.eventService.createEvent(el)
            .pipe(map(el=> new EventActions.PostEventSuccess(el)));
        } ));
}