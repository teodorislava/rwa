export class Landmark {
    constructor(
        public ID:number, 
        public id:number,
        public LANDMARK:string, 
        public DESCRIPTION:string,
        public VISITS:number
    ) {}
}