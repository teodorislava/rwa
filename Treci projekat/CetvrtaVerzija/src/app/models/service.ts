export class Service
{
    constructor(
        public ID:number,
        public PRICE:number,
        public DESCRIPTION:string,
        public VALID_FROM:Date,
        public VALID_TO: Date,
        public ID_HOTEL: number,
        public TYPE:string,
        public ROOM_TYPE:string,
        public RNR_TYPE:string,
        public LENGTH:number,
        public START_TIME: Date,
        public DURATION:number
    ) {}
}