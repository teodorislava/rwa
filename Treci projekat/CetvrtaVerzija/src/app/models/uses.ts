export class Uses
{
    constructor(
        public ID:number,
        public ID_CLIENT:number,
        public ID_SERVICE:number,
        public DURATION:number,
        public START_TIME:Date,
        public START:string,
        public END_TIME:string,
        public SERVICE_NAME:string, 
        public END:Date,
        public URGENT:boolean
    ) {}
}