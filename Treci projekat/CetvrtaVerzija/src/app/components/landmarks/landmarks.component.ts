import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Landmark } from '../../models/landmarks';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import {selectors as landmarksSelectors} from '../../store/reducers/landmarks.reducer';
import { GetLandmarks, PostInterest, AddVisit, DeleteVisit } from '../../store/actions';
import { Interest } from '../../models/interest';

@Component({
  selector: 'app-landmarks',
  templateUrl: './landmarks.component.html',
  styleUrls: ['./landmarks.component.css']
})
export class LandmarksComponent implements OnInit {

  landmarks$:Observable<Landmark[]>;
  
  constructor(private store$: Store<State>) { }

  ngOnInit() {
    this.landmarks$ = this.store$.select((state:State) => landmarksSelectors.selectAll(state.landmarks));
    console.log(this.landmarks$);
    this.store$.dispatch(new GetLandmarks());
    console.log(this.landmarks$);
    this.landmarks$.subscribe(el => {
      this.landmarks$=of(el);
    })
  }

  onVisit(l:Landmark)
  {
    this.store$.dispatch(new PostInterest(new Interest(0,0,l.id)));
    l.VISITS = l.VISITS+1;
    this.store$.dispatch(new AddVisit(l));
  }

  onDelete(l: Landmark)
  {
    l.VISITS = l.VISITS-1;
    this.store$.dispatch(new DeleteVisit(l));
  }
}
