import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Uses } from '../../models/uses';

@Component({
  selector: 'app-child-care',
  templateUrl: './child-care.component.html',
  styleUrls: ['./child-care.component.css']
})
export class ChildCareComponent implements OnInit {

  @Output()
  chose:EventEmitter<Uses> = new EventEmitter<Uses>();

  uses:Uses = new Uses(0,1,0,0,new Date(),"", "", "Vrtic", new Date(), false);
  constructor() { }

  ngOnInit() {
  }

  onClick()
  {
    this.chose.emit(this.uses);
  }
  onChangeStart(time)
  {
    const spl=time.target.value.split(':');
    let hour;
    let minute;

    if(spl[0]==="")
    {
      hour=0;
      minute=0;
    }
    else
    {
      hour=parseInt(spl[0],10)+2;
      minute=parseInt(spl[1],10);
    }
    this.uses.START_TIME.setHours(hour, minute);
  }
  onChangeEnd(time)
  {
    const spl=time.target.value.split(':');
    let hour;
    let minute;

    if(spl[0]==="")
    {
      hour=0;
      minute=0;
    }
    else
    {
      hour=parseInt(spl[0],10)+2;
      minute=parseInt(spl[1],10);
    }
    this.uses.END.setHours(hour, minute);
  }
}
