import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Uses } from '../../models/uses';

@Component({
  selector: 'app-activate-service',
  templateUrl: './activate-service.component.html',
  styleUrls: ['./activate-service.component.css']
})
export class ActivateServiceComponent implements OnInit {

  date: Date = new Date();
  uses: Uses = new Uses(0, 1, 0, 0, new Date(), "", "", "", new Date(), false);

  @Output()
  activate: EventEmitter<Uses> = new EventEmitter<Uses>();
  constructor() { }

  ngOnInit() {
  }

  onClick()
  {
    this.activate.emit(this.uses);
  }
}
