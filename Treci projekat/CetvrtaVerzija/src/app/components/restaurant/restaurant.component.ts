import { Component, OnInit, TemplateRef, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  @Output()
  chose: EventEmitter<string>=new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }
  
  onMealChosen(s: string)
  {
    this.chose.emit(s);
  }
}
