import { Component, OnInit } from '@angular/core';
import { Reservation } from '../../models/reservation';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { GetReservations, GetRooms, PostClient, PostReservation } from '../../store/actions';
import { Room } from '../../models/room';
import { zip, of } from 'rxjs';
import { Client } from '../../models/client';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  reservation:Reservation = new Reservation("",0,0,0,"","","", 0);
  client:Client = new Client(0, "","","","","","", "",0, "", null, null, "", 0);
  reservations$:Observable<Reservation[]>;
  rooms$:Observable<Room[]>;
  response$:Observable<Response>;
  responseClient$:Observable<Response>;
  beds:number;

  res:boolean = false;
  date:Date = new Date();

  constructor(private store$:Store<State>) { }

  ngOnInit() {
    this.reservations$ = this.store$.select(state => state.reservations).map(re => re.reservations);
    this.response$ = this.store$.select(state => state.reservations).map(re => re.response);
    this.responseClient$ = this.store$.select(state => state.reservations).map(re => re.responseClient);
    this.rooms$ = this.store$.select(state => state.reservations).map(re => re.rooms); 
  }
  
  onWifi(val)
  {
    if(val.checked)
      this.reservation.WIFI=1;
    else
      this.reservation.WIFI=0;
  }

  onAC(val)
  {
    if(val.checked)
      this.reservation.AC=1;
    else
      this.reservation.AC=0;
  }

  onBalcony(val)
  {
    if(val.checked)
      this.reservation.BALCONY=1;
    else
      this.reservation.BALCONY=0;
  }

  onStart(val)
  {
    let d = val.target.value;
    let s = d.split('/');
    if(s.length!=0)
    {
      if(Number(s[0])>=10 && Number(s[1])>=10)
        this.reservation.START=s[2]+'-'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])>=10)
        this.reservation.START=s[2]+'-0'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])<10)
        this.reservation.START=s[2]+'-0'+s[0]+'-0'+s[1]+' 00:00:00';
      else
        this.reservation.START=s[2]+'-'+s[0]+'-0'+s[1]+' 00:00:00';
    }
  }

  onEnd(val)
  {
    let d = val.target.value;
    let s = d.split('/');
    if(s.length!=0)
    {
      if(Number(s[0])>=10 && Number(s[1])>=10)
        this.reservation.FINISH=s[2]+'-'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])>=10)
        this.reservation.FINISH=s[2]+'-0'+s[0]+'-'+s[1]+' 00:00:00';
      else if(Number(s[0])<10 && Number(s[1])<10)
        this.reservation.FINISH=s[2]+'-0'+s[0]+'-0'+s[1]+' 00:00:00';
      else
        this.reservation.FINISH=s[2]+'-'+s[0]+'-0'+s[1]+' 00:00:00';
    }
  }

  onRequestRooms()
  {
    this.store$.dispatch(new GetReservations());
    this.store$.dispatch(new GetRooms());

    const obs = zip(this.rooms$, this.reservations$);

      obs.subscribe(data => {
      const rooms = data[0].filter(room => (room.TYPE==this.reservation.TYPE && room.AC==this.reservation.AC
      && room.BALCONY==this.reservation.BALCONY && room.NUMBER_OF_BEDS==this.beds));
      const rooms_id = rooms.map(r => r.ID);
      const ress = data[1].filter(el => (rooms_id.includes(el.ID_ROOM)));
      const sr = new Date(this.reservation.START);
      const er = new Date(this.reservation.FINISH);
      const final = ress.filter(el => (!(sr <= new Date(el.START) && er <= new Date(el.START)) &&
        !(er >= new Date(el.FINISH) && sr >= new Date(el.FINISH))));
      const ok_res = final.map(el => el.ID_ROOM);
      const available = rooms.filter(el => !(ok_res.includes(el.ID)));
      this.rooms$ = of(available);
    });
  }
  onType(val)
  {
    this.reservation.TYPE = val.value;
  }

  onNumberOfBeds(val)
  {
    this.beds=val.target.value;
  }

  onRoomSelect(id:number)
  {
    this.reservation.ID_ROOM=id;
    this.res=true;
  }

  onReservation()
  {
    this.store$.dispatch(new PostClient(this.client));
    this.store$.dispatch(new PostReservation(this.reservation));
  }

  onName(val)
  {
    this.client.NAME=val.target.value;
  }
  onLastName(val)
  {
    console.log(val);
    this.client.LAST_NAME=val.target.value;
  }
  onPassport(val)
  {
    this.client.PASSPORT_NUMBER=val.target.value;
  }
  onJMBG(val)
  {
    this.client.JMBG=val.target.value;
  }
  onEmail(val)
  {
    this.client.EMAIL=val.target.value;
  }
  onCountry(val)
  {
    this.client.COUNTRY=val.value;
  }
  onPhone(val)
  {
    this.client.PHONE_NUMBER=val.target.value;
  }
  onAdress(val)
  {
    this.client.ADDRESS=val.target.value;
  }
}