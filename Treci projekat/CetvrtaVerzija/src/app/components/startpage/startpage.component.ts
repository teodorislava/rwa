import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import {Router} from "@angular/router";

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.css']
})
export class StartpageComponent implements OnInit {

  modalRef:BsModalRef;
  public msg:string="";
  constructor(public modalService: BsModalService, private router:Router) { }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class:'modal-sm', animated: true });
  }

  onLoggedIn(msg:string, tempOld: TemplateRef<any>, tempNew:TemplateRef<any>)
  {
    this.msg=msg;
    console.log(this.msg);
    this.modalRef.hide();
    this.openModal(tempNew);
  }

  onMsgClosed()
  {
    this.modalRef.hide();
    this.router.navigate(['./startpage']);
  }
}
