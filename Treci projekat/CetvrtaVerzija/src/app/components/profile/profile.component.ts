import { Component, OnInit } from '@angular/core';
import { Client } from '../../models/client';
import { Observable } from 'rxjs';
import { State } from '../../store';
import { Store } from '@ngrx/store';
import { GetLogged, GetEvents } from '../../store/actions';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../../models/get-details';
import {Event} from '../../models/event';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  client$: Observable<Client>;
  tab:string = "licni";
  events$: Observable<Event[]>;

  constructor(private store$: Store<State>, private cookieService:CookieService) { }

  ngOnInit() {
    this.client$ = this.store$.select(state => state.logIn).map(el => el.client);
    this.events$ = this.store$.select(state => state.events).map(el => el.events);
    if(this.cookieService.check('token'))
    {
      let token = this.cookieService.get('token');
      let type = this.cookieService.get('type');
      let data = new GetDetails(token, type);
      console.log(data);
      this.store$.dispatch(new GetLogged(data));
      console.log(this.client$);
    }
    
    
  }

  onChange(tab:string)
  {
    console.log(tab);
    this.tab=tab;
    if(tab=='utisci')
      this.store$.dispatch(new GetEvents());
  }
}
