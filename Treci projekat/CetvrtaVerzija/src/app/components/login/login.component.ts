import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import { TryLogin, GetLogged } from '../../store/actions';
import { Observable } from 'rxjs/Observable';
import { Client } from '../../models/client';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../../models/get-details';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output()
  public login:EventEmitter<string> = new EventEmitter<string>();

  username:string;
  password:string;
  response$:Observable<Response>;
  client$:Observable<Client>;
  
  constructor(private cookieService: CookieService,public store$:Store<State>) { }

  ngOnInit() {
    this.store$.select((state:State) => state.logIn);
    this.response$=this.store$.select((state:State) => state.logIn).map(el => el.response);
    this.client$=this.store$.select((state:State) => state.logIn).map(el => el.client);
  }

  onLogin()
  {
    this.store$.dispatch(new TryLogin({USERNAME:this.username, PASSWORD:this.password}));
    console.log(this.response$);
    this.response$.subscribe(res => {
          console.log(res);
          this.cookieService.set('token', res.text.toString());
          this.cookieService.set('type', 'client');
          this.login.emit('Uspešno ste se ulogovali!');
    });
  }

  onUsername(user)
  {
    this.username=user.target.value;
  }

  onPassword(pass)
  {
    this.password=pass.target.value;
  }

}
