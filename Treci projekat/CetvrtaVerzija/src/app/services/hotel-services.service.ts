import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Service } from '../models/service';
import { HttpClient } from '@angular/common/http';
import { Uses } from '../models/uses';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class HotelServicesService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getServices(): Observable<Service[]>
  {
    let url='http://localhost:8000/api/get-service';
    return this.http.get<Service[]>(url);
  }

  createService(payload:Uses) : Observable<Response>
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');

    const body = {uses: payload, token:token, type:type};
    let url='http://localhost:8000/api/uses';
    return this.http.post<Response>(url, body);
  }

  getUses(): Observable<Uses[]>
  {
    let url='http://localhost:8000/api/uses';
    let body='2';
    return this.http.post<Uses[]>(url, body);
  }
}
