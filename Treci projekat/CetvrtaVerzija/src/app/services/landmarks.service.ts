import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { Landmark } from '../models/landmarks';
import { Interest } from '../models/interest';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LandmarksService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getLandmarks(): Observable<Landmark[]>
  {
    const url='http://localhost:8000/api/get-landmark';
    return this.http.get<Landmark[]>(url);
  }

  addVisit(payload: Interest): Observable<Response>
  {
    const url="http://localhost:8000/api/interests";
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');
    const body = {interests:payload, token:token, type:type};
    return this.http.post<Response>(url, body);
  }

  deleteVisit(payload: number)
  {
    const url="http://localhost:8000/api/interests/"+payload;
    return this.http.delete<Response>(url);
  }

}
