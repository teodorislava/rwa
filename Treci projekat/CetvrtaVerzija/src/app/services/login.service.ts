import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Service } from '../models/service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Uses } from '../models/uses';
import { Client } from '../models/client';
import { RequestOptions, Headers } from '@angular/http';

@Injectable()
export class LoginService {

  constructor(private http:HttpClient) { }

  getDetails(token:string, type:string): Observable<Client>
  {
    let url='http://localhost:8000/api/get-details';
    console.log(token);
    console.log(type);
    return this.http.post<Client>(url, {token:token, type:type});
  }

  tryLogin(payload) : Observable<Response>
  {
    let url='http://localhost:8000/api/login';
    let h = new HttpHeaders();
    console.log(payload);
    h.set('Access-Control-Allow-Credentials', 'true');
    let options = {headers:h, withCredentials:false};
    console.log(this.http.post<Response>(url, payload, options));
    return this.http.post<Response>(url, payload, options);
  }
}