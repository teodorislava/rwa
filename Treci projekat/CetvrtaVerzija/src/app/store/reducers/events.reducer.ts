import * as EventActions from '../actions';
import {Event} from '../../models/event';
import { Observable } from 'rxjs/Observable';
export type Action = EventActions.ReturnEvents;

export interface EventsState
{
    loading:boolean,
    events:Event[]
}

const initialState:EventsState = {
    loading:true,
    events:[]
};
export function eventsReducer(state: EventsState = initialState, action:Action)
{
    switch(action.type) {
        case EventActions.RETURN_EVENTS:
            return {...state, events:action.payload, loading:false};
        default:
            return state;
    }
}