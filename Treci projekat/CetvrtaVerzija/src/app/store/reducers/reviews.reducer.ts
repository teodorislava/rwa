import * as ReviewActions from '../actions';
import {Review} from '../../models/review';
import { Observable } from 'rxjs/Observable';
export type Action = ReviewActions.ReturnReviews;

export interface ReviewsState {
    loading: boolean,
    reviews: Review[]
}

const initialState:ReviewsState = {
    loading:true,
    reviews: []
}
export function reviewsReducer(state: ReviewsState = initialState, action:Action)
{
    switch(action.type) {
        case ReviewActions.RETURN_REVIEWS:
            return {...state, reviews: action.payload, loading:false};
        default:
            return state;
    }
}