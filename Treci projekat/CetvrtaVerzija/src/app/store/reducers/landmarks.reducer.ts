import * as LandmarkActions from '../actions';
import {Landmark} from '../../models/landmarks';
import {Action} from '@ngrx/store';
import {EntityState, createEntityAdapter, EntityAdapter} from '@ngrx/entity';

export interface LandmarkState extends EntityState<Landmark> {};

const adapter: EntityAdapter<Landmark> = createEntityAdapter<Landmark>();

const landmarks: Landmark[] = [

];


const InitialState: LandmarkState = {
    ids:[],
    entities: {
    }
}
export function landmarksReducer(state: LandmarkState = InitialState, action:Action)
{
    switch(action.type) {
        case LandmarkActions.RETURN_LANDMARKS:
        {
            const {payload} = (action as LandmarkActions.ReturnLandmarks);
            const ad =  adapter.addAll(payload, state);
            return ad;
        }
        case LandmarkActions.ADD_VISIT:
        {
            const {payload} = (action as LandmarkActions.AddVisit);
            console.log({payload});
            const ad =  adapter.updateOne({id:payload.id, changes:payload}, state);
            return ad;
        }
        case LandmarkActions.DELETE_VISIT:
        {
            const {payload} = (action as LandmarkActions.DeleteVisit);
            const ad =  adapter.updateOne({id:payload.id, changes:payload}, state);
            return ad;
        }
        default:
        {
            console.log(state);
            return state;
        }
    }
}

export const selectors = adapter.getSelectors();