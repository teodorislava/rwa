import * as ReviewActions from '../actions';
import {Review} from '../../models/review';

export type Action = ReviewActions.AllPostReview;

export function reviewReducer(state: Review, action:Action)
{
    switch(action.type) {
        case ReviewActions.POST_REVIEW:
            return {...state};
        case ReviewActions.POST_REVIEW_SUCCESS:
            return {...state, ...action.payload};
        default:
            return state;
    }
}