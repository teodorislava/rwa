import {Action} from '@ngrx/store';
import {Review} from '../models/review';
import {Observable} from 'rxjs/Observable';
import { Event } from '../models/event';
import { Attends } from '../models/attends';
import { Uses } from '../models/uses';
import { Service } from '../models/service';
import { Client } from '../models/client';
import { Reservation } from '../models/reservation';
import { Room } from '../models/room';
import { GetDetails } from '../models/get-details';
import { Landmark } from '../models/landmarks';
import { Interest } from '../models/interest';

export const POST_REVIEW = "Post review";
export const POST_REVIEW_SUCCESS = "Post review success";
export const GET_REVIEWS = "Get reviews";
export const RETURN_REVIEWS = "Return reviews";
export const GET_EVENTS="Get events";
export const RETURN_EVENTS="Return events";
export const EVENT_SIGNUP="Event sign up";
export const EVENT_SIGNUP_SUCCESS="Event sign up succes";
export const GET_SERVICES="Get services";
export const RETURN_SERVICES="Return services";
export const POST_SERVICE="Post service";
export const POST_SERVICE_SUCCESS="Post service success";
export const TRY_LOGIN="Try login";
export const LOGIN_RESPONSE="Login response";
export const GET_LOGGED="Get logged";
export const RETURN_LOGGED="Return logged";
export const POST_RESERVATION="Post reservation";
export const POST_RESERVATION_RESPONSE="Post reservation response";
export const GET_RESERVATIONS="Get reservations";
export const RETURN_RESERVATIONS="Return reservations";
export const GET_ROOMS="Get rooms";
export const RETURN_ROOMS="Return rooms";
export const POST_CLIENT="Post client";
export const POST_CLIENT_RESPONSE="Post client response";
export const GET_LANDMARKS="Get landmarks";
export const RETURN_LANDMARKS="Return landmarks";

export const ADD_VISIT = "Add visit";
export const ADD_VISIT_RESPONSE="AVR";
export const DELETE_VISIT = "Delete visit response";
export const DELETE_VISIT_RESPONSE = "DVR";
export const RETURN_USES="Return uses";
export const GET_USES="Get uses";
export const POST_INTEREST="PI";
export const POST_INTEREST_SUCCESS="PIS";
export const GET_INTEREST="GI";
export const RETURN_INTEREST="RI";


export class PostReview implements Action
{
    type=POST_REVIEW;
    constructor(public payload: Review) {}
}

export class PostEvent implements Action
{
    type=EVENT_SIGNUP;
    constructor(public payload: Attends) {}
}

export class PostEventSuccess implements Action
{
    type=EVENT_SIGNUP_SUCCESS;
    constructor(public payload: Response) {}
}

export class PostReviewSuccess implements Action
{
    type=POST_REVIEW_SUCCESS;
    constructor(public payload: Response) {}
}

export class GetReviews implements Action
{
    type=GET_REVIEWS;
    constructor() {}
}

export class ReturnReviews implements Action
{
    type=RETURN_REVIEWS;
    constructor(public payload: Review[]) {}
}


export class GetEvents implements Action
{
    type=GET_EVENTS;
    constructor() {}
}

export class ReturnEvents implements Action
{
    type=RETURN_EVENTS;
    constructor(public payload: Event[]) {}
}

export class GetServices implements Action
{
    type=GET_SERVICES;
    constructor(public payload:string) {}
}

export class ReturnServices implements Action
{
    type=RETURN_SERVICES;
    constructor(public payload: Service[]) {}
}

export class PostService implements Action
{
    type=POST_SERVICE;
    constructor(public payload: Uses) {}
}

export class PostServiceSuccess implements Action
{
    type=POST_SERVICE_SUCCESS;
    constructor(public payload: Response) {}
}

export class GetLogged implements Action
{
    type=GET_LOGGED;
    constructor(public payload:GetDetails) {}
}

export class ReturnLogged implements Action
{
    type=RETURN_LOGGED;
    constructor(public payload: Client) {}
}

export class TryLogin implements Action
{
    type=TRY_LOGIN;
    constructor(public payload) {}
}

export class LoginResponse implements Action
{
    type=LOGIN_RESPONSE;
    constructor(public payload: Response) {}
}

export class GetReservations implements Action
{
    type=GET_RESERVATIONS;
    constructor() {}
}

export class ReturnReservations implements Action
{
    type=RETURN_RESERVATIONS;
    constructor(public payload: Reservation[]) {}
}

export class PostReservation implements Action
{
    type=POST_RESERVATION;
    constructor(public payload: Reservation) {}
}

export class PostReservationResponse implements Action
{
    type=POST_RESERVATION_RESPONSE;
    constructor(public payload: Response) {}
}

export class GetRooms implements Action
{
    type=GET_ROOMS;
    constructor() {}
}

export class ReturnRooms implements Action
{
    type=RETURN_ROOMS;
    constructor(public payload: Room[]) {}
}

export class PostClient implements Action
{
    type=POST_CLIENT;
    constructor(public payload: Client) {}
}

export class PostClientResponse implements Action
{
    type=POST_CLIENT_RESPONSE;
    constructor(public payload: Response) { console.log(payload)}
}

export class GetLandmarks implements Action
{
    type=GET_LANDMARKS;
    constructor() {}
}

export class ReturnLandmarks implements Action
{
    type=RETURN_LANDMARKS;
    constructor(public payload: Landmark[]) { console.log(payload)}
}

export class GetUses implements Action
{
    type=GET_USES;
    constructor() {}
}

export class ReturnUses implements Action
{
    type=RETURN_USES;
    constructor(public payload: Uses[]) {}
}

export class GetInterest implements Action
{
    type=GET_INTEREST;
    constructor() {}
}

export class ReturnInterest implements Action
{
    type=RETURN_INTEREST;
    constructor(public payload: Interest[]) {}
}

export class PostInterest implements Action
{
    type=POST_INTEREST;
    constructor(public payload: Interest) {}
}

export class PostInterestSuccess implements Action
{
    type=POST_INTEREST_SUCCESS;
    constructor(public payload: Response) {}
}

export class AddVisit implements Action
{
    type=ADD_VISIT;
    constructor(public payload: Partial<Landmark>) {}
}

export class AddVisitResponse implements Action
{
    type=ADD_VISIT_RESPONSE;
    constructor(public payload: Response) {}
}

export class DeleteVisit implements Action
{
    type=DELETE_VISIT;
    constructor(public payload: Landmark) {}
}

export class DeleteVisitResponse implements Action
{
    type=DELETE_VISIT;
    constructor(public payload: Response) {}
}

export type AllPostReview
    = PostReview
    | PostReviewSuccess;

export type AllEventS
    = PostEvent
    | PostEventSuccess;