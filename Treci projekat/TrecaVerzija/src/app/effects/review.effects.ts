import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/Observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';
import {Review} from '../models/review';

import * as ReviewActions from '../store/actions';
import { ReviewService } from '../services/review.service';


@Injectable()
export class ReviewEffects
{
    constructor(private actions:Actions, private reviewService: ReviewService) {}

    @Effect()
    createReview$ = this.actions.ofType(ReviewActions.POST_REVIEW)
        .map((action:ReviewActions.PostReview) => action.payload)
        .pipe(switchMap((el) => {
            return this.reviewService.createReview(el)
            .pipe(map(el=> new ReviewActions.PostReviewSuccess(el)));
        } ));
}

@Injectable()
export class GetReviewEffects
{
    constructor(private actions:Actions, private reviewService:ReviewService) {console.log("pozvao sam ovaj efekaat!")}

    @Effect()
    getReviews$ = this.actions.ofType(ReviewActions.GET_REVIEWS)
        .pipe(switchMap(() =>
            {
                return this.reviewService.getReviews()
                    .delay(2000)
                    .pipe(map( rev => new ReviewActions.ReturnReviews(rev)))
        }
        ));
}