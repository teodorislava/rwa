import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import {map, switchMap} from 'rxjs/operators';
import {Landmark} from '../models/landmarks';

import * as LandmarkActions from '../store/actions';
import { LandmarksService } from '../services/landmarks.service';


@Injectable()
export class LandmarkEffects
{
    constructor(private actions:Actions, private lService: LandmarksService) {}

    @Effect()
    getLandmarks$ = this.actions.ofType(LandmarkActions.GET_LANDMARKS)
        .pipe(switchMap(() => {
            return this.lService.getLandmarks()
            .pipe(map(el => new LandmarkActions.ReturnLandmarks(el)));
        } ));
}