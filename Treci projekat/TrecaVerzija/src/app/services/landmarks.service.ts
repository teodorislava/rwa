import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { Landmark } from '../models/landmarks';

@Injectable()
export class LandmarksService {

  constructor(private http:HttpClient) { }

  getLandmarks(): Observable<Landmark[]>
  {
    const url='http://localhost:8000/api/get-landmark';
    return this.http.get<Landmark[]>(url);
  }
}
