import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Event } from '../models/event';
import { HttpClient } from '@angular/common/http';
import { Attends } from '../models/attends';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class EventService {

  constructor(private http:HttpClient, private cookieService:CookieService) { }

  getEvents(): Observable<Event[]>
  {
    let url='http://localhost:8000/api/get-event';
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');

    const body = {token:token, type:type};

    return this.http.post<Event[]>(url, body);
  }

  createEvent(payload:Attends) 
  {
    const token = this.cookieService.get('token');
    const type = this.cookieService.get('type');

    const body = {attends: payload, token:token, type:type};

    let url='http://localhost:8000/api/attends';
    return this.http.post<Response>(url, body);
  }
}
