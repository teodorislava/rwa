import { TestBed, inject } from '@angular/core/testing';

import { HotelServicesService } from './hotel-services.service';

describe('HotelServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HotelServicesService]
    });
  });

  it('should be created', inject([HotelServicesService], (service: HotelServicesService) => {
    expect(service).toBeTruthy();
  }));
});
