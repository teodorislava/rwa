import { TestBed, inject } from '@angular/core/testing';

import { LandmarksService } from './landmarks.service';

describe('LandmarksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LandmarksService]
    });
  });

  it('should be created', inject([LandmarksService], (service: LandmarksService) => {
    expect(service).toBeTruthy();
  }));
});
