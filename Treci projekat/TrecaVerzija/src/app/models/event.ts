
export class Event
{
    constructor(
      public NAME:string,
      public START:Date,
      public CAPACITY:number,
      public OPIS:string,
      public PRICE:number,
      public ID:number 
    )
    {}
}