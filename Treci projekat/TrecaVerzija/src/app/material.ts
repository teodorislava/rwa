import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatNativeDateModule} from '@angular/material';
import {MatIconModule} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';

import { NgModule } from '@angular/core';

@NgModule({
  imports: [MatDialogModule, MatExpansionModule, MatListModule, MatDividerModule, MatGridListModule, MatIconModule, MatProgressSpinnerModule, MatNativeDateModule, MatInputModule, MatDatepickerModule, MatSlideToggleModule, MatSelectModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatSidenavModule, MatFormFieldModule],
  exports: [MatDialogModule, MatExpansionModule, MatListModule, MatDividerModule, MatGridListModule, MatIconModule, MatProgressSpinnerModule, MatNativeDateModule, MatInputModule, MatDatepickerModule, MatSlideToggleModule, MatSelectModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatSidenavModule, MatFormFieldModule],
})
export class MyMaterialModule { }