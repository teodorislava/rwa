import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Uses } from '../../models/uses';
import { NumberValueAccessor } from '@angular/forms/src/directives';

@Component({
  selector: 'app-rnr-service',
  templateUrl: './rnr-service.component.html',
  styleUrls: ['./rnr-service.component.css']
})
export class RnrServiceComponent implements OnInit {

  @Output()
  chose: EventEmitter<Uses>= new EventEmitter<Uses>();

  @Input()
  public type:string;

  order: Uses = new Uses(0,1,0,0,new Date(),"", "", "", new Date(), false);
  date:Date= new Date();
  start:Date=new Date();
  hour:number;
  minute:number;
  
  constructor() { }

  ngOnInit() {
  }

  onOrder()
  {
    this.order.START_TIME.setDate(this.start.getDate());
    this.chose.emit(this.order);
  }

  onChange(time)
  {
    const spl=time.target.value.split(':');
    if(spl[0]==="")
    {
      this.hour=0;
      this.minute=0;
    }
    else
    {
      this.hour=parseInt(spl[0],10)+2;
      this.minute=parseInt(spl[1],10);
    }
    this.order.START_TIME.setHours(this.hour, this.minute);
  }
}
