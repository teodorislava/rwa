import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Landmark } from '../../models/landmarks';
import { Store } from '@ngrx/store';
import { State } from '../../store';
import {selectors as landmarksSelectors} from '../../store/reducers/landmarks.reducer';
import { GetLandmarks } from '../../store/actions';

@Component({
  selector: 'app-landmarks',
  templateUrl: './landmarks.component.html',
  styleUrls: ['./landmarks.component.css']
})
export class LandmarksComponent implements OnInit {

  landmarks$:Observable<Landmark[]>;
  
  constructor(private store$: Store<State>) { }

  ngOnInit() {
    this.landmarks$ = this.store$.select((state:State) => landmarksSelectors.selectAll(state.landmarks));
    console.log(this.landmarks$);
    this.store$.dispatch(new GetLandmarks());
    console.log(this.landmarks$);
    this.landmarks$.subscribe(el => {
      this.landmarks$=of(el);
    })
  }

}
