import { Component, OnInit } from '@angular/core';
import {Review} from '../../models/review';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import { PostReview, GetReviews, GetLogged } from '../../store/actions';
import {State} from '../../store';
import { ReviewsState } from '../../store/reducers/reviews.reducer';
import { CookieService } from 'ngx-cookie-service';
import { GetDetails } from '../../models/get-details';
import { Client } from '../../models/client';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  public reviewList$ : Observable<Review[]>;
  public loading$ : Observable<boolean>;
  public addReview:boolean = false;
  public client$:Observable<Client>;

  constructor(private store$: Store<State>, private cookieService:CookieService) {
    
   }

  ngOnInit() {
    this.reviewList$ = this.store$.select(state => state.reviews).map(el => el.reviews);
    this.loading$ = this.store$.select(state => state.reviews).map(el => el.loading);
    this.client$ = this.store$.select(state => state.logIn).map(cl => cl.client);
    this.store$.dispatch(new GetReviews());
  }

  showAddReview()
  {
    this.addReview=true;
    this.store$.dispatch(new GetLogged(new GetDetails(this.cookieService.get('token'), this.cookieService.get('type'))));
    console.log(this.client$);
  }

  onReviewAdded(review)
  {
    this.store$.dispatch(new PostReview(review));
    this.store$.dispatch(new GetReviews());
  }

}
