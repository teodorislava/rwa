import * as ReservationActions from '../actions';
import {Action} from '@ngrx/store';
import { Reservation } from '../../models/reservation';
import { Room } from '../../models/room';

export interface ReservationsState
{
    loading:boolean,
    reservations:Reservation[],
    response:Response,
    rooms: Room[],
    responseClient: Response
}

const initialState: ReservationsState = {
    loading:true, 
    reservations:[],
    response: new Response(),
    rooms:[],
    responseClient: new Response()
}

export function reservationsReducer(state: ReservationsState = initialState, action:Action)
{
    switch(action.type) {
        case ReservationActions.RETURN_RESERVATIONS:
        {
            const {payload} = (action as ReservationActions.ReturnReservations);
            return {...state, reservations:payload,  loading:false};
        }
        case ReservationActions.RETURN_ROOMS:
        {
            const {payload} = (action as ReservationActions.ReturnRooms);
            return {...state, rooms:payload,  loading:false};
        }
        case ReservationActions.POST_CLIENT_RESPONSE:
        {
            const {payload} = (action as ReservationActions.PostClientResponse);
            console.log({payload});
            return {...state, responseClient:payload, loading:false}
        }
        case ReservationActions.POST_RESERVATION_RESPONSE:
        {
            const {payload} = (action as ReservationActions.PostReservationResponse);
            return {...state, response: payload, loading:false};
        }
        default:
            return state;
    }
}