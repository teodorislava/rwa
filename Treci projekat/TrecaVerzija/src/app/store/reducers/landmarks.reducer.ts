import * as LandmarkActions from '../actions';
import {Landmark} from '../../models/landmarks';
import {Action} from '@ngrx/store';
import {EntityState, createEntityAdapter, EntityAdapter} from '@ngrx/entity';

export interface LandmarkState extends EntityState<Landmark> {};

const adapter: EntityAdapter<Landmark> = createEntityAdapter<Landmark>();

const landmarks: Landmark[] = [

];


const InitialState: LandmarkState = {
    ids:[],
    entities: {
    }
}
export function landmarksReducer(state: LandmarkState = InitialState, action:Action)
{
    switch(action.type) {
        case LandmarkActions.RETURN_LANDMARKS:
        {
            const {payload} = (action as LandmarkActions.ReturnLandmarks);
            console.log({payload});
            const ad =  adapter.addAll(payload, state);
            console.log(ad);
            return ad;
        }
        default:
        {
            console.log(state);
            return state;
        }
    }
}

export const selectors = adapter.getSelectors();