import * as Rxjs from 'rxjs';
import { IfObservable } from 'rxjs/observable/IfObservable';
import {DatabaseService} from './database';

export class Rezervacije
{
    constructor()
    {}
    static RezervisiNovuMasazu(el)
    {        
        let pr = fetch('http://localhost:3000/masaze', {
            method: 'post',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,maser_ime:el.maser_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=> 
                {
                    res.json();
                    if(res.status===201 || res.status===200)
                    {
                        let div=document.getElementById("dodatak");
                        div.innerHTML="";
                        div.innerHTML="Rezervacija uspesna!";
                    }
                    else
                    {
                        let div=document.getElementById("dodatak");
                        div.innerHTML="";
                        div.innerHTML="Rezervacija neuspesna!";
                    }
            });
        
            /*let req=new XMLHttpRequest();
            req.open("POST", 'http://localhost:3000/masaza', true);
            req.setRequestHeader('Content-Type', 'application/json');
            req.send(JSON.stringify({tip:el.tip,maser_ime:el.maser_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena}));
            */          
    }
    static obrisiMasazu(el)
    {
        fetch('http://localhost:3000/masaze/'+el.id, {
            method: 'delete'
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Brisanje uspesno!";
                }
                else
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Brisanje neuspesno!";
                }
        });
    }
    static izmeniMasazu(el)
    {
        console.log(el);
        const pr = fetch('http://localhost:3000/masaze/'+el.id, {
            method: 'put',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,maser_ime:el.maser_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Izmena uspesna!";
                }
                else
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Izmena neuspesna!";
                }
        });
    }
    static RezervisiNoviTrening(el)
    {        
        fetch('http://localhost:3000/treninzi', {
            method: 'post',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,trener_ime:el.trener_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Rezervacija uspesna!";
                }
                else
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Rezervacija neuspesna!";
                }
        });
    }
    static obrisiTrening(el)
    {
        fetch('http://localhost:3000/treninzi/'+el.id, {
            method: 'delete'
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Brisanje uspesno!";
                }
                else
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Brisanje neuspesno!";
                }
        });
    }
    static izmeniTrening(el)
    {
        fetch('http://localhost:3000/treninzi/'+el.id, {
            method: 'put',
            headers: {
              'Accept': 'application/json,  text/plain',
              'Content-Type': 'application/json'
            },
            body:JSON.stringify({tip:el.tip,trener_ime:el.trener_ime,trajanje:el.trajanje,klijent_ime:el.klijent_ime,klijent_prezime:el.klijent_prezime,dan:el.dan,mesec:el.mesec,godina:el.godina,vreme:el.vreme,cena:el.cena})
          }).then(res=>{
                res.json();
                if(res.status===201 || res.status===200)
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Izmena uspesna!";
                }
                else
                {
                    let div=document.getElementById("dodatak");
                    div.innerHTML="";
                    div.innerHTML="Izmena neuspesna!";
                }
        });
    }
}