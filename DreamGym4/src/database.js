import * as Rxjs from 'rxjs';
import { IfObservable } from 'rxjs/observable/IfObservable';
import {Trening} from './klase.js';
import {Masaza} from './klase.js';

export class DatabaseService {
  constructor() {}
  static prikazMasaza(od, doo, maser)
  {
    let splOd=od.split("-");
    let splDo=doo.split("-");
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let lab=document.createElement("label");
    lab.innerHTML="Pronadjene su ove rezervacije: ";
    div.appendChild(lab);
    const url="http://localhost:3000/masaze";
    const obs=Rxjs.Observable.fromPromise(fetch(url).then(response=>
    {
        response.json().then(el=> 
        {
          el.forEach(el=>{
            let m=new Masaza(el.id, el.tip, el.trajanje, el.maser_ime,
            el.klijent_ime, el.klijent_prezime,
            el.dan, el.mesec, el.godina, el.vreme, el.cena);
            if(m.godina>=splOd[0] && m.mesec>=splOd[1] && m.dan>=splOd[2] && m.godina<=splDo[0] && m.mesec<=splDo[1] && m.dan<=splDo[2] && m.maser_ime===maser)
            {
              m.prikaz();
            }
          });
        });
    }));
  }
  static prikazTreninga(od, doo, trener)
  {
    let splOd=od.split("-");
    let splDo=doo.split("-");
    let div=document.getElementById("dodatak");
    div.innerHTML="";
    let lab=document.createElement("label");
    lab.innerHTML="Pronadjene su ove rezervacije: ";
    div.appendChild(lab);
    const url="http://localhost:3000/treninzi";
    const obs=Rxjs.Observable.fromPromise(fetch(url).then(response=>
    {
        response.json().then(el=> 
        {
          el.forEach(el=>{
          let m=new Trening(el.id, el.tip, el.trajanje, el.trener_ime,
          el.klijent_ime, el.klijent_prezime,
          el.dan, el.mesec, el.godina, el.vreme, el.cena);
          if(m.godina>=splOd[0] && m.mesec>=splOd[1] && m.dan>=splOd[2] && m.godina<=splDo[0] && m.mesec<=splDo[1] && m.dan<=splDo[2] && m.trener_ime===trener)
          {
            m.prikaz();
          }
          });
        });
    }));
  }
  static get(tip)
  {
    if(tip==="masaza")
    {
      return fetch('http://localhost:3000/masaze').then( response =>
          response.json());
    }
    else 
      return fetch('http://localhost:3000/treninzi').then( response =>
      response.json());
  }
}
